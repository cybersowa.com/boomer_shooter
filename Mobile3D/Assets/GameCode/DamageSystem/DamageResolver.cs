using System.Collections;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

namespace GameCode.DamageSystem
{
    public class DamageResolver : MonoBehaviour, IValidRestrictions
    {
        public ADamageReceiver DamageReceiver;

        [SerializeField] private bool UsesAllHurtboxesInItsHierarchyAndNothingMore = true;
        [SerializeField] private List<Hurtbox> Hurtboxes = new List<Hurtbox>();

        // Update is called once per frame
        void Update()
        {
            int damage = 0;

            for (int i = 0; i < Hurtboxes.Count; i++)
            {
                var hurtbox = Hurtboxes[i];
                damage += hurtbox.AccumulatedDamage();
                hurtbox.ClearAccumulatedDamage();
            }

            if (damage > 0)
            {
                DamageReceiver?.ReceiveDamage(damage);
            }
        }

        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            if (UsesAllHurtboxesInItsHierarchyAndNothingMore)
            {
                var allHurtboxes = GetComponentsInChildren<Hurtbox>(true);
                bool didChange = false;
                if (allHurtboxes.Length != Hurtboxes.Count)
                {
                    didChange = true;
                }
                else
                {
                    for (int i = 0; i < allHurtboxes.Length; i++)
                    {
                        if (allHurtboxes[i] != Hurtboxes[i])
                        {
                            didChange = true;
                            break;
                        }
                    }
                }

                if (didChange)
                {
                    Hurtboxes.Clear();
                    Hurtboxes.AddRange(allHurtboxes);

                    return SetupValidationHelper.ChangedWithoutMessage();
                }
            }

            return SetupValidationHelper.NoChangeWithoutMessage();
        }
    }
}
