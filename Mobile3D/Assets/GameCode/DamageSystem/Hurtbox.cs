using System.Collections;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

public class Hurtbox : MonoBehaviour, IValidRestrictions
{
    private List<DamageEntry> m_DamageEntries = new List<DamageEntry>();
    
    public HitResponse AddDamageEntry(DamageEntry damageEntry)
    {
        m_DamageEntries.Add(damageEntry);

        return new HitResponse();
    }

    public int AccumulatedDamage()
    {
        int damageValue = 0;

        for (int i = 0; i < m_DamageEntries.Count; i++)
        {
            damageValue += m_DamageEntries[i].DamageValue;
        }

        return damageValue;
    }

    public void ClearAccumulatedDamage()
    {
        m_DamageEntries.Clear();
    }

    public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
    {
        if (gameObject.layer != Const.Layers.HurtboxLayer)
        {
            gameObject.layer = Const.Layers.HurtboxLayer;
            return SetupValidationHelper.ChangedWithoutMessage();
        }

        return SetupValidationHelper.NoChangeWithoutMessage();
    }
}

public struct DamageEntry
{
    public int DamageValue;
}

public struct HitResponse
{
    
}
