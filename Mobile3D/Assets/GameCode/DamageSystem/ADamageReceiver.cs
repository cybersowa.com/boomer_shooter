﻿using UnityEngine;

namespace GameCode.DamageSystem
{
    public abstract class ADamageReceiver : MonoBehaviour
    {
        public abstract void ReceiveDamage(int damageValue);
    }
}