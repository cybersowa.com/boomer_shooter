using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrudeElevator : MonoBehaviour
{
    public Rigidbody RigidbodyComponent;
    
    public float MoveHeight = 10.0f;
    public float MoveSpeed = 1.0f;

    public float Delay = 3.0f;
    
    private Vector3 m_InitialPosition;

    private Vector3 TopPosition
    {
        get => m_InitialPosition + Vector3.up * MoveHeight;
    }

    private void Awake()
    {
        m_InitialPosition = transform.position;
    }

    private void OnEnable()
    {
        transform.position = m_InitialPosition;
        StartCoroutine(MoveUp());
    }

    private IEnumerator MoveUp()
    {
        while (Vector3.Distance(transform.position, TopPosition) > float.Epsilon)
        {
            var targetPosition = Vector3.MoveTowards(transform.position, TopPosition, MoveSpeed * Time.fixedDeltaTime);
            RigidbodyComponent.MovePosition(targetPosition);
            // RigidbodyComponent.velocity = Vector3.up * MoveSpeed;
            yield return new WaitForFixedUpdate();
        }
        
        StartCoroutine(WaitOnTop());
    }

    private IEnumerator WaitOnTop()
    {
        RigidbodyComponent.velocity = Vector3.zero;
        float waitTime = 0.0f;
        while (waitTime <= Delay)
        {
            waitTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        
        StartCoroutine(MoveDown());
    }

    private IEnumerator MoveDown()
    {
        while (Vector3.Distance(transform.position, m_InitialPosition) > float.Epsilon)
        {
            var targetPosition = Vector3.MoveTowards(transform.position, m_InitialPosition, MoveSpeed * Time.fixedDeltaTime);
            RigidbodyComponent.MovePosition(targetPosition);
            // RigidbodyComponent.velocity = Vector3.down * MoveSpeed;
            yield return new WaitForFixedUpdate();
        }
        
        StartCoroutine(WaitOnBottom());
    }

    private IEnumerator WaitOnBottom()
    {
        RigidbodyComponent.velocity = Vector3.zero;
        float waitTime = 0.0f;
        while (waitTime <= Delay)
        {
            waitTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        
        StartCoroutine(MoveUp());
    }
}
