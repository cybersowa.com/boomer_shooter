using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public Hurtbox HitBox;
    public int HP = 100;

    public ExplosionEffect OnDestroyEffect;
    
    // Update is called once per frame
    void Update()
    {
        HP -= HitBox.AccumulatedDamage();
        HitBox.ClearAccumulatedDamage();
        
        if (HP <= 0)
        {
            var explosion = Instantiate(OnDestroyEffect, transform.position, Quaternion.identity);
            explosion.TriggerExplosion();
            Destroy(gameObject);
        }
    }
}
