﻿using System;
using System.Collections.Generic;
using GameCode.ActorsDeclarations;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Enemies.AI;
using GameCode.Levels;
using GameCode.Pooling;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.ProBuilder.MeshOperations;
using Random = UnityEngine.Random;

namespace GameCode.Enemies
{
    public class MoreHumanThanHumanEnemy : AIEnemyPawn
    {
        public Transform BarrelPivot;
        public Rigidbody RigidbodyComponent;

        [Header("movement")]
        public float MaxMovementSpeed = 10.0f;
        public float AccelerationRate = 7.5f;
        public float DecelerationRate = 10.0f;
        public float AggroSwingRepeatCycleDuration = 6.0f;
        public Vector2 LevitationHeightRange = new Vector2(2.0f, 5.0f);
        
        [Header("Aiming and shooting")]
        public float RotationRate = 5.0f;
        
        public float CautiosRange = 30.0f;
        public float AggroRange = 15.0f;
        public float ShotDelay = 1.0f;
        public float AimLockTime = 0.25f;

        public AShotEffect ShotEffectPrefab;
        public Vital Vitals;
        
        private float m_DelayToNextShot;
        private bool m_isPreparingShot = false;
        private float m_shotPreparationTimeCounter;

        private AIRequest m_CurrentRequest;
        private float m_PositionHeight;

        public override bool HasAIRequest => m_CurrentRequest != null;
        public override AIRequest CurrentAIRequest => m_CurrentRequest;

        public override void OnBeingPushedToPoolBucket()
        {
            base.OnBeingPushedToPoolBucket();
            
            ClearAIRequest();
            {//clear onw state
                m_DelayToNextShot = 0.0f;
                m_isPreparingShot = false;
                m_shotPreparationTimeCounter = 0.0f;
                m_PositionHeight = 0.0f;
            }
        }

        public override List<IAttackBehaviourInfo> GetAllAvailableAttackInfos()
        {
            var list = new List<IAttackBehaviourInfo>(1);
            list.Add(new BasicAttackBehaviourInfo()
            {
                AttackRange = new Vector2(Mathf.Min(5.0f, AggroRange * 0.5f), AggroRange),
                AttackTokenCost = 1
            });

            return list;
        }
        
        public override void SetRequest(AIRequest request)
        {
            if (request is AIMoveToRequest moreRequest)
            {
                m_PositionHeight = Random.Range(LevitationHeightRange.x, LevitationHeightRange.y);
            }
            else if(request is AIAttackRequest attackRequest)
            {
                m_DelayToNextShot = ShotDelay;
            }
            
            m_CurrentRequest = request;
        }

        public override void ClearAIRequest()
        {
            m_CurrentRequest = null;
        }

        private void Update()
        {
            if (HasAIRequest == false)
            {
                return;
            }

            if (m_CurrentRequest is AIMoveToRequest moveRequest)
            {
                // var direction = ((moveRequest.TargetPosition + Vector3.up * m_PositionHeight) - transform.position).normalized;
                // Vector3 finalVelocity = CalculateVelocityForDesiredDirection(direction, Time.deltaTime);
                var targetPosition = (moveRequest.MovementTargetPosition + Vector3.up * m_PositionHeight);
                Vector3 finalVelocity = CalculateVelocityForDesiredTarget(targetPosition, Time.deltaTime);
                RigidbodyComponent.velocity = finalVelocity;
                float twoFramesMovementDistance = finalVelocity.magnitude * Time.deltaTime;
                if ((targetPosition - transform.position).magnitude <= twoFramesMovementDistance)
                {
                    moveRequest.State = AIRequestState.CompletedWithSuccess;
                }

                if (moveRequest.MovementControlFlags.HasFlag(MovementControlFlags.FaceTarget))
                {
                    transform.LookAt(moveRequest.POI.position + Vector3.up, Vector3.up);
                }
            }
            else if(m_CurrentRequest is AIAttackRequest attackRequest)
            {
                m_DelayToNextShot -= Time.deltaTime;
                RigidbodyComponent.velocity = CalculateVelocityChangeTo(Vector3.zero, Time.deltaTime);

                if (m_DelayToNextShot <= 0.0f && attackRequest.State == AIRequestState.Running)
                {
                    ShotEffectPrefab.ShotCopy(BarrelPivot);
                    attackRequest.State = AIRequestState.CompletedWithSuccess;
                }
            }
            
            UpdateHealthStatus();
        }

        private Vector3 CalculateVelocityForDesiredTarget(Vector3 desiredTargetPosition, float deltaTime)
        {
            var offsetToTarget = desiredTargetPosition - transform.position;
            var velocity = CalculateVelocityForDesiredDirection(offsetToTarget.normalized, deltaTime);
            float distanceToTarget = offsetToTarget.magnitude;
            float movementDistanceDelta = velocity.magnitude * deltaTime;

            if (Mathf.Approximately(movementDistanceDelta, 0.0f) || Mathf.Approximately(distanceToTarget, 0.0f))
            {
                velocity = Vector3.zero;
            }
            else if (movementDistanceDelta > distanceToTarget)
            {
                velocity *= distanceToTarget / movementDistanceDelta;
            }

            return velocity;
        }

        private Vector3 CalculateVelocityForDesiredDirection(Vector3 desiredDirection, float deltaTime)
        {
            return Vector3.MoveTowards(RigidbodyComponent.velocity,
                desiredDirection * MaxMovementSpeed, AccelerationRate * deltaTime);
        }

        private Vector3 CalculateVelocityChangeTo(Vector3 targetVelocity, float deltaTime)
        {
            bool isDecelerating = RigidbodyComponent.velocity.sqrMagnitude > targetVelocity.magnitude;
            if (isDecelerating == false)
            {
                if (Vector3.Dot(RigidbodyComponent.velocity, targetVelocity) < 0.0f)
                {//going opposite direction
                    isDecelerating = true;
                }
            }

            float accelerationRate = isDecelerating ? DecelerationRate : AccelerationRate;
            
            return Vector3.MoveTowards(RigidbodyComponent.velocity,
                targetVelocity, accelerationRate * deltaTime);
        }

        private void UpdateHealthStatus()
        {
            if (Vitals.HP <= 0)
            {
                Actor.Despawn(this, false);
            }
        }

        private bool InAggroRange(float distanceToPlayer)
        {
            return distanceToPlayer < AggroRange;
        }

        private bool InCautiousRange(float distanceToPlayer)
        {
            return distanceToPlayer < CautiosRange;
        }

        private struct TargetInfo
        {
            public Vector3 OffsetToPlayer;
            public Vector3 DirectionToPlayer;
            public float DistanceToPlayer;
        }
        
        private void UpdateMovement(TargetInfo targetInfo)
        {
            if (InAggroRange(targetInfo.DistanceToPlayer))
            {
                float cycle = Mathf.Repeat(Time.realtimeSinceStartup, AggroSwingRepeatCycleDuration) - AggroSwingRepeatCycleDuration * 0.5f;
                cycle = Mathf.Sign(cycle);
                Vector3 sideDirection =
                    new Vector3(targetInfo.DirectionToPlayer.z, 0.0f, -targetInfo.DirectionToPlayer.x);
                sideDirection = sideDirection * cycle;
                sideDirection.Normalize();
                
                RigidbodyComponent.velocity = Vector3.MoveTowards(RigidbodyComponent.velocity,
                    sideDirection * MaxMovementSpeed, AccelerationRate * Time.deltaTime);
            }
            else if (InCautiousRange(targetInfo.DistanceToPlayer))
            {
                RigidbodyComponent.velocity = Vector3.MoveTowards(RigidbodyComponent.velocity,
                    targetInfo.DirectionToPlayer * MaxMovementSpeed, AccelerationRate * Time.deltaTime);
            }
            else
            {
                StopMovement();
            }
        }

        private void StopMovement()
        {
            RigidbodyComponent.velocity = Vector3.MoveTowards(RigidbodyComponent.velocity, Vector3.zero,
                DecelerationRate * Time.deltaTime);
        }

        public override AActorSnapshot CreateSnapshotForCurrentState()
        {
            SerializedSnapshot snapshot = new SerializedSnapshot();
            AssignBaseFields(snapshot);
            snapshot.HP = Vitals.HP;
            return snapshot;
        }

        public override AActorSnapshot CreateDefaultSnapshot()
        {
            SerializedSnapshot snapshot = new SerializedSnapshot();
            AssignBaseFields(snapshot);
            snapshot.HP = Vitals.HP;
            return snapshot;
        }

        public override void Load(AActorSnapshot fromSnapshot)
        {
            Assert.IsTrue(fromSnapshot is SerializedSnapshot);
            var turretState = fromSnapshot as SerializedSnapshot;

            Vitals.HP = turretState.HP;
        }

        [Serializable]
        public class SerializedSnapshot : AActorSnapshot
        {
            public int HP;
            
            public override object Clone()
            {
                var clone = new SerializedSnapshot();
                clone.PrefabID = PrefabID;
                clone.InstanceID = InstanceID;
                clone.HP = HP;

                return clone;
            }


            public static (bool, SerializedBucket) Serializer(object instance, int functionHandle)
            {
                Assert.IsTrue(instance is SerializedSnapshot);
                
                var state = instance as SerializedSnapshot;

                SerializedBucket bucket = new SerializedBucket()
                {
                    Header = new SerializedBucketHeader()
                    {
                        SerializationFunctionID = functionHandle,
                        Version = 1
                    }
                };
            
                List<byte> bucketData = new List<byte>();
                {
                    bucketData.AddRange(SerializationUtils.ToBytes(state.PrefabID));
                    bucketData.AddRange(SerializationUtils.ToBytes(state.InstanceID));
                    bucketData.AddRange(SerializationUtils.ToBytes(state.Position));
                    bucketData.AddRange(SerializationUtils.ToBytes(state.Rotation));
                    bucketData.AddRange(BitConverter.GetBytes(state.HP));
                }
                bucket.Header.BucketSize = bucketData.Count;
                bucket.DataBucket = bucketData.ToArray();

                return (true, bucket);
            }

            public static (bool, object) Deserializer(SerializedBucket bucket)
            {
                var state = new SerializedSnapshot();
                int readIndex = 0;
                var dataArray = bucket.DataBucket;

                readIndex += SerializationUtils.ToPrefabID(dataArray, readIndex, out state.PrefabID);
                readIndex += SerializationUtils.ToInstanceID(dataArray, readIndex, out state.InstanceID);
                readIndex += SerializationUtils.ToVector3(dataArray, readIndex, out state.Position);
                readIndex += SerializationUtils.ToQuaternion(dataArray, readIndex, out state.Rotation);
                state.HP = BitConverter.ToInt32(dataArray, readIndex);
                readIndex += sizeof(int);

                return (true, state);
            }
        }
    }
}