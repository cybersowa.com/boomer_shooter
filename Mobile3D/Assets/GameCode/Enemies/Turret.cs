﻿using System;
using GameCode.Levels;
using GameCode.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Enemies
{
    public class Turret : ASessionActor
    {
        public Transform BarrelPivot;

        public float CautiosRange = 30.0f;
        public float AggroRange = 15.0f;
        public float ShotDelay = 1.0f;

        public AShotEffect ShotEffectPrefab;
        public Vital Vitals;
        
        private float m_DelayToNextShot;
        
        private void Update()
        {
            if (!WorldManager.Current.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
            {
                Unity.Assertions.Assert.IsTrue(false);
                return;
            }
            
            var player = playerCharacterWorldComponent.PlayerController;
            var playerPosition = player.Character.transform.position + Vector3.up;

            var offsetToPlayer = (playerPosition - transform.position);
            var distanceToPlayer = offsetToPlayer.magnitude;
            var directionToPlayer = offsetToPlayer.normalized;

            if (distanceToPlayer < CautiosRange)
            {
                BarrelPivot.forward = directionToPlayer;
            }

            if (distanceToPlayer < AggroRange)
            {
                m_DelayToNextShot -= Time.deltaTime;
                if (m_DelayToNextShot <= 0.0f)
                {
                    ShotEffectPrefab.ShotCopy(BarrelPivot);
                    m_DelayToNextShot = ShotDelay;
                }
            }
            else
            {
                m_DelayToNextShot = ShotDelay;
            }

            if (Vitals.HP <= 0)
            {
                Destroy(gameObject);
            }
        }

        public override AActorSnapshot CreateSnapshotForCurrentState()
        {
            SerializedSnapshot snapshot = new SerializedSnapshot();
            AssignBaseFields(snapshot);
            snapshot.HP = Vitals.HP;
            return snapshot;
        }

        public override AActorSnapshot CreateDefaultSnapshot()
        {
            SerializedSnapshot snapshot = new SerializedSnapshot();
            AssignBaseFields(snapshot);
            snapshot.HP = Vitals.HP;
            return snapshot;
        }

        public override void Load(AActorSnapshot fromSnapshot)
        {
            Assert.IsTrue(fromSnapshot is SerializedSnapshot);
            var turretState = fromSnapshot as SerializedSnapshot;

            Vitals.HP = turretState.HP;
        }

        [Serializable]
        public class SerializedSnapshot : AActorSnapshot
        {
            public int HP;
            
            public override object Clone()
            {
                var clone = new SerializedSnapshot();
                clone.PrefabID = PrefabID;
                clone.InstanceID = InstanceID;
                clone.HP = HP;

                return clone;
            }
        }
    }
}