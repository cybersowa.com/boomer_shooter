﻿using System;
using UnityEngine;

namespace GameCode.Enemies.AI
{
    public abstract class AIRequest
    {
        public float RequestTime;
        public AIRequestState State;
    }

    public class AIMoveToRequest : AIRequest
    {
        public MovementControlFlags MovementControlFlags;
        public Vector3 MovementTargetPosition;
        public Transform POI;//field type is a subject to change
        //consider adding a range param so AI agent doesn't have to achieve exact position
    }

    public class AIAttackRequest : AIRequest
    {
        public AttackControlFlags AttackControlFlags;
        public GameObject AttackTarget;//field type is a subject to change
        public int TokensSpent;
    }

    public enum AIRequestState
    {
        Unset,
        Running,
        CompletedWithSuccess,
        DroppedDueToFailure
    }

    [Flags]
    public enum MovementControlFlags
    {
        Default = 0,
        FaceTarget,
        Stealth,
        Sprint
    }

    [Flags]
    public enum AttackControlFlags
    {
        Default = 0,
    }
}