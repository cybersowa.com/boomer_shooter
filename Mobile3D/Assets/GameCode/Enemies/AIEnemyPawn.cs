﻿using System.Collections.Generic;
using GameCode.Enemies.AI;
using UnityEngine;

namespace GameCode.Enemies
{
    public abstract class AIEnemyPawn : ASessionActor
    {
        public abstract List<IAttackBehaviourInfo> GetAllAvailableAttackInfos();
        public abstract bool HasAIRequest { get; }
        public abstract AIRequest CurrentAIRequest { get; }

        public abstract void SetRequest(AIRequest request);

        public abstract void ClearAIRequest();
    }

    public interface IAttackBehaviourInfo
    {
        public Vector2 AttackRange { get; }
        public int AttackTokenCost { get; }
    }

    public class BasicAttackBehaviourInfo : IAttackBehaviourInfo
    {
        public Vector2 AttackRange { get; set; }
        public int AttackTokenCost { get; set; }
    }
}