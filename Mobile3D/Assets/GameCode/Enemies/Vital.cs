﻿using GameCode.DamageSystem;
using GameCode.Levels;
using UnityEngine;

namespace GameCode.Enemies
{
    public class Vital : ADamageReceiver
    {
        public int HP = 100;
        
        public override void ReceiveDamage(int damageValue)
        {
            HP -= damageValue;
            Debug.Log($"{name} received {damageValue} damage. Health left is {HP}");
        }
    }
}