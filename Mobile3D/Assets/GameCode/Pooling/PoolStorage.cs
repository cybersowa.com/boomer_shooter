﻿using System.Collections.Generic;
using UnityEngine;

namespace GameCode.Pooling
{
    public class PoolStorage : MonoBehaviour
    {
        public PoolBucket BucketPrototype;

        private Dictionary<PrefabID, PoolBucket> m_Buckets = new Dictionary<PrefabID, PoolBucket>();

        public bool TryGet(PrefabID prefabID, out IPoolable poolableObject)
        {
            poolableObject = null;
            
            if (m_Buckets.TryGetValue(prefabID, out var bucket))
            {
                if (bucket.TryPop(out poolableObject))
                {
                    return true;
                }
            }

            return false;
        }

        public void Push(IPoolable poolableObject)
        {
            var prefabId = poolableObject.PrefabID;
            
            if (m_Buckets.TryGetValue(prefabId, out var bucket) == false)
            {
                bucket = new GameObject($"PoolBucket_For_{poolableObject.GetType()}_{prefabId}")
                    .AddComponent<PoolBucket>();
                bucket.transform.parent = transform;
                m_Buckets.Add(prefabId, bucket);
            }
            
            bucket.Push(poolableObject);
        }
    }
}