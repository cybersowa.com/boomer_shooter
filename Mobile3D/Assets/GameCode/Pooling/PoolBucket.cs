﻿using System;
using System.Collections.Generic;
using GameCode.Utils;
using UnityEngine;

namespace GameCode.Pooling
{
    public class PoolBucket : MonoBehaviour
    {
        [NonSerialized]
        public float IdleTime = 0.0f;

        private List<IPoolable> m_PooledObjects = new List<IPoolable>();

        public bool TryPop(out IPoolable poolableObject)
        {
            IdleTime = 0;
            poolableObject = null;

            if (m_PooledObjects.Count > 0)
            {
                var lastIndex = m_PooledObjects.LastIndex();
                poolableObject = m_PooledObjects[lastIndex];
                m_PooledObjects.RemoveAt(lastIndex);

                poolableObject.OnBeingReleasedFromPoolBucket();
                poolableObject.gameObject.transform.parent = null;
                poolableObject.gameObject.SetActive(true);
                
                return true;
            }

            return false;
        }

        public void Push(IPoolable poolableObject)
        {
            m_PooledObjects.Add(poolableObject);
            poolableObject.OnBeingPushedToPoolBucket();
            poolableObject.gameObject.SetActive(false);
            poolableObject.gameObject.transform.parent = transform;
        }

        public void RetireLast()
        {
            if (m_PooledObjects.Count > 0)
            {
                var lastIndex = m_PooledObjects.LastIndex();
                var pooledObject = m_PooledObjects[lastIndex];
                m_PooledObjects.RemoveAt(lastIndex);
                
                Destroy(pooledObject.gameObject);
            }
        }

        public void RetireAll()
        {
            while (m_PooledObjects.Count > 0)
            {
                RetireLast();
            }
        }
    }
}