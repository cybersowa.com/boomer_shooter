﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.Pooling
{
    public static class ObjectPool
    {
        private static PoolStorage m_Pools = null;

        private static PoolStorage Pools
        {
            get
            {
                if (m_Pools == null)
                {
                    m_Pools = new GameObject("ObjectPoolStorage").AddComponent<PoolStorage>();
                    GameObject.DontDestroyOnLoad(m_Pools.gameObject);
                }

                return m_Pools;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="instanceID">Instance ID that should be assigned to returned object</param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <param name="targetScene"></param>
        /// <param name="parent"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Instantiate<T>(T prefab, Vector3 position, Quaternion rotation,
            Scene targetScene, Transform parent = null) where T : MonoBehaviour, IPoolable
        {
            T objectInstance = null; 
            if (Pools.TryGet(prefab.PrefabID, out var poolableObject))
            {
                objectInstance = (T)poolableObject;
                objectInstance.transform.parent = parent;
                objectInstance.transform.position = position;
                objectInstance.transform.rotation = rotation;
            }
            else
            {
                objectInstance = UnityEngine.Object.Instantiate(prefab, position, rotation, parent);
            }

            if (targetScene.IsValid())
            {
                SceneManager.MoveGameObjectToScene(objectInstance.gameObject, targetScene);
            }
            else
            {
                Object.DontDestroyOnLoad(objectInstance.gameObject);
            }
            
            objectInstance.SetPooledFlag(false);

            return objectInstance;
        }

        public static void Destroy(IPoolable poolObject)
        {
            Object.DontDestroyOnLoad(poolObject.gameObject);
            poolObject.SetPooledFlag(true);
            Pools.Push(poolObject);
        }

        public static bool IsNullOrPooled(IPoolable poolableObject)
        {
            if (poolableObject == null)
            {
                return true;
            }

            return poolableObject.IsInPool;
        }
    }

    public interface IPoolable
    {
        public PrefabID PrefabID { get; }
        public GameObject gameObject { get; }
        bool IsInPool { get; }

        /// <summary>
        /// To be used only by pooling system 
        /// </summary>
        /// <param name="isPooled"></param>
        void SetPooledFlag(bool isPooled);

        /// <summary>
        /// Aka: please object, reset yourself to vanilla prefab state
        /// </summary>
        public void OnBeingReleasedFromPoolBucket();
        public void OnBeingPushedToPoolBucket();
    }
}