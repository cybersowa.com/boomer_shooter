﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCode.ActorsDeclarations;
using GameCode.General;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using GameCode.Player;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace GameCode.GameState
{
    public interface IGameComponent{}
    
    public static class Game
    {
        /// <summary>
        /// Demo mode is used for example when we test in editor starting from selected checkpoint.
        /// In demo mode we don't want to:
        ///  - save game state to file
        /// </summary>
        public static bool InDemoMode = false;
        
        /// <summary>
        /// No need to serialize, no need for bespoke component
        /// </summary>
        public static int ActiveSaveSlot = -1;
        
        /// <summary>
        /// Used ONLY to reset gamestate to it's checkpoint snapshot
        /// </summary>
        public static GameSnapshot CheckpointState = null;

        /// <summary>
        /// Need to set value of this field properly when loading a saved game
        /// </summary>
        public static ulong LastGlobalActorInstanceID;
        
        /// <summary>
        /// here goes EVERYTHING runtime-simulation-active-actors happen to need.
        /// For example when chunk is unloaded, we store snapshots here but if there
        /// was no new checkpoint achieved, upon death we reset to CHECKPOINT state.
        /// I.E. RuntimeState is allowed to have MORE RECENT data than CHECKPOINT
        /// because CHECKPOINT is allowed to reset gameworld to older state BY DESIGN.
        /// </summary>
        public static GameSnapshot RuntimeState = new GameSnapshot();
        
        public static WorldManager CurrentWorld = null;

        /// <summary>
        /// I'm not sure if global actors should be allowed. Leaving them not to have "feature completion" 
        /// </summary>
        public static List<ASessionActor> GlobalActors = new List<ASessionActor>();

        private static AsyncOperation s_WorldSceneLoadingOperation = null;

        private static Dictionary<System.Type, IGameComponent> Components => RuntimeState.GameComponents;
        private static Dictionary<int, PlayerData> PlayerDatas => RuntimeState.PlayerDatas;
        
        public static float CurrentLoadedState
        {
            get;
            private set;
        }
        
        public static void ResetState()
        {
            InDemoMode = false;
            ActiveSaveSlot = -1;
            CheckpointState = null;
            CurrentWorld = null;
            s_WorldSceneLoadingOperation = null;
            GlobalActors.Clear();
            ResetGameComponentsToDefault();
        }

        public static T Get<T>() where T : IGameComponent
        {
            var type = typeof(T);
            return (T)Components[type];
        }

        public static void Set<T>(T component) where T : IGameComponent
        {
            var type = typeof(T);
            Components[type] = component;
        }

        public static bool Has<T>() where T : IGameComponent
        {
            return Components.ContainsKey(typeof(T));
        }

        public static void Set(IGameComponent component)
        {
            var type = component.GetType();
            Components[type] = component;
        }
        

        public static bool TryGetPlayerComponent<T>(int playerID, out T component) where T : IPlayerDataComponent
        {
            if (PlayerDatas.TryGetValue(playerID, out var playerData))
            {
                var type = typeof(T);
                return playerData.TryGet(out component);
            }

            component = default(T);
            return false;
        }
        

        public static void SetPlayerComponent(int playerID, IPlayerDataComponent component)
        {
            var type = component.GetType();
            
            if (PlayerDatas.TryGetValue(playerID, out var playerData) == false)
            {
                playerData = new PlayerData();
                PlayerDatas.Add(playerID, playerData);
            }
            
            playerData.AddOrReplace(component);
        }

        public static bool TryGet<T>(out T component) where T : IGameComponent
        {
            var type = typeof(T);
            if (Components.ContainsKey(type))
            {
                component = (T)Components[type];
                return true;
            }

            component = default(T);
            return false;
        }

        public static T GetOrCreate<T>() where T : class, IGameComponent
        {
            if (TryGet(out T component))
            {
                return component;
            }

            component = New<T>();
            Set(component);

            return component;
        }

        private static T New<T>() where T : class, IGameComponent
        {
            var type = typeof(T);

            if (type == typeof(PermaDeathSetting)) return new PermaDeathSetting() as T;
            if (type == typeof(GameTime)) return new GameTime() as T;
            if (type == typeof(GameUpdateFlags)) return new GameUpdateFlags() as T;
            if (type == typeof(GameStatistics)) return new GameStatistics() as T;
            
            Assert.IsTrue(false, $"Component factory for type {type} isn't set up");

            return null;
        }
        

        public static GameSnapshot CreateSnapshot()
        {
            UpdateStateWithCurrentSimulation(RuntimeState);
            var snapshotClone = RuntimeState.Clone() as GameSnapshot;
            
            return snapshotClone;
        }

        private static void UpdateStateWithCurrentSimulation(GameSnapshot snapshot)
        {
            int worldID = CurrentWorld.Metadata.SceneGUID;
            if (snapshot.ChunkStates.TryGetValue(worldID,
                out var worldSerializedMap) == false)
            {
                worldSerializedMap = new SerializedStateMap();
                snapshot.ChunkStates[worldID] = worldSerializedMap;
            }

            {//handle static managers
                for (int i = 0; i < CurrentWorld.AllStaticManagers.Count; i++)
                {
                    var sm = CurrentWorld.AllStaticManagers[i];
                    worldSerializedMap[sm.ID] = sm.GenerateSerializedState();
                }

                var allLoadedChunks = CurrentWorld.RuntimeObjectsDatabase.All<ChunkManager>();
                for (int x = 0; x < allLoadedChunks.Count; x++)
                {
                    var chunkManager = allLoadedChunks[x];
                    if (snapshot.ChunkStates.TryGetValue(chunkManager.Metadata.SceneGUID,
                        out var chunkSerializedMap) == false)
                    {
                        chunkSerializedMap = new SerializedStateMap();
                        snapshot.ChunkStates[chunkManager.Metadata.SceneGUID] = chunkSerializedMap;
                    }

                    for (int i = 0; i < chunkManager.AllChunkStaticManagers.Count; i++)
                    {
                        var sm = chunkManager.AllChunkStaticManagers[i];
                        chunkSerializedMap[sm.ID] = sm.GenerateSerializedState();
                    }
                }
            }

            if (CurrentWorld.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
            {//handle player character data
                var characterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;
                snapshot.PlayerPosition = characterTransform.position;
                snapshot.PlayerRotation = characterTransform.rotation;
            }

            {//handle session actors
                var allActors = CurrentWorld.RuntimeObjectsDatabase.All<ASessionActor>();
                var allChunks = GetChunkMetadatasForWorld(CurrentWorld);
                
                for (int i = 0; i < allActors.Count; i++)
                {
                    var actor = allActors[i];
                    var actorSnapshot = actor.CreateSnapshotForCurrentState();
                    var chunk = FindChunkForActor(actor, allChunks);
                    int chunkID = (chunk != null) ? chunk.SceneGUID : 0;
                    snapshot.AddOrReplace(actorSnapshot, chunkID);
                }
                
                for (int i = 0; i < GlobalActors.Count; i++)
                {
                    var actor = GlobalActors[i];
                    var actorSnapshot = actor.CreateSnapshotForCurrentState();
                    var chunk = FindChunkForActor(actor, allChunks);
                    int chunkID = (chunk != null) ? chunk.SceneGUID : 0;
                    snapshot.AddOrReplace(actorSnapshot, chunkID);
                }
            }
        }

        public static List<ASessionActor> GetAllActorsWithinChunk(WorldManager world, ChunkSceneMetadata chunk)
        {
            var allChunkActors = new List<ASessionActor>();
            
            var allActorsInWorld = world.RuntimeObjectsDatabase.All<ASessionActor>();

            for (int i = 0; i < allActorsInWorld.Count; i++)
            {
                var actor = allActorsInWorld[i];
                if (chunk.IsPointWithinLoadZones(actor.transform.position))
                {
                    allChunkActors.Add(actor);
                }
            }
            
            return allChunkActors;
        }

        /// <summary>
        /// TODO: add LoadSaveFile routine where aside from loading RuntimeState we also load CheckpointState and other stuff if needed
        /// </summary>
        /// <param name="gameSnapshot"></param>
        public static void LoadSnapshot(GameSnapshot gameSnapshot)
        {
            ResetGameComponentsToDefault();
            for (int i = 0; i < GlobalActors.Count; i++)
            {
                Actor.Despawn(GlobalActors[i], false);
            }
            foreach (var gameComponent in gameSnapshot.GameComponents)
            {
                Set(gameComponent.Value);
            }

            RuntimeState = gameSnapshot;
            
            LoadWorld(RuntimeState.WorldName);
        }

        /// <summary>
        /// For the same game state that is currently used
        /// </summary>
        public static void LoadWorld(string worldName)
        {
            if (CurrentWorld != null)
            {
                SaveActorSnapshotsForWorld(RuntimeState, CurrentWorld);
            }
            
            RuntimeState.WorldName = worldName;
            
            //candidate to move to separate method -> tbd when checkpoint state / serialized game state refactor will be done
            var gameUpdateFlagsComponent = GetOrCreate<GameUpdateFlags>();
            gameUpdateFlagsComponent.StateFlags |= GameUpdateFlags.Flags.GameActive;
            gameUpdateFlagsComponent.StateFlags |= GameUpdateFlags.Flags.Paused;//ugly hack and currently we want to set/reset this flag from multiple places. Should add another layer of control
            
            //load required scene
            CurrentLoadedState = 0.0f;
            s_WorldSceneLoadingOperation = SceneManager.LoadSceneAsync(RuntimeState.WorldName, LoadSceneMode.Single);
            s_WorldSceneLoadingOperation.completed += OnGameSceneLoaded;
        }

        private static void SaveActorSnapshotsForWorld(GameSnapshot gameSnapshot, WorldManager world)
        {
            //TODO: separate actors to different chunks based on positions and chunk boundaries
            //NOTE: to do that we need all scene metadata registry <- done, needs testing
            
            var worldActors = world.RuntimeObjectsDatabase.All<ASessionActor>();
            var allWorldChunks = GetChunkMetadatasForWorld(world);
            ChunkSceneMetadata targetChunk = null;
            
            foreach (var actor in worldActors)
            {
                targetChunk = FindChunkForActor(actor, allWorldChunks);

                //TODO: decide if we want to assert if targetChunk is null or just put stuff in "world global"
                gameSnapshot.AddOrReplace(actor.CreateSnapshotForCurrentState(), 
                    (targetChunk != null) ? targetChunk.SceneGUID : 0);
            }
        }

        public static List<ChunkSceneMetadata> GetChunkMetadatasForWorld(WorldManager world)
        {
            var worldSceneMetadata = world.Metadata;

            var allWorldChunks = SceneRegistry.Instance.WorldBucketChunkScenesListMap[worldSceneMetadata.SceneGUID];
            return allWorldChunks;
        }

        public static ChunkSceneMetadata FindChunkForActor(ASessionActor actor, List<ChunkSceneMetadata> allWorldChunks)
        {
            ChunkSceneMetadata targetChunk = null;
            var actorPosition = actor.transform.position;
            
            // actorSnapshots[actor.InstanceID.ID] = actor.CreateSnapshotFOrCurrentState();
            for (int i = 0; i < allWorldChunks.Count; i++)
            {
                var candidateChunk = allWorldChunks[i];
                if (candidateChunk.IsPointWithinLoadZones(actorPosition))
                {
                    targetChunk = candidateChunk;
                    break;
                }
            }

            return targetChunk;
        }

        private static void AddActorToGameSnapshot(GameSnapshot gameSnapshot, AActorSnapshot actorSnapshot)
        {
            //check if actor already is registered. If so we probably need to reshuffle it a bit
            if (gameSnapshot.IdToActorMap.ContainsKey(actorSnapshot.InstanceID))
            {
                //update all relevant maps
                WorldChunkID targetPlacement;
            }

            gameSnapshot.IdToActorMap[actorSnapshot.InstanceID] = actorSnapshot;
        }

        private static void ResetGameComponentsToDefault()
        {
            Components.Clear();
        }

        private static void OnGameSceneLoaded(AsyncOperation worldLoadingOp)
        {
            CurrentWorld = FindWorldManager(RuntimeState.WorldName);
            LoadingScreen.LoadingScreen.Instance.StartCoroutine(LoadChunks());
        }

        public static WorldManager FindWorldManager(string worldName)
        {
            WorldManager worldManager = null;
            var scene = SceneManager.GetSceneByName(worldName);
            var rootObjects = scene.GetRootGameObjects();

            for (int i = 0; i < rootObjects.Length; i++)
            {
                //for perf reasons we assume the manager is in the root of the scene. Also for perf reason it should be the first one in the hierarchy
                var rootObject = rootObjects[i];
                if (rootObject.tag.Equals(Const.Tags.WorldManager))
                {
                    worldManager = rootObject.GetComponent<WorldManager>();

                    if (worldManager != null)
                    {
                        break;
                    }
                }
            }

            return worldManager;
        }

        /// <summary>
        /// TODO: When we load state from checkpoint, we should override RuntimeState so every piece of logic references the RuntimeState.
        /// This is needed because with Permadeath there will be in CheckpointState instance
        /// </summary>
        /// <returns></returns>
        private static IEnumerator LoadChunks()
        {
            WorldManager worldManager = WorldManager.Current;
            WorldSceneMetadata worldSceneMetadata = worldManager.Metadata;
            worldManager.ChunkLoadingSystem.WorldMetadata = worldSceneMetadata;
            SerializedStateMap serializedStateMapForWorld = null;
            if (RuntimeState.WorldStates.TryGetValue(worldSceneMetadata.SceneGUID,
                out serializedStateMapForWorld) == false)
            {
                serializedStateMapForWorld = new SerializedStateMap();
            }
            
            
            Time.timeScale = 0.0f;
            var frameWait = new WaitForEndOfFrame();
            var tenthOfSecondWait = new WaitForSecondsRealtime(0.1f);
            
            //TODO: all condition based chunk loading
            
            
            //Player position based chunk loading

            if (!worldManager.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
            {
                Time.timeScale = 1.0f;
                CurrentLoadedState = 1.0f;
                yield break;
            }
            
            PlayerCheckpoint checkpoint = null;

            Vector3 targetPlayerPosition = Vector3.zero;
            Quaternion targetPlayerRotation = Quaternion.identity;
            if (RuntimeState.ForceFirstCheckpoint)
            {
                targetPlayerPosition = worldSceneMetadata.FirstPlayerPosition;
                targetPlayerRotation = worldSceneMetadata.FirstPlayerRotation;
            }
            else
            {
                targetPlayerPosition = RuntimeState.PlayerPosition;
                targetPlayerRotation = RuntimeState.PlayerRotation;
            }
            
            // worldManager.PlayerController.Character.Motor.SetPositionAndRotation(targetPlayerPosition,
            // targetPlayerRotation);
            playerCharacterWorldComponent.PlayerController.Character.Motor.SetPositionAndRotation(targetPlayerPosition,
                targetPlayerRotation);
            
            do
            {
                worldManager.ChunkLoadingSystem.UpdateLoadingTasks(targetPlayerPosition);
                worldManager.ChunkLoadingSystem.UpdateLoadingStats();
                yield return tenthOfSecondWait;
            } while (worldManager.ChunkLoadingSystem.IsLoading);

            CurrentLoadedState = 1.0f;

            for (int i = 0; i < worldManager.AllStaticManagers.Count; i++)
            {
                var staticManager = worldManager.AllStaticManagers[i];
                staticManager.LoadState(serializedStateMapForWorld);
            }
            
            //spawn Global and "WorldGlobal" actors
            var worldGlobalChunkID = new WorldChunkID(CurrentWorld.Metadata.SceneGUID, 0);
            if (RuntimeState.ChunkToActorMap.TryGetValue(worldGlobalChunkID, out var actorMap))
            {
                for (int i = 0; i < actorMap.Count; i++)
                {
                    var actorInstanceID = actorMap[i];
                    var actorSnapshot = RuntimeState.IdToActorMap[actorInstanceID];
                    bool hasPrefab = ActorPrefabRegistry.Instance.TryGet(actorSnapshot.PrefabID, out ASessionActor actorPrefab);
                    Unity.Assertions.Assert.IsTrue(hasPrefab);
                    
                    Actor.Respawn(actorPrefab, actorSnapshot.Position, actorSnapshot.Rotation, actorSnapshot);
                }
            }

            if (RuntimeState.ForceFirstCheckpoint)
            {
                checkpoint = playerCharacterWorldComponent.InitialCheckpoint; //worldManager.InitialCheckpoint;
            }
            else
            {
                List<PlayerCheckpoint> allCheckpoints = new List<PlayerCheckpoint>();
                allCheckpoints.AddRange(playerCharacterWorldComponent.AllCheckpoints);//(worldManager.AllCheckpoints);
                var allLoadedChunks = worldManager.RuntimeObjectsDatabase.All<ChunkManager>();
                for (int i = 0; i < allLoadedChunks.Count; i++)
                {
                    allCheckpoints.AddRange(
                            allLoadedChunks[i].AllChunkCheckpoints
                        );
                }
                
                for (int i = 0; i < allCheckpoints.Count; i++)
                {
                    var c = allCheckpoints[i];
                    if (c.ID == RuntimeState.CheckpointID)
                    {
                        checkpoint = c;
                        break;
                    }
                }
            }
            
            Time.timeScale = 1.0f;
            
            yield return null;
        }


        #region Player Loop Updates

        public static void UpdateGameTime()
        {
            if (Application.isPlaying == false)
            {
                return;
            }
            
            GameUpdateFlags.Flags updateFlags = GameUpdateFlags.Flags.Clear;
            if (TryGet(out GameUpdateFlags component))
            {
                updateFlags = component.StateFlags;
            }

            if (updateFlags == GameUpdateFlags.Flags.Clear)
            {
                return;
            }

            var gameTimeComponent = GetOrCreate<GameTime>();
                
            if (updateFlags.HasFlag(GameUpdateFlags.Flags.GameActive) &&
                !updateFlags.HasFlag(GameUpdateFlags.Flags.Paused))
            {
                long deltaTime = (long)(Time.deltaTime * 1000.0f);
                gameTimeComponent.TotalPlaytimeInMilliseconds += deltaTime;
            }
        }

        #endregion

        public static bool IsPermadeathOn()
        {
            if (TryGet(out PermaDeathSetting permaDeathSetting) && permaDeathSetting.Value)
            {
                return true;
            }

            return false;
        }

        public static ulong GetNextInstanceID()
        {
            LastGlobalActorInstanceID++;
            return LastGlobalActorInstanceID;
        }
    }
}