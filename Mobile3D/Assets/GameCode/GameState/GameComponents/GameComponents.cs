﻿using System;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using UnityEngine.Assertions;

namespace GameCode.GameState
{
    public class PermaDeathSetting : IGameComponent
    {
        public const bool DefaultValue = false;
        
        public bool Value = DefaultValue;
        
        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is PermaDeathSetting);
            var permaDeathSetting = instance as PermaDeathSetting;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            bucketData.AddRange(BitConverter.GetBytes(permaDeathSetting.Value));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var permaDeathSetting = new PermaDeathSetting();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            permaDeathSetting.Value = BitConverter.ToBoolean(dataArray, readIndex);
            readIndex += sizeof(bool);

            return (true, permaDeathSetting);
        }
    }
    
    

    public class GameTime : IGameComponent
    {
        public long TotalPlaytimeInMilliseconds = 0;
        
        
        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is GameTime);
            var gameTime = instance as GameTime;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            bucketData.AddRange(BitConverter.GetBytes(gameTime.TotalPlaytimeInMilliseconds));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var gameTime = new GameTime();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            gameTime.TotalPlaytimeInMilliseconds = BitConverter.ToInt64(dataArray, readIndex);
            readIndex += sizeof(long);

            return (true, gameTime);
        }
    }
    
    

    public class GameUpdateFlags : IGameComponent
    {
        [Flags]
        public enum Flags
        {
            Clear = 0,
            /// <summary>
            /// Aka not in main menu and such. Still can be paused
            /// </summary>
            GameActive,
            Paused,
        }

        public Flags StateFlags;
        
        
        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is GameUpdateFlags);
            Assert.IsTrue(sizeof(Flags) == sizeof(int));
            var gameTime = instance as GameUpdateFlags;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            bucketData.AddRange(BitConverter.GetBytes((int)gameTime.StateFlags));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var gameTime = new GameUpdateFlags();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            gameTime.StateFlags = (Flags)BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(Flags);

            return (true, gameTime);
        }
    }

    
    
    public class GameStatistics : IGameComponent
    {
        public int RoundCounter = 0;
        
        
        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is GameStatistics);
            var gameTime = instance as GameStatistics;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            bucketData.AddRange(BitConverter.GetBytes(gameTime.RoundCounter));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var gameTime = new GameStatistics();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            gameTime.RoundCounter = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            return (true, gameTime);
        }
    }
}