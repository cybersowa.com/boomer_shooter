﻿using System.Collections;
using System.Collections.Generic;
using GameCode.Config;
using GameCode.GameState;
using UnityEngine;
using UnityEngine.LowLevel;
using UnityEngine.PlayerLoop;

public static class Kickstart
{
    [RuntimeInitializeOnLoadMethod]
    private static void AppStart()
    {
        InjectCustomSystems();
        SerializationSystem.ResetSerializationSystem();
        Game.ResetState();
    }
    
    private static void InjectCustomSystems()
    {
        //We don't need to use .GetDefaultPlayerLoop() in editor as the player loop seems to be reset on enter PlayMode
        //Also we don't want to use default one because it doesn't include ECS systems
        var defaultSystems = PlayerLoop.GetCurrentPlayerLoop();
        
        var customUpdate = new PlayerLoopSystem()
        {
            updateDelegate = Game.UpdateGameTime,
            type = typeof(Game)
        };

        if (PlayerLoopUtils.AddSystemAfter<TimeUpdate.WaitForLastPresentationAndUpdateTime>(ref defaultSystems, customUpdate))
        {
            Debug.Log("<color=green>Added CASSystem to engine update loop</color>");
        }
        else
        {
            Debug.LogError("<color=red>failed to add CASSystem to engine update loop</color>");
        }
        
        PlayerLoop.SetPlayerLoop(defaultSystems);
    }
}
