﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using GameCode.Player;
using GameCode.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

namespace GameCode.GameState
{
    public class SerializedStateMap : Dictionary<int, ALevelStaticManger.ASerializedState>
    {
        public SerializedStateMap() : base()
        {
        }

        public SerializedStateMap(Dictionary<int, ALevelStaticManger.ASerializedState> baseDict)
        {
            foreach (var entry in baseDict)
            {
                this.Add(entry.Key, entry.Value);
            }
        }

        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedStateMap);
            var serializedStateMap = instance as SerializedStateMap;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();

            bucketData.AddRange(SerializationUtils.ToBytes(serializedStateMap));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var serializedStateMapBase = new Dictionary<int, ALevelStaticManger.ASerializedState>();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;

            readIndex += SerializationUtils.ToDictionary(dataArray, readIndex, out serializedStateMapBase);
            var serializedStateMap = new SerializedStateMap(serializedStateMapBase);

            return (true, serializedStateMap);
        }
    }

    /// <summary>
    /// Use empty key to indicate "world global" actor
    /// </summary>
    public class ActorSnapshotChunkBuckets : Dictionary<string, Dictionary<ulong, AActorSnapshot>>
    {
    }

    public class GameSnapshot : ICloneable
    {
        public string WorldName;
        public bool ForceFirstCheckpoint;
        public int CheckpointID;
        public Vector3 PlayerPosition;
        public Quaternion PlayerRotation;

        public Dictionary<System.Type, IGameComponent> GameComponents = new Dictionary<System.Type, IGameComponent>();
        public Dictionary<int, PlayerData> PlayerDatas = new Dictionary<int, PlayerData>();

        public SerializedStateMap GlobalStates = new SerializedStateMap();
        public Dictionary<int, SerializedStateMap> WorldStates = new Dictionary<int, SerializedStateMap>();
        public Dictionary<int, SerializedStateMap> ChunkStates = new Dictionary<int, SerializedStateMap>();

        public Dictionary<InstanceID, AActorSnapshot> IdToActorMap = new Dictionary<InstanceID, AActorSnapshot>(); 
        public Dictionary<InstanceID, WorldChunkID> ActorToChunkMap = new Dictionary<InstanceID, WorldChunkID>(); 
        public Dictionary<WorldChunkID, List<InstanceID>> ChunkToActorMap = new Dictionary<WorldChunkID, List<InstanceID>>();

        public void AddOrReplace(AActorSnapshot snapshot, int chunkSceneID)
        {
            Remove(snapshot.InstanceID);
            Add(snapshot, chunkSceneID);
        }
        
        public void Remove(InstanceID snapshotID)
        {
            IdToActorMap.Remove(snapshotID);

            if (ActorToChunkMap.TryGetValue(snapshotID, out var worldChunkID))
            {
                if (ChunkToActorMap.TryGetValue(worldChunkID, out var instanceIdList))
                {
                    instanceIdList.Remove(snapshotID);
                    if (instanceIdList.Count == 0)
                    {
                        ChunkToActorMap.Remove(worldChunkID);
                    }
                }

                ActorToChunkMap.Remove(snapshotID);
            }
        }

        public void Add(AActorSnapshot snapshot, int chunkSceneID)
        {
            var snapshotID = snapshot.InstanceID;
            IdToActorMap[snapshotID] = snapshot;

            WorldChunkID worldChunkID = new WorldChunkID(snapshot.InstanceID.WorldID, chunkSceneID);
            ActorToChunkMap[snapshotID] = worldChunkID;

            if (ChunkToActorMap.TryGetValue(worldChunkID, out var instanceIdList))
            {
                instanceIdList.Add(snapshotID);
            }
            else
            {
                instanceIdList = new List<InstanceID>();
                instanceIdList.Add(snapshotID);
                ChunkToActorMap[worldChunkID] = instanceIdList;
            }
        }
        /// <summary>
        /// TODO: serialization/deserialization REALLY needs codegen with custom attributes and option to create custom Serialization/Deserialization function ONLY when needed!
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="functionHandle"></param>
        /// <returns></returns>
        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is GameSnapshot);
            var gs = instance as GameSnapshot;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            bucketData.AddRange(SerializationUtils.ToBytes(gs.WorldName));
            bucketData.AddRange(BitConverter.GetBytes(gs.ForceFirstCheckpoint));
            bucketData.AddRange(BitConverter.GetBytes(gs.CheckpointID));
            bucketData.AddRange(SerializationUtils.ToBytes(gs.PlayerPosition));
            bucketData.AddRange(SerializationUtils.ToBytes(gs.PlayerRotation));

            SerializationSystem.Serialize(gs.GameComponents.Values.ToList(), out SerializedBucket gameComponentsListBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(gameComponentsListBucket));
            
            bucketData.AddRange(SerializationUtils.ToBytes(gs.PlayerDatas));
            
            SerializationSystem.Serialize(gs.GlobalStates, out SerializedBucket globalStatesBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(globalStatesBucket));

            bucketData.AddRange(SerializationUtils.ToBytes(gs.WorldStates));
            bucketData.AddRange(SerializationUtils.ToBytes(gs.ChunkStates));
            
            {//handle actor snapshots in a way that can be deconstructed
                var actorInstanceIDList = gs.IdToActorMap.Keys.ToList();
                var actorSnapshotList = new List<AActorSnapshot>();
                var actorPlacementList = new TrivialTypeList<WorldChunkID>();
                for (int i = 0; i < actorInstanceIDList.Count; i++)
                {
                    var id = actorInstanceIDList[i];
                    actorSnapshotList.Add(gs.IdToActorMap[id]);
                    actorPlacementList.Add(gs.ActorToChunkMap[id]);
                }

                SerializationSystem.Serialize(actorSnapshotList, out SerializedBucket actorSnapshotListBucket);
                bucketData.AddRange(SerializationUtils.ToBytes(actorSnapshotListBucket));

                SerializationSystem.Serialize(actorPlacementList, out SerializedBucket actorPlacementListBucket);
                bucketData.AddRange(SerializationUtils.ToBytes(actorPlacementListBucket));
            }
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            if (bucket.Header.Version == 1)
            {
                return DeserializerVersion1(bucket);
            }
            else
            {
                Debug.LogError($"Version {bucket.Header.Version} isn't supported");
            }
            
            return (false, null);
        }

        public static (bool, System.Object) DeserializerVersion1(SerializedBucket bucket)
        {
            var gs = new GameSnapshot();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            //read semi-fixed data
            readIndex += SerializationUtils.ToString(dataArray, readIndex, out gs.WorldName);
            gs.ForceFirstCheckpoint = BitConverter.ToBoolean(dataArray, readIndex);
            readIndex += sizeof(bool);
            gs.CheckpointID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);
            readIndex += SerializationUtils.ToVector3(dataArray, readIndex, out gs.PlayerPosition);
            readIndex += SerializationUtils.ToQuaternion(dataArray, readIndex, out gs.PlayerRotation);
            
            //read we-hell-don't-know-whats-there-data
            var gameComponents = SerializationUtils.ReadObjectList<IGameComponent>(ref readIndex, dataArray);
            for (int i = 0; i < gameComponents.Count; i++)
            {
                gs.GameComponents.Add(gameComponents[i].GetType(), gameComponents[i]);
            }
            
            readIndex += SerializationUtils.ToDictionary(dataArray, readIndex, out gs.PlayerDatas);
            
            readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex, out SerializedBucket serializedStateMapBucket);
            SerializationSystem.Deserialize(serializedStateMapBucket, out System.Object serializedStateMapInstance);
            gs.GlobalStates = serializedStateMapInstance as SerializedStateMap;

            readIndex += SerializationUtils.ToDictionary(dataArray, readIndex, out gs.WorldStates);
            readIndex += SerializationUtils.ToDictionary(dataArray, readIndex, out gs.ChunkStates);
            
            {//handle actor snapshots in a way that can be deconstructed
                readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex,
                    out SerializedBucket actorSnapshotListBucket);
                SerializationSystem.Deserialize(actorSnapshotListBucket, out System.Object actorSnapshotListInstance);
                var actorSnapshots = ((IList) actorSnapshotListInstance).Cast<AActorSnapshot>().ToList();

                readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex,
                    out SerializedBucket actorPlacementListBucket);
                SerializationSystem.Deserialize(actorPlacementListBucket, out System.Object actorPlacementListInstance);
                var actorPlacements = (TrivialTypeList<WorldChunkID>)actorPlacementListInstance;

                for (int i = 0; i < actorSnapshots.Count; i++)
                {
                    gs.AddOrReplace(actorSnapshots[i], actorPlacements[i].ChunkID);
                }
            }

            return (true, gs);
        }

        public object Clone()
        {
            // //TODO: refactor this to more runtime friendly way of cloning
            // var cloneBucket = Serializer(this, 0);
            // var cloneOutcome = Deserializer(cloneBucket.Item2);
            //
            // return cloneOutcome.Item2 as GameSnapshot;

            SerializationSystem.Serialize(this, out var cloneBucket);
            SerializationSystem.Deserialize(cloneBucket, out GameSnapshot clone);

            return clone;
        }
    }
}