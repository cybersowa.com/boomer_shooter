﻿using GameCode.General;
using GameCode.Levels.Metadata;
using UnityEngine;

namespace GameCode.GameState
{
    [CreateAssetMenu(fileName = nameof(GameSnapshotPreset), menuName = Const.MenuNameGameState + nameof(GameSnapshotPreset), order = 0)]
    public class GameSnapshotPreset : ScriptableObject
    {
        public WorldSceneMetadata WorldMetadata;
        public bool ForceFirstCheckpoint;
        public int CheckpointID;
        public Vector3 PlayerPosition;

        public GameSnapshot CreateSnapshot()
        {
            var snapshot = new GameSnapshot();
            snapshot.WorldName = WorldMetadata.SceneFileName;
            snapshot.ForceFirstCheckpoint = ForceFirstCheckpoint;
            snapshot.CheckpointID = CheckpointID;
            snapshot.PlayerPosition = PlayerPosition;

            return snapshot;
        }
    }
}