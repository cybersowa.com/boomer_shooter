﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Enemies;
using GameCode.GameState;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    public class SimpleTurretSpawner : ALevelStaticManger
    {
        public Turret TurretPrefab;

        private Turret m_turretInstance;

        public override ASerializedState GenerateSerializedState()
        {
            SerializedState state = new SerializedState();
            state.ID = ID;
            state.IsAlive = m_turretInstance != null;
            if (state.IsAlive)
            {
                state.turretSnapshot = m_turretInstance.CreateSnapshotForCurrentState() as Turret.SerializedSnapshot;
            }
            else
            {
                state.turretSnapshot = null;
            }
            
            return state;
        }

        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            if (serializedStateMap.TryGetValue(ID, out ASerializedState serializedState))
            {
                Assert.IsTrue(serializedState is SerializedState);
                var turretState = serializedState as SerializedState;
                if (turretState.IsAlive)
                {
                    m_turretInstance = Instantiate(TurretPrefab, transform.position, transform.rotation);
                    m_turretInstance.Load(turretState.turretSnapshot);
                }
            }
            else
            {
                m_turretInstance = Instantiate(TurretPrefab, transform.position, transform.rotation);
                m_turretInstance.Load(TurretPrefab.CreateDefaultSnapshot());
            }
        }

        public override void ClearState()
        {
            if (m_turretInstance != null)
            {
                Destroy(m_turretInstance.gameObject);
            }
        }

        [Serializable]
        public class SerializedState : ASerializedState
        {
            public bool IsAlive = true;
            public Turret.SerializedSnapshot turretSnapshot;
            
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;
                clone.IsAlive = IsAlive;
                clone.turretSnapshot = (Turret.SerializedSnapshot) turretSnapshot.Clone();

                return clone;
            }
        }
        
        

        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedState);
            var state = instance as SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            SerializationSystem.Serialize(state.turretSnapshot, out SerializedBucket turretBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(turretBucket));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);
            readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex, out SerializedBucket turretBucket);
            SerializationSystem.Deserialize(turretBucket, out System.Object turretInstance);
            state.turretSnapshot = turretInstance as Turret.SerializedSnapshot;

            return (true, state);
        }
    }
}