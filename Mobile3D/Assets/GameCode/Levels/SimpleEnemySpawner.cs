﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameCode.ActorsDeclarations;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Enemies;
using GameCode.GameState;
using GameCode.Pooling;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    public class SimpleEnemySpawner : ALevelStaticManger
    {
        public ActorPrefabReference EnemyPrefabRef;

        private InstanceID m_EnemyInstanceID;
        private bool HasSpawned = false;

        public override ASerializedState GenerateSerializedState()
        {
            SerializedState state = new SerializedState();
            state.ID = ID;
            state.EnemyInstanceID = m_EnemyInstanceID;
            state.HasSpawned = HasSpawned;
            
            return state;
        }

        public bool GetEnemyInstance(out ASessionActor enemy)
        {
            enemy = null;

            var allActors = Game.CurrentWorld.RuntimeObjectsDatabase.All<ASessionActor>();
            for (int i = 0; i < allActors.Count; i++)
            {
                if (allActors[i].InstanceID == m_EnemyInstanceID)
                {
                    enemy = allActors[i];
                    return true;
                }
            }

            return false;
        }

        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            bool hasPrefab = ActorPrefabRegistry.Instance.TryGet(EnemyPrefabRef, out ASessionActor enemyPrefab);
            Unity.Assertions.Assert.IsTrue(hasPrefab);

            bool shouldSpawn = true;
            
            if (serializedStateMap.TryGetValue(ID, out ASerializedState serializedState))
            {
                Assert.IsTrue(serializedState is SerializedState);
                var spawnerSnapshot = serializedState as SerializedState;

                HasSpawned = spawnerSnapshot.HasSpawned;
                m_EnemyInstanceID = spawnerSnapshot.EnemyInstanceID;
            }

            if (HasSpawned == false)
            {
                Actor.Spawn(enemyPrefab, transform.position, transform.rotation, WorldManager.Current, out m_EnemyInstanceID);
                HasSpawned = true;
            }
        }

        public override void ClearState()
        {
            m_EnemyInstanceID = InstanceID.Invalid;
            HasSpawned = false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position + Vector3.up, "EnemyGizmo.png", true);
        }

        public class SerializedState : ALevelStaticManger.ASerializedState
        {
            public InstanceID EnemyInstanceID;
            public bool HasSpawned = false;
            
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;
                clone.EnemyInstanceID = EnemyInstanceID;
                clone.HasSpawned = HasSpawned;

                return clone;
            }
        }
        

        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedState);
            var state = instance as SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            bucketData.AddRange(SerializationUtils.ToBytes(state.EnemyInstanceID));
            bucketData.AddRange(BitConverter.GetBytes(state.HasSpawned));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);
            readIndex += SerializationUtils.ToInstanceID(dataArray, readIndex, out state.EnemyInstanceID);
            state.HasSpawned = BitConverter.ToBoolean(dataArray, readIndex);
            readIndex += sizeof(bool);

            return (true, state);
        }
    }
}