﻿using System;
using System.Collections.Generic;
using GameCode.ActorsDeclarations;
using GameCode.GameState;
using GameCode.General;
using GameCode.Levels.Metadata;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

//title ide: F.U.N. - Fucking Unbelievable Nightmare

namespace GameCode.Levels
{
    public class ChunkLoadingSystem
    {
        public WorldSceneMetadata WorldMetadata;

        public bool IsLoading { get; set; } = false;
        public float LoadedPercentage { get; set; } = 0.0f;

        private List<SingleChunkLoadingState> m_CurrentTasks = new List<SingleChunkLoadingState>();
        /// <summary>
        /// Counts only fully loaded.
        /// </summary>
        public readonly Dictionary<ChunkSceneMetadata, ChunkManager> FullyLoadedChunks = new Dictionary<ChunkSceneMetadata, ChunkManager>();

        public void UpdateLoadingStats()
        {
            if (m_CurrentTasks.Count == 0)
            {
                IsLoading = false;
                LoadedPercentage = 1.0f;
                return;
            }

            float fCount = m_CurrentTasks.Count;
            float progressSum = 0.0f;
            for (int i = 0; i < m_CurrentTasks.Count; i++)
            {
                progressSum += m_CurrentTasks[i].Handle.progress;
            }

            IsLoading = true;
            LoadedPercentage = progressSum / fCount;
        }

        public void UpdateLoadingTasks(Vector3 playerPosition)
        {
            for (int i = 0; i < WorldMetadata.AllChunks.Count; i++)
            {
                var chunk = WorldMetadata.AllChunks[i];

                bool isLoaded = FullyLoadedChunks.ContainsKey(chunk);
                bool wantLoaded = chunk.IsPointWithinLoadZones(playerPosition);
                bool keepLoaded = wantLoaded || chunk.IsPointWithinUnloadPreventionZones(playerPosition);
                bool wantUnload = keepLoaded == false;
                
                {//check if we have a task already running for the scene
                    var chunkLoadingTaskIndex = m_CurrentTasks.FindIndex(x => x.Target == chunk);
                    if (chunkLoadingTaskIndex >= 0)
                    {
                        var task = m_CurrentTasks[chunkLoadingTaskIndex];
                        if (task.IsLoading && wantLoaded)
                        {
                            //DO NOTHING
                            continue;
                        }

                        if (!task.IsLoading && wantUnload)
                        {
                            //DO NOTHING
                            continue;
                        }

                        Debug.LogError(
                            $"{chunk.SceneFileName}: is task loading scene? = {task.IsLoading}, {nameof(wantLoaded)}={wantLoaded}, {nameof(wantUnload)}={wantUnload}, {nameof(isLoaded)}={isLoaded}, {nameof(playerPosition)}={playerPosition}");
                        continue;
                    }
                }
                
                {//here we know there are no tasks running for selected chunk, so we can simply compare current state and current desire
                    if (keepLoaded && isLoaded)
                    {
                        //we want loaded state and we have loaded state
                        continue;
                    }

                    if (!wantLoaded && !isLoaded)
                    {
                        //we don't want loaded state and we have unloaded state
                        continue;
                    }
                }
                
                {//here we know there are no tasks running and we know the desired state differs from actual state
                    var newTask = new SingleChunkLoadingState()
                    {
                        Target = chunk,
                        IsLoading = wantLoaded
                    };

                    if (newTask.IsLoading)
                    {
                        newTask.Handle = SceneManager.LoadSceneAsync(chunk.SceneFileName, LoadSceneMode.Additive);
                    }
                    else
                    {
                        var worldManager = WorldManager.Current;
                        var chunkManager = FullyLoadedChunks[chunk];

                        var allChunkActors = Game.GetAllActorsWithinChunk(worldManager, chunk);
                        for (int j = allChunkActors.Count - 1; j >= 0; j--)
                        {
                            Actor.Despawn(allChunkActors[j], true);
                        }
                        
                        worldManager.RuntimeObjectsDatabase.Remove<ChunkManager>(chunkManager);
                        FullyLoadedChunks.Remove(chunk);
                        
                        newTask.Handle = SceneManager.UnloadSceneAsync(chunk.SceneFileName); 
                    }
                    
                    m_CurrentTasks.Add(newTask);
                }
            }
            
            //inversed so we can safely remove entries
            for (int i = m_CurrentTasks.Count - 1; i >= 0; i--)
            {
                var task = m_CurrentTasks[i];
                if (task.Handle.isDone)
                {
                    if (task.IsLoading)
                    {
                        try
                        {
                            LoadSavedStateForChunk(task.Target);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError($"Exception when loading static managers states for {task.Target.SceneFileName} | {e}");
                        }
                    }
                    
                    m_CurrentTasks.RemoveAt(i);
                }
            }
        }

        private void LoadSavedStateForChunk(ChunkSceneMetadata chunkSceneMetadata)
        {
            var scene = SceneManager.GetSceneByName(chunkSceneMetadata.SceneFileName);
            var rootObjects = scene.GetRootGameObjects();
            bool debugWasManagerFound = false;
            ChunkManager chunkManager = null;
            
            for (int i = 0; i < rootObjects.Length; i++)
            {
                //for perf reasons we assume the manager is in the root of the scene. Also for perf reason it should be the first one in the hierarchy
                var rootObject = rootObjects[i];
                if (rootObject.tag.Equals(Const.Tags.ChunkManager))
                {
                    chunkManager = rootObject.GetComponent<ChunkManager>();
                    FullyLoadedChunks.Add(chunkSceneMetadata, chunkManager);
                    var worldManager = WorldManager.Current;
                    worldManager.RuntimeObjectsDatabase.Add<ChunkManager>(chunkManager);
                    SerializedStateMap chunkSerializedMap = null;
                    if (Game.RuntimeState.ChunkStates.TryGetValue(chunkSceneMetadata.SceneGUID,
                        out chunkSerializedMap) == false)
                    {
                        chunkSerializedMap = new SerializedStateMap();
                        Game.RuntimeState.ChunkStates[chunkSceneMetadata.SceneGUID] = chunkSerializedMap;//optional, as we are just loading
                    }
                    
                    for (int j = 0; j < chunkManager.AllChunkStaticManagers.Count; j++)
                    {
                        var staticManager = chunkManager.AllChunkStaticManagers[j];
                        staticManager.LoadState(chunkSerializedMap);
                    }
                    
                    debugWasManagerFound = true;
                    break;
                }
            }
            
            //TODO: destroy all spawned ASessionActors so we can start with clean state <- might be not needed?
            
            //spawn actors based on information from runtime GameSnapshot. Also: check if Actor for specific InstanceID already was spawned
            var worldGlobalChunkID = new WorldChunkID(Game.CurrentWorld.Metadata.SceneGUID, chunkManager.Metadata.SceneGUID);
            if (Game.RuntimeState.ChunkToActorMap.TryGetValue(worldGlobalChunkID, out var actorMap))
            {
                for (int i = 0; i < actorMap.Count; i++)
                {
                    var actorInstanceID = actorMap[i];
                    var actorSnapshot = Game.RuntimeState.IdToActorMap[actorInstanceID];
                    bool hasPrefab = ActorPrefabRegistry.Instance.TryGet(actorSnapshot.PrefabID, out ASessionActor actorPrefab);
                    Unity.Assertions.Assert.IsTrue(hasPrefab);
                    
                    Actor.Respawn(actorPrefab, actorSnapshot.Position, actorSnapshot.Rotation, actorSnapshot);
                }
            }
            
            if (debugWasManagerFound == false)
            {
                throw new Exception($"{chunkSceneMetadata.SceneFileName} doesn't contain properly setup ChunkManger. " +
                                    $"Check if manager is in root of the hierarchy and that it has tag {Const.Tags.ChunkManager} selected." +
                                    $"Also check if scene is actually loaded: " +
                                    $"{nameof(scene.name)}={scene.name}" +
                                    $"{nameof(scene.isLoaded)}={scene.isLoaded}" +
                                    $"{nameof(scene.handle)}={scene.handle}" +
                                    $"{nameof(scene.buildIndex)}={scene.buildIndex}" +
                                    $"{nameof(scene.isDirty)}={scene.isDirty}" +
                                    $"{nameof(scene.isSubScene)}={scene.isSubScene}" +
                                    $"{nameof(scene.rootCount)}={scene.rootCount}" +
                                    $"{nameof(scene.IsValid)}={scene.IsValid()}");
            }
        }

        private struct SingleChunkLoadingState
        {
            public ChunkSceneMetadata Target;
            public bool IsLoading;
            public AsyncOperation Handle;
        }
    }
}