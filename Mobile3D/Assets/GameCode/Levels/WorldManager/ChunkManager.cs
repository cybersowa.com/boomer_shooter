﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCode.General;
using GameCode.Levels.Metadata;
using UnityEngine;

namespace GameCode.Levels
{
    public class ChunkManager : MonoBehaviour, IValidRestrictions
    {
        public ChunkSceneMetadata Metadata;
        [Space]
        public List<PlayerCheckpoint> AllChunkCheckpoints = new List<PlayerCheckpoint>();
        public List<ALevelStaticManger> AllChunkStaticManagers = new List<ALevelStaticManger>(); // <- this need to be moved to ChunkManager, 2021.08.12
        
        
        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            IValidRestrictions.Outcome outcome = IValidRestrictions.Outcome.NoChange;
            StringBuilder sb = new StringBuilder();
            
            List<ALevelStaticManger> allChunkStaticManagers = new List<ALevelStaticManger>();
            var allRoots = gameObject.scene.GetRootGameObjects();
            for (int i = 0; i < allRoots.Length; i++)
            {
                allChunkStaticManagers.AddRange(allRoots[i].GetComponentsInChildren<ALevelStaticManger>());
            }

            bool doesAllChunkCheckpointsNeedUpdating = false;
            if (allChunkStaticManagers.Count != AllChunkStaticManagers.Count)
            {
                sb.Append("Number of static managers has changed. Updating AllChunkStaticManagers");
                doesAllChunkCheckpointsNeedUpdating = true;
            }
            else
            {
                for (int i = 0; i < allChunkStaticManagers.Count; i++)
                {
                    if (!AllChunkStaticManagers.Contains(allChunkStaticManagers[i]))
                    {
                        sb.Append("Static managers has changed. Updating AllChunkStaticManagers");
                        doesAllChunkCheckpointsNeedUpdating = true;
                        break;
                    }
                }
            }

            if (doesAllChunkCheckpointsNeedUpdating)
            {
                AllChunkStaticManagers = allChunkStaticManagers;
                outcome = IValidRestrictions.Outcome.Updated;
            }

            return (outcome, sb.ToString());
        }
    }
}