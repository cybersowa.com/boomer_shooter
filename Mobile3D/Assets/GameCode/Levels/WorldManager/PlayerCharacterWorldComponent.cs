﻿using System.Collections.Generic;
using GameCode.Player;

namespace GameCode.Levels
{
    public class PlayerCharacterWorldComponent : AWorldComponent
    {
        public PlayerController PlayerController;
        public PlayerCheckpoint InitialCheckpoint;
        public List<PlayerCheckpoint> AllCheckpoints = new List<PlayerCheckpoint>();
    }
}