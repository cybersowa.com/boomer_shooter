﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCode.GameState;
using GameCode.General;
using GameCode.Levels.Metadata;
using GameCode.Player;
using GameCode.Utils;
using KinematicCharacterController;
using Unity.Assertions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameCode.Levels
{
    public abstract class AWorldComponent : MonoBehaviour
    {
    }

    public class WorldManager : MonoBehaviour, IValidRestrictions
    {
        private static WorldManager m_Current;
        public static WorldManager Current
        {
            get
            {
                if (m_Current == null)
                {
                    m_Current = FindObjectOfType<WorldManager>();
                }

                return m_Current;
            }
        }

        public WorldSceneMetadata Metadata;
        
        /// <summary>
        /// When loading world we need to update this value according to what was saved
        /// </summary>
        [NonSerialized]
        public ulong LastActorInstanceID;
        
        public PlayerController PlayerController;
        public PlayerCheckpoint InitialCheckpoint;
        public List<PlayerCheckpoint> AllCheckpoints = new List<PlayerCheckpoint>(); // <- this need to be moved to ChunkManager, 2021.08.12
        public List<ALevelStaticManger> AllStaticManagers = new List<ALevelStaticManger>(); // <- this need to be moved to ChunkManager, 2021.08.12

        public ChunkLoadingSystem ChunkLoadingSystem = new ChunkLoadingSystem();
        public RuntimeObjectsDatabase RuntimeObjectsDatabase = new RuntimeObjectsDatabase();// TODO: make it serialized with the world

        private Dictionary<System.Type, AWorldComponent> m_Components = new Dictionary<Type, AWorldComponent>();

        private void Awake()
        {
            var allComponents = GetComponents<AWorldComponent>();
            for (int i = 0; i < allComponents.Length; i++)
            {
                m_Components.Add(allComponents[i].GetType(), allComponents[i]);
            }
        }

        private void Update()
        {
            if (m_Components.TryGetValue(typeof(PlayerCharacterWorldComponent), out var component))
            {
                var playerWorldComponent = component as PlayerCharacterWorldComponent;
                var playerPosition = playerWorldComponent.PlayerController.Character.transform.position;
                ChunkLoadingSystem.UpdateLoadingTasks(playerPosition);
            }
        }

        public bool TryGet<T>(out T component) where T : AWorldComponent
        {
            bool has = m_Components.TryGetValue(typeof(T), out var aComponent);
            component = (T) aComponent;
            return has;
        }

        public void ResetToLastCheckpoint()
        {
            GameState.Game.LoadSnapshot(GameState.Game.CheckpointState);
        }

        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            StringBuilder messageBuilder = new StringBuilder();
            bool neededUpdate = false;
            bool hasErrorsInSetup = false;

            ValidateStaticManagers(ctx, messageBuilder, out bool staticManagersHaveChanged);
            neededUpdate |= staticManagersHaveChanged;
            
            //move to proper component
            // ValidateCheckpoints(ctx, messageBuilder, out var checkpointsNeededUpdate, out var checkpoiontsHaveErrorInSetup);
            // neededUpdate |= checkpointsNeededUpdate;
            // hasErrorsInSetup |= checkpoiontsHaveErrorInSetup;

            ValidateSubSceneMangers(ctx, messageBuilder, out bool subSceneManagersHaveChanged);
            neededUpdate |= subSceneManagersHaveChanged;
            
            if (neededUpdate)
            {
                return SetupValidationHelper.Changed(messageBuilder.ToString());
            }
            else if(hasErrorsInSetup)
            {
                return SetupValidationHelper.Fail(messageBuilder.ToString());
            }
            
            return SetupValidationHelper.NoChange(messageBuilder.ToString());
        }
/*
        private void ValidateCheckpoints(IValidationContext ctx, StringBuilder messageBuilder, out bool neededUpdate, out bool hasErrorInSetup)
        {
            List<PlayerCheckpoint> checkpoints = new List<PlayerCheckpoint>();

            var scene = gameObject.scene;
            for (int i = 0; i < scene.rootCount; i++)
            {
                checkpoints.AddRange(
                    scene.GetRootGameObjects()[i].GetComponentsInChildren<PlayerCheckpoint>(true)
                );
            }

            neededUpdate = false;
            if (checkpoints.Count != AllCheckpoints.Count)
            {
                neededUpdate = true;
            }
            else
            {
                for (int i = 0; i < AllCheckpoints.Count; i++)
                {
                    if (checkpoints.Contains(AllCheckpoints[i]) == false)
                    {
                        neededUpdate = true;
                        break;
                    }
                }
            }

            if (neededUpdate)
            {
                AllCheckpoints = checkpoints;
                messageBuilder.Append($"Updated checkpoint list in {name}. ");
            }

            hasErrorInSetup = false;

            PlayerCheckpoint initialCheckpoint = null;
            for (int i = 0; i < AllCheckpoints.Count; i++)
            {
                if (AllCheckpoints[i].InitialCheckpoint)
                {
                    if (AllCheckpoints[i] != InitialCheckpoint)
                    {
                        if (initialCheckpoint == null)
                        {
                            neededUpdate = true;
                            messageBuilder.Append($"Updated initial checkpoint in {name}. ");
                        }
                        else
                        {
                            hasErrorInSetup = true;
                            messageBuilder.Append(
                                $"There are many checkpoints set as initial: {initialCheckpoint.name} and {AllCheckpoints[i].name}. ");
                        }
                    }
                    
                    initialCheckpoint = AllCheckpoints[i];
                }
            }

            if (initialCheckpoint == null)
            {
                hasErrorInSetup = true;
                messageBuilder.Append($"There's no checkpoint set as initial");
            }

            if (neededUpdate)
            {
                InitialCheckpoint = initialCheckpoint;
            }
        }
*/
        private void ValidateStaticManagers(IValidationContext ctx, StringBuilder sb, out bool neededUpdate)
        {
            neededUpdate = false;
            
            if (SetupValidationHelper.SceneObjectListValidation(gameObject.scene, AllStaticManagers,
                out List<ALevelStaticManger> staticMangers))
            {
                sb.Append(
                    $"Static managers on scene {gameObject.scene} differs from previously serialized list. ");
                AllStaticManagers = staticMangers;
                neededUpdate = true;
            }

            HashSet<int> ids = new HashSet<int>();

            for (int i = 0; i < AllStaticManagers.Count; i++)
            {
                var manager = AllStaticManagers[i];

                bool needsGenerating = false;
                if (manager.ID == 0)
                {
                    sb.Append(
                        $"Generating ID for: {manager.name}. Adding object to dirty list. ");
                    needsGenerating = true;
                }
                
                if (ids.Contains(manager.ID))
                {
                    sb.Append(
                        $"Found ID collision: {manager.name} used ID {manager.ID}. Regenerating and adding object to dirty list. ");
                    needsGenerating = true;
                }
                else
                {
                    ids.Add(manager.ID);
                }
                
                if(needsGenerating){
                    neededUpdate = true;
                    manager.AssignNewID(ID.GenerateNewIntUID());
                    ctx.AddToDirtyList(manager);
                    //re-evaluate if new ID doesn't have collisions
                    i--;
                }
            }
        }

        private void ValidateSubSceneMangers(IValidationContext ctx, StringBuilder sb, out bool neededUpdate)
        {
            neededUpdate = false;
            
            //TODO: check if validation on chunks can be called independently? - 2021.08.30
            // if (SetupValidationHelper.SceneObjectListValidation(gameObject.scene, AllChunks,
            //     out List<Obsolete_ChunkManager> subSceneMangers))
            // {
            //     sb.Append(
            //         $"SubScene managers on scene {gameObject.scene} differs from previously serialized list. ");
            //     AllChunks = subSceneMangers;
            //     neededUpdate = true;
            // }
        }

        public void OnCheckpointAchieved(PlayerCheckpoint checkpoint)
        {
            // var savedGameState = GameState.Game.CheckpointState;
            //
            // savedGameState.CheckpointID = checkpoint.ID;
            //
            // WorldManager worldManager = WorldManager.Current;
            // if (!worldManager.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
            // {
            //     Assert.IsTrue(false);
            //     return;
            // }
            //
            // savedGameState.PlayerPosition = playerCharacterWorldComponent.PlayerController.Character.Motor.InitialSimulationPosition;
            // savedGameState.PlayerRotation = playerCharacterWorldComponent.PlayerController.Character.Motor.InitialSimulationRotation;
            //
            //
            // for (int i = 0; i < AllStaticManagers.Count; i++)
            // {
            //     var staticManagerState = AllStaticManagers[i].GenerateSerializedState();
            //     savedGameState.ManagersStates[staticManagerState.ID] = staticManagerState;
            // }

            var checkpointSnapshot = Game.CreateSnapshot();
            Game.CheckpointState = checkpointSnapshot;
        }

        public ulong GetNextInstanceID()
        {
            LastActorInstanceID++;
            return LastActorInstanceID;
        }
    }
}