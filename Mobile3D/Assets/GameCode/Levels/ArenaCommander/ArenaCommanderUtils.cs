﻿using System;

namespace GameCode.Levels.ArenaCommander
{
    public struct ArenaCell
    {
        public ArenaCellOccupancyType Occupancy;
        public ASessionActor Occupant;
        public ArenaCellInfo Info;

        static public ArenaCell GetNewStaticallyBlocked()
        {
            return new ArenaCell()
            {
                Occupancy = ArenaCellOccupancyType.StaticallyBlocked,
                Occupant = null
            };
        }
    }
    
    public enum ArenaCellOccupancyType
    {
        Free = 0,
        StaticallyBlocked,
        HasOccupant
    }

    [Flags]
    public enum ArenaCellInfo
    {
        Default = 0,
        InPlayerAim = 1 << 1,
        DamageThread = 1 << 2
    }

    public enum ArenaCellNavInfo
    {
        Unknown = 0,
        StartingPoint = 1,
        Traversable = 2,
        Blocked = 4
    }
}