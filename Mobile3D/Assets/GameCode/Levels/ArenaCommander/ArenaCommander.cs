using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameCode.Enemies.AI;
using GameCode.Enemies;
using GameCode.General;
using GameCode.Levels;
using GameCode.Levels.ArenaCommander;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class ArenaCommander : MonoBehaviour
{
    const float CellSize = 4.0f;
    private const bool AllowAttacks = true;

    [FormerlySerializedAs("Arena")]
    public RectInt ArenaSize;

    public ArenaCell[,] RuntimeArena; 

    public float RangersPreferredAttackDistance = 10.0f;
    public float RangerAttackDistanceRange = 5.0f;
    public Rect PlayerAimThreadArea = Rect.MinMaxRect(-4.5f, 0f, 4.5f, 100f);

    public int MaxAttackersAtOnce = 2;

    public float TickEvery = 1.0f;
    public float MaxMovementRequestDuration = 7.5f;
    
    public List<SimpleEnemySpawner> ArenaSpawners = new List<SimpleEnemySpawner>();

    private float m_TimeToTick;
    
    #region UnityCallbacks
    
    // Start is called before the first frame update
    void Start()
    {
        RuntimeArena = new ArenaCell[ArenaSize.width, ArenaSize.height];
    }

    private void ClearRuntimeArenaFlags(Transform playerCharacterTransform)
    {
        //firstly clear flags so we don't have to worry about leaking state
        for (int x = 0; x < RuntimeArena.GetLength(0); x++)
        {
            for (int z = 0; z < RuntimeArena.GetLength(1); z++)
            {
                ref var cell = ref RuntimeArena[x, z];
                cell.Occupancy = ArenaCellOccupancyType.Free;
                cell.Occupant = null;
                cell.Info = ArenaCellInfo.Default;
                var cellPosition = GetCellPosition(x, z);
                if (IsPointInFlatThreadZone(cellPosition, PlayerAimThreadArea, playerCharacterTransform))
                {
                    cell.Info |= ArenaCellInfo.InPlayerAim;
                }
            }
        }
    }

    void Update()
    {
        var glm = WorldManager.Current;
        if (!glm.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
        {
            Assert.IsTrue(false);
            return;
        }
        
        var playerCharacterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;
        var currentPlayerPosition = playerCharacterTransform.position;
        if (!IsPointWithinArena(currentPlayerPosition))
        {
            return;
        }
        
        ClearRuntimeArenaFlags(playerCharacterTransform);

        var unitsToCommand = GetAllUnitControlledByThisCommander();
        foreach(var unit in unitsToCommand) ClearFinishedAIRequest(unit);

        int attackTokensAvailable = MaxAttackersAtOnce;

        var unitsCommandedInUpdate = new List<AIEnemyPawn>();
        
        CommandPass_UnitsAlreadyAttacking(unitsToCommand, unitsCommandedInUpdate, ref attackTokensAvailable);
        unitsToCommand.RemoveAll(unitsCommandedInUpdate);
        unitsCommandedInUpdate.Clear();
        
        CommandPass_RunFromPlayerAim(unitsToCommand, unitsCommandedInUpdate);
        unitsToCommand.RemoveAll(unitsCommandedInUpdate);
        unitsCommandedInUpdate.Clear();

        CommandPass_AttackPlayer(unitsToCommand, unitsCommandedInUpdate, ref attackTokensAvailable);
        unitsToCommand.RemoveAll(unitsCommandedInUpdate);
        unitsCommandedInUpdate.Clear();
        
        CommandPass_UnitsWhichShouldKeepMoving(unitsToCommand, unitsCommandedInUpdate, MaxMovementRequestDuration);
        unitsToCommand.RemoveAll(unitsCommandedInUpdate);
        unitsCommandedInUpdate.Clear();
        
        CommandPass_MoveUnits(unitsToCommand, unitsCommandedInUpdate);
        unitsToCommand.RemoveAll(unitsCommandedInUpdate);
        unitsCommandedInUpdate.Clear();
    }

    private void CommandPass_MoveUnits(List<AIEnemyPawn> unitsToCommand, List<AIEnemyPawn> unitsCommandedInUpdate)
    {
        var glm = WorldManager.Current;
        if (!glm.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
        {
            Assert.IsTrue(false);
            return;
        }
        var playerCharacterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;
        
        foreach (var unit in unitsToCommand)
        {
            var unitPosition = unit.transform.position;
            var unitPositionIndex = GetArenaCellIndexForPositionClamped(unitPosition);
            GetMoveTargetCell(unitPositionIndex, out Vector2Int targetPosIndex);
            SubmitMoveRequest(targetPosIndex, unit, playerCharacterTransform);
            unitsCommandedInUpdate.Add(unit);
        }
    }

    private void CommandPass_UnitsWhichShouldKeepMoving(List<AIEnemyPawn> unitsToCommand, List<AIEnemyPawn> unitsCommandedInUpdate, float maxMovementRequestDuration)
    {
        float currentTime = Time.realtimeSinceStartup;
        foreach (var unit in unitsToCommand)
        {
            if (unit.HasAIRequest && unit.CurrentAIRequest is AIMoveToRequest moveRequest)
            {
                float timeRunning = currentTime - moveRequest.RequestTime;
                if (timeRunning > maxMovementRequestDuration)
                {
                    unitsCommandedInUpdate.Add(unit); 
                }
            }
        }
    }

    private void CommandPass_AttackPlayer(List<AIEnemyPawn> unitsToCommand, List<AIEnemyPawn> unitsCommandedInUpdate, ref int attackToken)
    {
        var glm = WorldManager.Current;
        if (!glm.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
        {
            Assert.IsTrue(false);
            return;
        }
        var playerCharacterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;

        foreach (var unit in unitsToCommand)
        {
            float distance = (playerCharacterTransform.transform.position - unit.transform.position).magnitude;
            var attacks = unit.GetAllAvailableAttackInfos();
            int tokensToSpend = 0;
            
            for (int i = 0; i < attacks.Count; i++)
            {
                if (attacks[i].AttackTokenCost > attackToken)
                {
                    continue;
                }
                
                if (attacks[i].AttackRange.InRange(distance))
                {
                    tokensToSpend = Mathf.Max(tokensToSpend, attacks[i].AttackTokenCost);
                }
            }

            if (tokensToSpend > 0)
            {
                attackToken -= tokensToSpend;
                SubmitAttackRequest(unit, playerCharacterTransform.gameObject, tokensToSpend);
                unitsCommandedInUpdate.Add(unit);
            }
        }
    }

    private void CommandPass_RunFromPlayerAim(List<AIEnemyPawn> unitsToCommand, List<AIEnemyPawn> unitsCommandedInUpdate)
    {
        foreach (var unit in unitsToCommand)
        {
            var unitPosition = unit.transform.position;
            var unitPositionIndex = GetArenaCellIndexForPositionClamped(unitPosition);
            ref var unitCell = ref GetCellRef(unitPositionIndex);
            if (unitCell.Info == ArenaCellInfo.InPlayerAim)
            {
                unitsCommandedInUpdate.Add(unit);
            }
        }
        
        var glm = WorldManager.Current;
        if (!glm.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
        {
            Assert.IsTrue(false);
            return;
        }
        var playerCharacterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;

        foreach (var unit in unitsCommandedInUpdate)
        {
            var unitPosition = unit.transform.position;
            var unitPositionIndex = GetArenaCellIndexForPositionClamped(unitPosition);
            GetClosestCell(ArenaCellInfo.Default, unitPositionIndex, out Vector2Int navStartPosIndex);
            GetMoveTargetCell(navStartPosIndex, out Vector2Int targetPosIndex);
            SubmitMoveRequest(targetPosIndex, unit, playerCharacterTransform);
        }
    }

    private void SubmitMoveRequest(Vector2Int targetPosIndex, AIEnemyPawn unit,
        Transform facingTarget)
    {
        ref ArenaCell cell = ref GetCellRef(targetPosIndex);
        cell.Occupancy = ArenaCellOccupancyType.HasOccupant;
        cell.Occupant = unit;
        unit.SetRequest(new AIMoveToRequest()
        {
            RequestTime = Time.realtimeSinceStartup,
            State = AIRequestState.Running,
            MovementTargetPosition = GetCellPosition(targetPosIndex),
            MovementControlFlags = MovementControlFlags.FaceTarget,
            POI = facingTarget
        });
    }

    private void SubmitAttackRequest(AIEnemyPawn unit, GameObject attackTarget, int tokensToSpent)
    {
        unit.SetRequest(new AIAttackRequest()
        {
            AttackTarget = attackTarget,
            RequestTime = Time.realtimeSinceStartup,
            State = AIRequestState.Running,
            TokensSpent = tokensToSpent
        });
    }

    private bool GetMoveTargetCell(Vector2Int navStartPosIndex, out Vector2Int vector2Int)
    {
        vector2Int = navStartPosIndex;

        ArenaCellNavInfo[,] navData = new ArenaCellNavInfo[ArenaSize.width, ArenaSize.height];
        List<Vector2Int> arenaCellIndexes = new List<Vector2Int>(ArenaSize.height * ArenaSize.width);
        Stack<Vector2Int> pointToCheck = new Stack<Vector2Int>();
        pointToCheck.Push(navStartPosIndex);
        navData[navStartPosIndex.x, navStartPosIndex.y] = ArenaCellNavInfo.StartingPoint;

        void FillNavCell(Vector2Int cellIndex)
        {
            if (navData[cellIndex.x, cellIndex.y] == ArenaCellNavInfo.Unknown)
            {
                ref var aCell = ref GetCellRef(cellIndex);
                if ((aCell.Occupancy == ArenaCellOccupancyType.Free)
                    && (aCell.Info == ArenaCellInfo.Default))
                {
                    navData[cellIndex.x, cellIndex.y] = ArenaCellNavInfo.Traversable;
                    arenaCellIndexes.Add(cellIndex);
                    pointToCheck.Push(cellIndex);
                }
            }
        }
        
        while (pointToCheck.Count > 0)
        {
            var cellIndex = pointToCheck.Pop();
            var dirs = Get4DirectionsFrom(cellIndex);
            for (int i = 0; i < dirs.Length; i++)
            {
                if (navData.Contains(dirs[i]))
                {
                    FillNavCell(dirs[i]);
                }
            }
        }

        if (arenaCellIndexes.Count == 0)
        {
            return false;
        }
        
        var glm = WorldManager.Current; //TODO: pass player pos as param
        if (!glm.TryGet<PlayerCharacterWorldComponent>(out var playerCharacterWorldComponent))
        {
            Assert.IsTrue(false);
            return false;
        }
        
        var playerCharacterTransform = playerCharacterWorldComponent.PlayerController.Character.transform;
        var currentPlayerPosition = playerCharacterTransform.position;

        var startPosition = GetCellPosition(navStartPosIndex);
          
        var directionFromPlayer = startPosition - currentPlayerPosition;
        var preferredOffsetToTarget = directionFromPlayer.normalized * RangersPreferredAttackDistance;
        var preferredPosition = preferredOffsetToTarget + currentPlayerPosition;
        var preferredTargetPositionIndex = GetArenaCellIndexForPositionClamped(preferredPosition);

        float distanceFromIdealPosition = float.MaxValue;
        Vector2Int bestPositionIndex = navStartPosIndex;
        for (int acIndex = 0; acIndex < arenaCellIndexes.Count; acIndex++)
        {
            var cellIndex = arenaCellIndexes[acIndex];
                
            var indexOffset = cellIndex - preferredTargetPositionIndex;
            var indexOffsetSqrMagnitude = indexOffset.sqrMagnitude;
            if (indexOffsetSqrMagnitude < distanceFromIdealPosition)
            {
                bestPositionIndex = cellIndex;
                distanceFromIdealPosition = indexOffsetSqrMagnitude;
            }
        }

        vector2Int = bestPositionIndex;
        return true;
    }

    private bool GetClosestCell(ArenaCellInfo desiredCellType, Vector2Int startingIndex, out Vector2Int foundIndex, bool skipStartingPosition = true)
    {
        bool hasIndexBeenFound = false;
        foundIndex = startingIndex;

        List<Vector2Int> indexesToCheck = new List<Vector2Int>();
        bool[,] recurrencyGuard = new bool[ArenaSize.width, ArenaSize.height];
        if (skipStartingPosition)
        {
            recurrencyGuard[startingIndex.x, startingIndex.y] = true;
            var dirs = Get4DirectionsFrom(startingIndex);
            for (int i = 0; i < dirs.Length; i++)
            {
                if(recurrencyGuard.Contains(dirs[i])) indexesToCheck.Add(dirs[i]);
            }
        }
        else
        {
            indexesToCheck.Add(startingIndex);
        }

        while (indexesToCheck.Count > 0)
        {
            var indexPos = indexesToCheck[0];
            indexesToCheck.RemoveAt(0);
            
            ref var cell = ref GetCellRef(indexPos);
            if (cell.Info == desiredCellType)
            {
                foundIndex = indexPos;
                hasIndexBeenFound = true;
                break;
            }
            
            recurrencyGuard[indexPos.x, indexPos.y] = true;
            
            var dirs = Get4DirectionsFrom(indexPos);
            for (int i = 0; i < dirs.Length; i++)
            {
                if (recurrencyGuard.Contains(dirs[i])
                    && !recurrencyGuard.Get(dirs[i]))
                {
                    indexesToCheck.Add(dirs[i]);
                }
            }
        }
        
        return hasIndexBeenFound;
    }

    private Vector2Int[] Get4DirectionsFrom(Vector2Int indexPosition)
    {
        Vector2Int[] dirs = new [] {indexPosition, indexPosition, indexPosition, indexPosition};
        dirs[0].x--;
        dirs[1].x++;
        dirs[2].y--;
        dirs[3].y++;
        return dirs;
    }

    private void CommandPass_UnitsAlreadyAttacking(List<AIEnemyPawn> unitsToCommand, List<AIEnemyPawn> unitsCommandedInUpdate, ref int attackTokens)
    {
        foreach (var unit in unitsToCommand)
        {
            if (unit.HasAIRequest && unit.CurrentAIRequest is AIAttackRequest attackRequest)
            {
                unitsCommandedInUpdate.Add(unit);
                attackTokens -= attackRequest.TokensSpent;
            }
        }
    }

    private void ClearFinishedAIRequest(AIEnemyPawn unit)
    {
        if (unit.HasAIRequest && unit.CurrentAIRequest.State != AIRequestState.Running)
        {
            unit.ClearAIRequest();
        }
    }

    private List<AIEnemyPawn> GetAllUnitControlledByThisCommander()
    {
        List<AIEnemyPawn> list = new List<AIEnemyPawn>();
        for (int i = 0; i < ArenaSpawners.Count; i++)
        {
            if (ArenaSpawners[i].GetEnemyInstance(out var enemy))
            {
                if (enemy is AIEnemyPawn mhthEnemy)
                {
                    list.Add(mhthEnemy);
                }
            }
        }

        return list;
    }

    private bool IsPointInFlatThreadZone(Vector3 point, Rect threadArena, Transform relativeTo)
    {
        point = relativeTo.InverseTransformPoint(point);
        if (threadArena.Contains(point.ToVec2XZ()))
        {
            return true;
        }

        return false;
    }

    private bool IsPointWithinArena(Vector3 position)
    {
        Vector3 centerOffset = new Vector3(
            ArenaSize.width * CellSize * 0.5f + ArenaSize.x * CellSize,
            0.0f,
            ArenaSize.height * CellSize * 0.5f + ArenaSize.y * CellSize
        );

        Vector3 size = new Vector3(
            ArenaSize.width * CellSize,
            10.0f,//arbitrary bounding box height
            ArenaSize.height * CellSize
        );

        Bounds arenaBounds = new Bounds(transform.position + centerOffset, size);

        return arenaBounds.Contains(position);
    }

    private ref ArenaCell GetArenaCellForPositionClamped(Vector3 position)
    {
        Vector2Int index = Vector2Int.zero;
        index = GetArenaCellIndexForPositionClamped(position);

        return ref GetCellRef(index);
    }

    private Vector2Int GetArenaCellIndexForPositionClamped(Vector3 position)
    {
        Vector2Int index;
        GetArenaCellIndexForPosition(position, out index);
        index = index.GetClampedExclusive(ArenaSize.width, ArenaSize.height);
        return index;
    }

    private bool GetArenaCellForPosition(Vector3 position, ref ArenaCell cell)
    {
        if (GetArenaCellIndexForPosition(position, out var index))
        {
            cell = GetCellRef(index);
            return true;
        }

        return false;
    }

    
    
    private ref ArenaCell GetCellRef(Vector3 position)
    {
        return ref GetCellRef(GetArenaCellIndexForPositionClamped(position));
    }
    private ref ArenaCell GetCellRef(Vector2Int index)
    { 
        return ref RuntimeArena[index.x, index.y];
    }

    private bool GetArenaCellIndexForPosition(Vector3 position, out Vector2Int cellIndex)
    {
        //flatten arena
        Vector2 targetPosition = position.ToVec2XZ();
        Vector2 arenaOriginCorner = transform.position.ToVec2XZ();
        arenaOriginCorner.x += ArenaSize.x * CellSize;
        arenaOriginCorner.y += ArenaSize.y * CellSize;

        Vector2 localTargetPosition = targetPosition - arenaOriginCorner;
        localTargetPosition /= CellSize;
        cellIndex = localTargetPosition.ToIntFloor(0.5f);

        if (cellIndex.x < 0 || cellIndex.y < 0)
        {
            return false;
        }

        if (cellIndex.x >= ArenaSize.width || cellIndex.y >= ArenaSize.height)
        {
            return false;
        }

        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position + Vector3.up * 3.0f, "EnemyCommanderGizmo.png", true);
    }

    private void OnDrawGizmosSelected()
    {
        // SnapToCellGrid();

        DrawArenaCells();
    }

    #endregion

    private void DrawArenaCells()
    {
        Color gizmoColor = Color.green;
        gizmoColor.a = 0.5f;
        Color playerAimGizmoColor = Color.yellow;
        playerAimGizmoColor.a = 0.5f;
        Color hasOccupantGizmoColor = Color.cyan;
        hasOccupantGizmoColor.a = 0.5f;
        Gizmos.color = gizmoColor;

        for (int x = 0; x < ArenaSize.width; x++)
        {
            for (int z = 0; z < ArenaSize.height; z++)
            {
                var cellPosition = GetCellPosition(x, z);
                if (RuntimeArena != null)
                {
                    if (RuntimeArena[x, z].Info.HasFlag(ArenaCellInfo.InPlayerAim))
                    {
                        Gizmos.color = playerAimGizmoColor;
                    }
                    else if (RuntimeArena[x, z].Occupancy == ArenaCellOccupancyType.HasOccupant)
                    {
                        Gizmos.color = hasOccupantGizmoColor;
                    }
                    else
                    {
                        Gizmos.color = gizmoColor;
                    }
                }
                
                Gizmos.DrawCube(cellPosition, Vector3.one * CellSize);
            }
        }
    }

    [ContextMenu(nameof(SetChildrenAsArenaSpawners))]
    private void SetChildrenAsArenaSpawners()
    {
        ArenaSpawners = GetComponentsInChildren<SimpleEnemySpawner>().ToList();
    }

    private Vector3 GetCellPosition(Vector2Int index)
    {
        return GetCellPosition(index.x, index.y);
    }

    private Vector3 GetCellPosition(int x, int z)
    {
        float xOffset = (ArenaSize.x + x) * CellSize;
        float zOffset = (ArenaSize.y + z) * CellSize;
        return transform.position + new Vector3(xOffset, 0.0f, zOffset);
    }

    private void SnapToCellGrid()
    {
        Vector3 position = transform.position;
        position.x = Mathf.Floor((position.x + CellSize * 0.5f) / CellSize) * CellSize;
        position.z = Mathf.Floor((position.z + CellSize * 0.5f) / CellSize) * CellSize;

        transform.position = position;
    }
}
