﻿using GameCode.General;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.Levels
{
    public class Obsolete_ChunkManager : MonoBehaviour, IValidRestrictions
    {
        public string SceneName;//this is why we need a scene metadata

        public void LoadAdditively()
        {
            SceneManager.LoadScene(SceneName, LoadSceneMode.Additive);
        }

        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            if (string.IsNullOrEmpty(SceneName))
            {
                return SetupValidationHelper.Fail("Scene name is empty");
            }

            bool isSceneInBUildSettings = false;
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                Scene scene = SceneManager.GetSceneByBuildIndex(i);
                if (scene.name.Equals(SceneName))
                {
                    isSceneInBUildSettings = true;
                }
            }

            if (isSceneInBUildSettings)
            {
                return SetupValidationHelper.NoChangeWithoutMessage();
            }
            else
            {
                return SetupValidationHelper.Fail($"Scene {SceneName} isn't in build settings");
            }
        }
    }
}