using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTrap : MonoBehaviour
{
    public float ClosedStateTime = 2.0f;
    public float OpenedStateTime = 1.75f;
    public bool StartOpened = false;
    public float InitialDelay = 0.0f;
    
    [Space] 
    public AnimatedStateMachine StateMachine;
    public AnimatedState OpenedState;
    public AnimatedState ClosedState;

    private bool m_IsOpenOrOpening = false;
    private float m_TimeToChangeState;
    
    private void OnEnable()
    {
        if (StartOpened)
        {
            StateMachine.RequestState(OpenedState, false);
            m_IsOpenOrOpening = true;
            m_TimeToChangeState = OpenedStateTime;
        }
        else
        {
            StateMachine.RequestState(ClosedState, false);
            m_IsOpenOrOpening = false;
            m_TimeToChangeState = ClosedStateTime;
        }

        m_TimeToChangeState -= InitialDelay;
    }

    void Update()
    {
        m_TimeToChangeState -= Time.deltaTime;
        if (m_TimeToChangeState <= 0.0f)
        {
            if (m_IsOpenOrOpening)
            {
                ChangeStateToClosing();
            }
            else
            {
                ChangeStateToOpening();
            }
        }
    }

    private void ChangeStateToOpening()
    {
        StateMachine.RequestState(OpenedState, true);
        m_TimeToChangeState = OpenedStateTime;
        m_IsOpenOrOpening = true;
    }

    private void ChangeStateToClosing()
    {
        StateMachine.RequestState(ClosedState, true);
        m_TimeToChangeState = ClosedStateTime;
        m_IsOpenOrOpening = false;
    }
}
