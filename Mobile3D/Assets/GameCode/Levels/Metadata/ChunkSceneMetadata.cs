﻿using System.Collections.Generic;
using GameCode.General;
using Unity.Rendering;
using UnityEngine;

namespace GameCode.Levels.Metadata
{
    [CreateAssetMenu(fileName = nameof(ChunkSceneMetadata), menuName = Const.MenuNameProject + nameof(ChunkSceneMetadata))]
    public class ChunkSceneMetadata : ASceneMetadata
    {
        /// <summary>
        /// TODO: we might need a separate zones for actor chunk bucketing
        /// </summary>
        public List<Bounds> LoadChunkZones = new List<Bounds>();
        public List<Bounds> IgnoreLoadChunkZones = new List<Bounds>();
        public List<Bounds> UnloadPreventionZones = new List<Bounds>();

        public bool IsPointWithinLoadZones(Vector3 point, bool useIgnoreLoadChunkZones = true)
        {
            bool isWithinLoadZones = false;
            for (int i = 0; i < LoadChunkZones.Count; i++)
            {
                var loadZone = LoadChunkZones[i];
                if (loadZone.Contains(point))
                {
                    isWithinLoadZones = true;
                    break;
                }
            }

            if (isWithinLoadZones && useIgnoreLoadChunkZones)
            {
                for (int i = 0; i < IgnoreLoadChunkZones.Count; i++)
                {
                    var ignoreLoadZone = IgnoreLoadChunkZones[i];
                    if (ignoreLoadZone.Contains(point))
                    {
                        isWithinLoadZones = false;
                        break;
                    }
                }
            }

            return isWithinLoadZones;
        }

        public bool IsPointWithinUnloadPreventionZones(Vector3 point)
        {
            for (int i = 0; i < UnloadPreventionZones.Count; i++)
            {
                var unloadPreventionZone = UnloadPreventionZones[i];
                if (unloadPreventionZone.Contains(point))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public struct WorldChunkID
    {
        public WorldChunkID Global => new WorldChunkID(0, 0);
        
        public readonly int WorldID;
        public readonly int ChunkID;

        public WorldChunkID(int worldID, int chunkID)
        {
            WorldID = worldID;
            ChunkID = chunkID;
        }

        public override int GetHashCode()
        {
            int hash = 23;
            hash = hash * 31 + WorldID;
            hash = hash * 31 + ChunkID;

            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj is WorldChunkID other)
            {
                return other.WorldID == WorldID
                       && other.ChunkID == ChunkID;
            }

            return false;
        }

        public static bool operator ==(WorldChunkID o1, WorldChunkID o2)
        {
            return o1.WorldID == o2.WorldID
                   && o1.ChunkID == o2.ChunkID;
        } 

        public static bool operator!=(WorldChunkID o1, WorldChunkID o2)
        {
            return o1.WorldID != o2.WorldID
                   || o1.ChunkID != o2.ChunkID;
        } 
    }
}