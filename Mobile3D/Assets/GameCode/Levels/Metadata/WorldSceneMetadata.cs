﻿using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

namespace GameCode.Levels.Metadata
{
    [CreateAssetMenu(fileName = nameof(WorldSceneMetadata), menuName = Const.MenuNameProject + nameof(WorldSceneMetadata))]
    public class WorldSceneMetadata : ASceneMetadata
    {
        public List<ChunkSceneMetadata> AllChunks = new List<ChunkSceneMetadata>();
        public Vector3 FirstPlayerPosition;
        public Quaternion FirstPlayerRotation;
    }
}