﻿using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

namespace GameCode.Levels.Metadata
{
    [CreateAssetMenu(fileName = nameof(SceneRegistry), menuName = Const.MenuNameGame + nameof(SceneRegistry))]
    public partial class SceneRegistry : ScriptableObject
    {
        private const string RegistryPath = "WorldsMetadata/SceneRegistry";
        
        private static SceneRegistry s_Instance;

        public static SceneRegistry Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = Resources.Load<SceneRegistry>(RegistryPath);
                    s_Instance.InitRuntimeCache();
                }

                return s_Instance;
            }
        }

        public static void ResetStatics()
        {
            s_Instance = null;
        }

        public List<ASceneMetadata> AllScenes = new List<ASceneMetadata>();
        public Dictionary<int, ASceneMetadata> AllScenesMap = new Dictionary<int, ASceneMetadata>();
        public Dictionary<int, ASceneMetadata> WorldScenesMap = new Dictionary<int, ASceneMetadata>();
        public Dictionary<int, List<ChunkSceneMetadata>> WorldBucketChunkScenesListMap = new Dictionary<int, List<ChunkSceneMetadata>>();
        public Dictionary<WorldChunkID, ASceneMetadata> ChunkScenesMap = new Dictionary<WorldChunkID, ASceneMetadata>();
        

        private void InitRuntimeCache()
        {
            AllScenesMap.Clear();
            WorldScenesMap.Clear();
            WorldBucketChunkScenesListMap.Clear();
            ChunkScenesMap.Clear();
            
            for (int i = 0; i < AllScenes.Count; i++)
            {
                var scene = AllScenes[i];
                AllScenesMap.Add(scene.SceneGUID, scene);
                if (scene is WorldSceneMetadata worldSceneMetadata)
                {
                    WorldScenesMap.Add(worldSceneMetadata.SceneGUID, worldSceneMetadata);
                    var allChunks = worldSceneMetadata.AllChunks;
                    WorldBucketChunkScenesListMap[worldSceneMetadata.SceneGUID] = allChunks;

                    for (int j = 0; j < allChunks.Count; j++)
                    {
                        var chunk = allChunks[j];
                        WorldChunkID id = new WorldChunkID(worldSceneMetadata.SceneGUID, chunk.SceneGUID);
                        ChunkScenesMap.Add(id, chunk);
                    }
                }
            }
        }
    }
}