﻿using System.Collections.Generic;

namespace GameCode.Levels.Metadata
{
#if UNITY_EDITOR
    
    public partial class SceneRegistry
    {
        public List<ASceneMetadata> Editor_AllScenes => AllScenes;
        
        public void Editor_InitRuntimeCache()
        {
            InitRuntimeCache();
        }
    }
    
#endif
}