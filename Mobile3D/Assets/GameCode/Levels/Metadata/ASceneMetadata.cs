using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// TODO: add all scene metadata registry
/// </summary>
public abstract class ASceneMetadata : ScriptableObject
{
    /// <summary>
    /// 0 means null - no scene
    /// </summary>
    public int SceneGUID;
    public string SceneFileName;
}