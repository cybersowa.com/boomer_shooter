using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using GameCode.BucketSerialization;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    public class PickupInteractable : AInteractables
    {
        public SerializedPickupData PickupData;

        public override string GetInteractionText(IInteractionContext context)
        {
            return $"Press [E] to pick up {PickupData?.name ?? null}";
        }

        public override void TriggerInterraction(IInteractionContext context)
        {
            Assert.IsTrue(context is PlayerInteractionContext, $"Currently we don't support other interaction contexts than player. Passed interaction context type is {context.GetType()}");
            Assert.IsFalse(m_WasPickedUp, "Don't trigger picking up item on already picked up pickable!");
            
            if (context is PlayerInteractionContext playerContext)
            {
                var inventory = playerContext.PlayerController.Inventory;
                var itemID = PickupData.ID;
                if (inventory.ContainsKey(itemID))
                {
                    inventory[itemID]++;
                }
                else
                {
                    inventory[itemID] = 1;
                }

                m_WasPickedUp = true;
                SetRegisteredState(false);
                StateMachine.RequestState(PickedUp);
            }
        }

        private bool m_WasPickedUp = false;

        public AnimatedState Pickable;
        public AnimatedState PickedUp;

        public AnimatedStateMachine StateMachine;


        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            if (serializedStateMap.TryGetValue(ID, out ASerializedState serializedState))
            {
                Assert.IsTrue(serializedState is SerializedState);
                var pickupState = serializedState as SerializedState;
                m_WasPickedUp = pickupState.WasPickedUp;
            }
            else
            {
                m_WasPickedUp = false;
            }

            SetRegisteredState(false);//note: temp hack just to test the UI
            SetRegisteredState(m_WasPickedUp == false);

            if (m_WasPickedUp)
            {
                StateMachine.RequestState(PickedUp, false);
            }
            else
            {
                StateMachine.RequestState(Pickable, false);
            }
        }

        /// <summary>
        /// TODO: we need a callback or procedure for when we are resetting and/or removing StaticManagers
        /// so they can remove their entries from RuntimeObjectDatabase
        /// </summary>
        public override void ClearState()
        {
            m_WasPickedUp = false;
            // SetRegisteredState(m_WasPickedUp == false);
        }

        private void SetRegisteredState(bool targetRegisterState)
        {
            if (targetRegisterState)
            {
                WorldManager.Current?.RuntimeObjectsDatabase.Add<AInteractables>(this);
            }
            else
            {
                WorldManager.Current?.RuntimeObjectsDatabase.Remove<AInteractables>(this);
            }
        }

        public override ASerializedState GenerateSerializedState()
        {
            var state = new SerializedState();
            state.ID = ID;
            state.WasPickedUp = m_WasPickedUp;

            return state;
        }

        public class SerializedState : ASerializedState
        {
            public bool WasPickedUp;

            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;
                return clone;
            }
        }


        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedState);
            var state = instance as SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            bucketData.AddRange(BitConverter.GetBytes(state.WasPickedUp));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);
            state.WasPickedUp = BitConverter.ToBoolean(dataArray, readIndex);
            readIndex += sizeof(bool);

            return (true, state);
        }
    }
}
