using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using GameCode.GameState;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    public class PlayerCheckpoint : ALevelStaticManger
    {
        public bool InitialCheckpoint;

        public override void LoadState(SerializedStateMap serializedStateMap)
        {
        }

        public override void ClearState()
        {
        }

        public override ASerializedState GenerateSerializedState()
        {
            SerializedState state = new SerializedState();
            state.ID = ID;
            return state;
        }
        
        public class SerializedState : ASerializedState
        {
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;

                return clone;
            }
        }


        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedState);
            var state = instance as SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            return (true, state);
        }
    }
}
