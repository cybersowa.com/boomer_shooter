﻿using System;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using GameCode.GameState;
using GameCode.Player;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    public class DoorInteractable : AInteractables
    {
        public SerializedPickupData OptionalKey;

        public bool RequiresKey => OptionalKey != null;
        private bool m_IsOpen = false;

        public AnimatedState OpenState;
        public AnimatedState ClosedState;

        public AnimatedStateMachine StateMachine;
        
        
        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            // throw new System.NotImplementedException();

            SetRegisteredState(false);//note: temp hack just to test the UI
            SetRegisteredState(m_IsOpen == false);

            if (m_IsOpen)
            {
                StateMachine.RequestState(OpenState, false);
            }
            else
            {
                StateMachine.RequestState(ClosedState, false);
            }
        }

        public override void ClearState()
        {
            // throw new System.NotImplementedException();
        }

        public override ASerializedState GenerateSerializedState()
        {
            // throw new System.NotImplementedException();
            var state = new SerializedState();
            state.ID = ID;
            return state;
        }

        public override string GetInteractionText(IInteractionContext context)
        {
            Assert.IsTrue(context is PlayerInteractionContext, $"Currently we don't support other interaction contexts than player. Passed interaction context type is {context.GetType()}");
            Assert.IsFalse(m_IsOpen, "Don't ask for interaction on already opened door!");

            if (RequiresKey)
            {
                var playerContext = context as PlayerInteractionContext;
                var inventory = playerContext.PlayerController.Inventory;
                var keyID = OptionalKey.ID;
                if (inventory.ContainsKey(keyID) && inventory[keyID] > 0)
                {
                    return $"Press [E] to use {OptionalKey.name} key and open the door";
                }
                else
                {
                    return $"Requires {OptionalKey.name} key";
                }
                        
            }
            else
            {
                return "Press [E] to open the door";
            }
        }

        public override void TriggerInterraction(IInteractionContext context)
        {
            Assert.IsTrue(context is PlayerInteractionContext, $"Currently we don't support other interaction contexts than player. Passed interaction context type is {context.GetType()}");
            Assert.IsFalse(m_IsOpen, "Don't trigger interaction on already opened door!");
            
            if (context is PlayerInteractionContext playerContext)
            {
                bool canBeOpened = true;
                
                if (RequiresKey)
                {
                    var inventory = playerContext.PlayerController.Inventory;
                    var keyID = OptionalKey.ID;
                    if (inventory.ContainsKey(keyID) && inventory[keyID] > 0)
                    {
                        // note: for now treat keys as not consumables. Might change in the future
                        // inventory[keyID]--;
                    }
                    else
                    {
                        canBeOpened = false;
                    }
                }

                if (canBeOpened)
                {
                    m_IsOpen = true;
                    StateMachine.RequestState(OpenState);
                    SetRegisteredState(false);
                }
            }
        }

        private void SetRegisteredState(bool targetRegisterState)
        {
            if (targetRegisterState)
            {
                WorldManager.Current?.RuntimeObjectsDatabase.Add<AInteractables>(this);
            }
            else
            {
                WorldManager.Current?.RuntimeObjectsDatabase.Remove<AInteractables>(this);
            }
        }

        public class SerializedState : ASerializedState
        {
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;

                return clone;
            }
        }
        

        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SerializedState);
            var state = instance as SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            return (true, state);
        }
    }
}