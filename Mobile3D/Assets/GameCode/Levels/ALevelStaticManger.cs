﻿using System;
using GameCode.GameState;
using GameCode.General;
using GameCode.InspectorUtils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Levels
{
    /// <summary>
    /// Note: remember to serialize static managers before unloading scene!
    /// TODO: what we do with actors that should be left (coz they move cross scene) when manages unloads?
    /// Should the manager implementation be as plain C# object that we can unload independenly?
    /// </summary>
    public abstract partial class ALevelStaticManger : MonoBehaviour, IValidRestrictions
    {
        [field: ReadOnly]
        [field: SerializeField] 
        public int ID
        {
            get;
            private set;
        }

        public void AssignNewID(int newID)
        {
            Assert.IsFalse(Application.isPlaying, $"{nameof(AssignNewID)} isn't meant to be called in runtime! It's editor only utility!");

            ID = newID;
        }

        public abstract void LoadState(SerializedStateMap serializedStateMap);
        public abstract void ClearState();

        //TODO: Make use of it!!!
        public abstract ASerializedState GenerateSerializedState();

        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            if (ID == 0)
            {
                ID = Utils.ID.GenerateNewIntUID();
                return SetupValidationHelper.Changed($"Changed ID for {gameObject.name}");
            }
            else
            {
                return SetupValidationHelper.NoChangeWithoutMessage();
            }
        }
        
        public abstract class ASerializedState : ICloneable
        {
            public int ID;
            
            public abstract object Clone();
        }
    }
}