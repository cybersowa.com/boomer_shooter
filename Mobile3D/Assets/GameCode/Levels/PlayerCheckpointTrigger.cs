﻿using System;
using GameCode.GameState;
using GameCode.General;
using GameCode.SaveFiles;
using UnityEngine;

namespace GameCode.Levels
{
    public class PlayerCheckpointTrigger : MonoBehaviour
    {
        public PlayerCheckpoint Checkpoint;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Const.Tags.Player))
            {
                WorldManager.Current.OnCheckpointAchieved(Checkpoint);
                if (!Game.InDemoMode)
                {
                    Game.RuntimeState.ForceFirstCheckpoint = false;
                    
                    if (!Game.IsPermadeathOn())
                    {
                        var gameSnapshot = GameState.Game.CreateSnapshot();
                        Game.CheckpointState = gameSnapshot;
                    }
                    
                    // {//TODO: this is temp. The intention is to take snapshot when player exits the game, not in bespoke points
                    //     var gameSnapshot = GameState.Game.CreateSnapshot();
                    //     Game.RuntimeState = gameSnapshot;
                    // }

                    SaveFileContents contents = new SaveFileContents();
                    contents.Header = new SaveFileHeader()
                    {
                        LastModifiedDateInTicks = DateTime.Now.Ticks,
                        PlayTimeInMiliSeconds = GameState.Game.GetOrCreate<GameTime>().TotalPlaytimeInMilliseconds,
                        HasCheckpointState = Game.CheckpointState != null
                    };

                    contents.RuntimeState = Game.RuntimeState;
                    contents.CheckpointState = Game.CheckpointState;
                    
                    SaveFileUtils.SaveContentsToFileAtSlot(contents, Game.ActiveSaveSlot);
                }
            }
        }
    }
}