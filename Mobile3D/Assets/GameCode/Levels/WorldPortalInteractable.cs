﻿using System;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using GameCode.GameState;
using GameCode.Levels.Metadata;
using Unity.Assertions;
using UnityEngine;

namespace GameCode.Levels
{
    public class WorldPortalInteractable : AInteractables
    {
        public WorldSceneMetadata TargetWorld;
        public bool UsePositionOverride;
        public Vector3 AfterTeleportPositionOverride;
        public float AfterTeleportRotationOverride;
        
        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            WorldManager.Current?.RuntimeObjectsDatabase.Add<AInteractables>(this);
        }

        public override void ClearState()
        {
            WorldManager.Current?.RuntimeObjectsDatabase.Remove<AInteractables>(this);
        }

        public override ASerializedState GenerateSerializedState()
        {
            return new SerializedState() {ID = ID};
        }

        public override string GetInteractionText(IInteractionContext context)
        {
            return $"Go to {TargetWorld.SceneFileName}";
        }

        public override void TriggerInterraction(IInteractionContext context)
        {
            Game.RuntimeState = Game.CreateSnapshot();
            if (UsePositionOverride)
            {
                Game.RuntimeState.PlayerPosition = AfterTeleportPositionOverride;
                Game.RuntimeState.PlayerRotation = Quaternion.Euler(0.0f, AfterTeleportRotationOverride, 0.0f);
            }
            else
            {
                Game.RuntimeState.ForceFirstCheckpoint = true;
            }
            Game.LoadWorld(TargetWorld.SceneFileName);
        }

        public class SerializedState : ASerializedState
        {
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;
                return clone;
            }
        }

        public static (bool, SerializedBucket) Serializer(object instance, int functionHandle)
        {
            Assert.IsTrue(instance is WorldPortalInteractable.SerializedState);
            var state = instance as WorldPortalInteractable.SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, object) Deserializer(SerializedBucket bucket)
        {
            var state = new WorldPortalInteractable.SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            return (true, state);
        }
    }
}