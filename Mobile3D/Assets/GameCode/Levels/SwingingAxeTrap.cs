using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingingAxeTrap : MonoBehaviour
{
    [Header("Initial State")]
    public bool StartOnLeft = true;

    [Header("Auto Looping")] 
    public bool AutoLooping = true;
    public float TimeBetweenSwitch = 2.0f;
    public float FirstSwingDelay = 0.0f;
    
    [Header("Base Setup")]
    public AnimatedStateMachine StateMachine;
    public AnimatedState OnLeftState;
    public AnimatedState OnRightState;

    private float m_TimeInState = 0.0f;
    private bool m_IsOnOrTransitioningToLeft = false;

    private void OnEnable()
    {
        if (StartOnLeft)
        {
            StateMachine.RequestState(OnLeftState, false);
            m_IsOnOrTransitioningToLeft = true;
        }
        else
        {
            StateMachine.RequestState(OnRightState, false);
            m_IsOnOrTransitioningToLeft = false;
        }

        m_TimeInState = 0.0f - FirstSwingDelay;
    }

    private void Update()
    {
        m_TimeInState += Time.deltaTime;
        if (AutoLooping && m_TimeInState >= TimeBetweenSwitch)
        {
            SwitchSides();
        }
    }

    public void SwitchSides()
    {
        if (m_IsOnOrTransitioningToLeft)
        {
            StateMachine.RequestState(OnRightState, true);
            m_IsOnOrTransitioningToLeft = false;
        }
        else
        {
            StateMachine.RequestState(OnLeftState, true);
            m_IsOnOrTransitioningToLeft = true;
        }

        m_TimeInState = 0.0f;
    }
}
