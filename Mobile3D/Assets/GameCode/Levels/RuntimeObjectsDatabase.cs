﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GameCode.Levels
{
    public class RuntimeObjectsDatabase
    {
        private Dictionary<Type, IList> m_RuntimeUnityObjects = new Dictionary<Type, IList>();

        public List<T> All<T>()
        {
            var key = typeof(T);
            
            if (m_RuntimeUnityObjects.TryGetValue(key, out IList list))
            {
                return list as List<T>;
            }
            else
            {
                list = new List<T>();
                m_RuntimeUnityObjects.Add(key, list);
                return (List<T>)list;
            }
        }

        public void Add<T>(T entry)
        {
            var list = All<T>();
            list.Add(entry);
        }

        public void Remove<T>(T entry)
        {
            var list = All<T>();
            list.Remove(entry);
        }
    }
}