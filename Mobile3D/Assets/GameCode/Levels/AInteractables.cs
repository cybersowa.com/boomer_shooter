using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using GameCode.Levels;
using UnityEngine;

public abstract class AInteractables : ALevelStaticManger
{
    public abstract string GetInteractionText(IInteractionContext context);
    public abstract void TriggerInterraction(IInteractionContext context);

    private void OnDestroy()
    {
        WorldManager.Current?.RuntimeObjectsDatabase.Remove<AInteractables>(this);
    }
}

public interface IInteractionContext
{
}
