using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.Weapons;
using UnityEngine;

public class WeaponViewModelController : MonoBehaviour
{
    public List<Slot> Slots = new List<Slot>();

    private WeaponViewModel m_CurrentViewModel = null;

    public WeaponViewModel CurrentViewModel => m_CurrentViewModel;

    private void Awake()
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            var viewModel = Slots[i].ViewModel;
            viewModel.gameObject.SetActive(viewModel == CurrentViewModel);
        }
    }

    public void Select(WeaponDefinition weapon)
    {
        if (CurrentViewModel != null)
        {
            CurrentViewModel.gameObject.SetActive(false);
        }
        
        for (int i = 0; i < Slots.Count; i++)
        {
            if (Slots[i].Weapon == weapon)
            {
                var viewModel = Slots[i].ViewModel;
                viewModel.gameObject.SetActive(true);
                m_CurrentViewModel = viewModel;
                break;
            }
        }
    }
    
    [Serializable]
    public class Slot
    {
        public WeaponDefinition Weapon;
        public WeaponViewModel ViewModel;
    }
}
