using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponViewModel : MonoBehaviour
{
    public AnimatedStateMachine StateMachine;
    [Space] 
    public AnimatedState Idle;
    public AnimatedState Shoot;
    public AnimatedState Show;
    public AnimatedState Hide;

    private float m_ShootCycleTimeLeft = 0;
    
    public void PlayIdle()
    {
        StateMachine.RequestState(Idle, false);
    }
    
    public void PlayShot()
    {
        m_ShootCycleTimeLeft = (float) Shoot.StateAnimation.duration;
        StateMachine.RequestState(Shoot, false);
    }

    private void Update()
    {
        if (m_ShootCycleTimeLeft > 0.0f)
        {
            m_ShootCycleTimeLeft -= Time.deltaTime;

            if (m_ShootCycleTimeLeft <= 0.0f)
            {
                PlayIdle();
            }
        }
    }
}
