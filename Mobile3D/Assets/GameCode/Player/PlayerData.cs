﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Utils;
using Unity.Assertions;

namespace GameCode.Player
{
    public class PlayerData
    {
        private Dictionary<System.Type, IPlayerDataComponent> m_Components = new Dictionary<Type, IPlayerDataComponent>();

        public void AddOrReplace(IPlayerDataComponent component)
        {
            var type = component.GetType();
            m_Components[type] = component;
        }

        public bool TryGet<T>(out T component) where T : IPlayerDataComponent
        {
            var type = typeof(T);
            bool hasComponent = m_Components.TryGetValue(type, out IPlayerDataComponent iComponent);
            if (iComponent is T)
            {
                component = (T)iComponent;
                return hasComponent;
            }

            component = default(T);
            return false;
        }

        public static (bool, SerializedBucket) Serializer(object instance, int functionHandle)
        {
            Assert.IsTrue(instance is PlayerData);
            var state = instance as PlayerData;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            
            SerializationSystem.Serialize(state.m_Components.Values.ToList(), out SerializedBucket playerComponentsListBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(playerComponentsListBucket));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, object) Deserializer(SerializedBucket bucket)
        {
            var playerData = new PlayerData();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            var playerComponents = SerializationUtils.ReadObjectList<IPlayerDataComponent>(ref readIndex, dataArray);
            for (int i = 0; i < playerComponents.Count; i++)
            {
                playerData.AddOrReplace(playerComponents[i]);
            }

            return (true, playerData);
        }
    }

    public interface IPlayerDataComponent
    {
    }

    public class PlayerInventoryComponent : IPlayerDataComponent
    {
        public Dictionary<int, int> Inventory = new Dictionary<int, int>();

        public static (bool, SerializedBucket) Serializer(object instance, int functionhandle)
        {
            Assert.IsTrue(instance is PlayerInventoryComponent);
            var pic = instance as PlayerInventoryComponent;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionhandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            
            TrivialTypeList<int> keys = new TrivialTypeList<int>();
            TrivialTypeList<int> values = new TrivialTypeList<int>();
            foreach (var kvp in pic.Inventory)
            {
                keys.Add(kvp.Key);
                values.Add(kvp.Value);
            }
            
            SerializationSystem.Serialize(keys, out SerializedBucket inventoryKeysListBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(inventoryKeysListBucket));
            
            SerializationSystem.Serialize(values, out SerializedBucket inventoryValuesListBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(inventoryValuesListBucket));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, object) Deserializer(SerializedBucket bucket)
        {
            var pic = new PlayerInventoryComponent();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;

            var inventoryKeys = SerializationUtils.ReadTrivialTypeList<int>(ref readIndex, dataArray);
            var inventoryValues = SerializationUtils.ReadTrivialTypeList<int>(ref readIndex, dataArray);
            Assert.AreEqual(inventoryKeys.Count, inventoryValues.Count);

            for (int i = 0; i < inventoryKeys.Count; i++)
            {
                pic.Inventory.Add(inventoryKeys[i], inventoryValues[i]);
            }
            
            return (true, pic);
        }
    }
}