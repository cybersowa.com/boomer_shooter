﻿using System.Collections.Generic;
using System.Timers;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.Weapons;
using KinematicCharacterController;
using KinematicCharacterController.Examples;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace GameCode.Player
{
    public class PlayerInteractionContext : IInteractionContext
    {
        public PlayerController PlayerController;
    }

    public class PlayerController : MonoBehaviour
    {
        public int PlayerID = 0;
        public PlayerCharacterController Character;
        public PlayerCamera CharacterCamera;

        [Space]
        public List<WeaponDefinition> Weapons = new List<WeaponDefinition>();

        [Space] 
        public CanvasGroup InteractableCanvas;
        public TextMeshProUGUI InteractableHintLabel;
        public HealthbarPresenter HealthbarPresenter;
        
        private const string MouseXInput = "Mouse X";
        private const string MouseYInput = "Mouse Y";
        private const string MouseScrollInput = "Mouse ScrollWheel";
        private const string HorizontalInput = "Horizontal";
        private const string VerticalInput = "Vertical";

        private float m_WeaponCooldown;
        private WeaponDefinition m_CurrentWeapon;
        private float WeaponCooldown => m_CurrentWeapon?.WeaponCooldown ?? 0.0f;

        private WeaponViewModelController m_WeaponViewModelController;

        public Dictionary<int, int> Inventory => m_InventoryCompoent.Inventory;
        private PlayerInventoryComponent m_InventoryCompoent = new PlayerInventoryComponent();

        private PlayerInteractionContext InteractionContext { get; set; }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;

            // Tell camera to follow transform
            CharacterCamera.SetFollowTransform(Character.CameraFollowPoint);

            // Ignore the character's collider(s) for camera obstruction checks
            CharacterCamera.IgnoredColliders.Clear();
            CharacterCamera.IgnoredColliders.AddRange(Character.GetComponentsInChildren<Collider>());

            m_CurrentWeapon = GetWeapon(0);
            
            InteractionContext = new PlayerInteractionContext(){ PlayerController = this };

            if (Game.TryGetPlayerComponent(PlayerID, out m_InventoryCompoent) == false)
            {
                m_InventoryCompoent = new PlayerInventoryComponent();
                Game.SetPlayerComponent(PlayerID, m_InventoryCompoent);
            }

            m_WeaponViewModelController = CharacterCamera.GetComponent<WeaponViewModelController>();
            m_WeaponViewModelController.Select(m_CurrentWeapon);
            m_WeaponViewModelController.CurrentViewModel.PlayIdle();
        }

        private WeaponDefinition GetWeapon(int i)
        {
            WeaponDefinition ws = null;

            if (i >= 0 && i < Weapons.Count)
            {
                ws = Weapons[i];
            }

            return ws;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            HandleCharacterInput();
            HandleItemInput();
            HandleUsingInteractable();
            HandlePlayerStatusUI();
        }

        private void HandlePlayerStatusUI()
        {
            var vital = Character?.GetComponent<PlayerVitals>();
            if (vital != null)
            {
                HealthbarPresenter.UpdateBar(vital.HP / 100.0f);
            }
        }

        /// <summary>
        /// TODO: switch interactable reach check from cylinder check to line of sight and interactable bounding box check
        /// </summary>
        private void HandleUsingInteractable()
        {
            Assert.IsNotNull(WorldManager.Current);
            Assert.IsNotNull(Character);

            AInteractables selectedInteractable = null;
            var allInteractables = WorldManager.Current.RuntimeObjectsDatabase.All<AInteractables>();
            Vector2 interactionHeightLimit =
                new Vector2(Character.transform.position.y - 2.0f, Character.transform.position.y + 2.0f);
            Vector3 interactionCheckCenter = Character.transform.position + Character.transform.forward;
            float interactionRadius = 2.0f;
            
            for (int i = 0; i < allInteractables.Count; i++)
            {
                Vector3 interactionPoint = allInteractables[i].transform.position;
                
                if(interactionPoint.y < interactionHeightLimit.x) continue;
                if(interactionPoint.y > interactionHeightLimit.y) continue;
                if(Vector3.Distance(interactionPoint, interactionCheckCenter) > interactionRadius) continue;

                selectedInteractable = allInteractables[i];
                break;
            }

            if (selectedInteractable == null)
            {
                {//HIDE UI
                    InteractableCanvas.alpha = 0.0f;
                }
            }
            else
            {
                {//SHOW UI
                    InteractableCanvas.alpha = 1.0f;
                    InteractableHintLabel.text = selectedInteractable.GetInteractionText(InteractionContext);
                }
                
                if (Input.GetKeyDown(KeyCode.E))
                {
                    //Interact
                    selectedInteractable.TriggerInterraction(InteractionContext);
                }
            }
        }

        private void HandleItemInput()
        {
            var prevWeapon = m_CurrentWeapon;
            
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                m_CurrentWeapon = GetWeapon(0);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                m_CurrentWeapon = GetWeapon(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                m_CurrentWeapon = GetWeapon(2);
            }

            if (m_CurrentWeapon != prevWeapon)
            {
                m_WeaponViewModelController.Select(m_CurrentWeapon);
            }

            m_WeaponCooldown -= Time.deltaTime;
            if (Input.GetMouseButton(0) && m_WeaponCooldown <= 0.0f)
            {
                m_CurrentWeapon?.ShotEffect.ShotCopy(CharacterCamera.transform);
                m_WeaponCooldown = WeaponCooldown;
                m_WeaponViewModelController.CurrentViewModel.PlayShot();
            }
        }

        private void LateUpdate()
        {
            // Handle rotating the camera along with physics movers
            if (CharacterCamera.RotateWithPhysicsMover && Character.Motor.AttachedRigidbody != null)
            {
                CharacterCamera.PlanarDirection =
                    Character.Motor.AttachedRigidbody.GetComponent<PhysicsMover>().RotationDeltaFromInterpolation *
                    CharacterCamera.PlanarDirection;
                CharacterCamera.PlanarDirection = Vector3
                    .ProjectOnPlane(CharacterCamera.PlanarDirection, Character.Motor.CharacterUp).normalized;
            }

            HandleCameraInput();
        }

        private void HandleCameraInput()
        {
            // Create the look input vector for the camera
            float mouseLookAxisUp = Input.GetAxisRaw(MouseYInput);
            float mouseLookAxisRight = Input.GetAxisRaw(MouseXInput);
            Vector3 lookInputVector = new Vector3(mouseLookAxisRight, mouseLookAxisUp, 0f);

            // Prevent moving the camera while the cursor isn't locked
            if (Cursor.lockState != CursorLockMode.Locked)
            {
                lookInputVector = Vector3.zero;
            }

            // Input for zooming the camera (disabled in WebGL because it can cause problems)
            float scrollInput = -Input.GetAxis(MouseScrollInput);
#if UNITY_WEBGL
    scrollInput = 0f;
#endif

            // Apply inputs to the camera
            CharacterCamera.UpdateWithInput(Time.deltaTime, scrollInput, lookInputVector);

            // Handle toggling zoom level
            if (Input.GetMouseButtonDown(1))
            {
                CharacterCamera.TargetDistance =
                    (CharacterCamera.TargetDistance == 0f) ? CharacterCamera.DefaultDistance : 0f;
            }
        }

        private void HandleCharacterInput()
        {
            PlayerCharacterInputs characterInputs = new PlayerCharacterInputs();

            // Build the CharacterInputs struct
            characterInputs.MoveAxisForward = Input.GetAxisRaw(VerticalInput);
            characterInputs.MoveAxisRight = Input.GetAxisRaw(HorizontalInput);
            characterInputs.CameraRotation = CharacterCamera.Transform.rotation;
            characterInputs.JumpDown = Input.GetKeyDown(KeyCode.Space);
            characterInputs.CrouchDown = Input.GetKeyDown(KeyCode.C);
            characterInputs.CrouchUp = Input.GetKeyUp(KeyCode.C);

            // Apply inputs to character
            Character.SetInputs(ref characterInputs);
        }
    }
}