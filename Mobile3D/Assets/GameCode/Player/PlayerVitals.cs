﻿using GameCode.DamageSystem;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.UI.MainMenu;
using UnityEngine;

namespace GameCode.Player
{
    public class PlayerVitals : ADamageReceiver
    {
        public int HP = 100;
        
        public override void ReceiveDamage(int damageValue)
        {
            HP -= damageValue;

            if (HP <= 0)
            {
                if (Game.IsPermadeathOn() || Game.CheckpointState == null)
                {
                    var snapshot = MainMenuModel.GetSnapshotForNewGame();
                    Game.RuntimeState.WorldName = snapshot.WorldName;
                    Game.LoadSnapshot(Game.RuntimeState.Clone() as GameSnapshot);
                }
                else
                {
                    Game.LoadSnapshot(Game.CheckpointState);
                }
            }
        }
    }
}