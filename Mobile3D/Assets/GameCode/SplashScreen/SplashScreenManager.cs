﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.Enemies;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.SplashScreen
{
    public class SplashScreenManager : MonoBehaviour
    {
        public List<TMP_Text> CreatorMessages = new List<TMP_Text>();

        public float Duration = 3.0f;
        
        private void Awake()
        {
            if (CreatorMessages.Count > 0)
            {
                CreatorMessages[0].gameObject.SetActive(true);
                for (int i = 1; i < CreatorMessages.Count; i++)
                {
                    CreatorMessages[i].gameObject.SetActive(false);
                }
            }
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(Duration);

            int nextSceneIndex = gameObject.scene.buildIndex + 1;
            SceneManager.LoadScene(nextSceneIndex);
        }
    }
}