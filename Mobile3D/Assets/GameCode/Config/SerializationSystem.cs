﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCode.BucketSerialization;
using GameCode.Enemies;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using GameCode.Player;
using GameCode.SaveFiles;
using GameCode.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Object = System.Object;

namespace GameCode.Config
{
    public enum BucketedTypeSerializationFunction
    {
        Invalid = -1,
        Null = 0,

        //1000 to 1999: base framework and player character stuff
        SerializedGameState = 1000,
        SaveFileContents = 1001,
        IList = 1002,//TODO: need to add list of ints and floats as first-class citizen deserializer <- use  where T : struct to create a generic base
        TrivialList = 1003,
        SaveFileHeader = 1004,
        SerializedStateMap = 1005,
        
        //2000 to 2999: game components
        PermaDeathSetting = 2000,
        GameTime = 2001,
        GameUpdateFlags = 2002,
        GameStatistics = 2003,

        //3000 to 3999: gameplay stuff - enemies
        SimpleEnemySpawner = 3000,
        SimpleTurretSpawner = 3001,
        MoreHumanThanHumanEnemy = 3002,

        //4000 to 4999: gameplay stuff - non enemy things
        PlayerCheckpoint = 4000,
        PickupInteractable = 4001,
        DoorInteractable = 4002,
        ShowStart = 4003,
        WorldPortalInteractable = 4004,

        //5000 to 5999: player character data
        PlayerData = 5000,
        PlayerInventoryComponent = 5001,

        //6000 to 6999:
    }

    public enum TrivialTypeSerializationID
    {
        Invalid = -1,
        Null = 0,
        
        //1000 to 1999: C# build-in value types
        SByte = 1000,
        Byte = 1001,
        Short = 1002,
        UShort = 1003,
        Int = 1004,
        UInt = 1005,
        Long = 1006,
        ULong = 1007,
        Char = 1008,
        
        //2000 to 2999: Unity GameObject-Component value types
        Vector2 = 2000,
        Vector3 = 2001,
        Vector4 = 2002,
        Quaternion = 2003,
        
        //3000 to 3999: Unity DOTS value types
        
        //4000 to 4999: custom types
        PrefabID = 4000,
        InstanceID = 4001,
        WorldChunkID = 4002,
    }
    
    public static class SerializationSystem
    {
        //TODO: make it codegen
        //Adding those two on top so it's easy to find and modify
        private static void RegisterFunctionsForBucketedTypes()
        {
            //1000 to 1999: base framework and player character stuff
            AddFunctions(BucketedTypeSerializationFunction.SerializedGameState, GameSnapshot.Serializer, GameSnapshot.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.SaveFileContents, SaveFileContents.Serializer, SaveFileContents.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.IList, IListSerializer, IListDeserializer);
            AddFunctions(BucketedTypeSerializationFunction.TrivialList, TrivialTypeListUtils.Serializer, TrivialTypeListUtils.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.SaveFileHeader, SaveFileHeader.Serializer, SaveFileHeader.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.SerializedStateMap, SerializedStateMap.Serializer, SerializedStateMap.Deserializer);
            
            //2000 to 2999: game components
            AddFunctions(BucketedTypeSerializationFunction.PermaDeathSetting, PermaDeathSetting.Serializer, PermaDeathSetting.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.GameTime, GameTime.Serializer, GameTime.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.GameUpdateFlags, GameUpdateFlags.Serializer, GameUpdateFlags.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.GameStatistics, GameStatistics.Serializer, GameStatistics.Deserializer);
            
            
            //3000 to 3999: gameplay stuff - enemies
            AddFunctions(BucketedTypeSerializationFunction.SimpleEnemySpawner, SimpleEnemySpawner.Serializer, SimpleEnemySpawner.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.SimpleTurretSpawner, SimpleTurretSpawner.Serializer, SimpleTurretSpawner.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.MoreHumanThanHumanEnemy, MoreHumanThanHumanEnemy.SerializedSnapshot.Serializer, MoreHumanThanHumanEnemy.SerializedSnapshot.Deserializer);
            
            //4000 to 4999: gameplay stuff - non enemy things
            AddFunctions(BucketedTypeSerializationFunction.PlayerCheckpoint, PlayerCheckpoint.Serializer, PlayerCheckpoint.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.PickupInteractable, PickupInteractable.Serializer, PickupInteractable.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.DoorInteractable, DoorInteractable.Serializer, DoorInteractable.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.ShowStart, ShowStart.Serializer, ShowStart.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.WorldPortalInteractable, WorldPortalInteractable.Serializer, WorldPortalInteractable.Deserializer);

            //5000 to 5999: player character data
            AddFunctions(BucketedTypeSerializationFunction.PlayerData, PlayerData.Serializer, PlayerData.Deserializer);
            AddFunctions(BucketedTypeSerializationFunction.PlayerInventoryComponent, PlayerInventoryComponent.Serializer, PlayerInventoryComponent.Deserializer);
        }

        //TODO: make it codegen
        private static void RegisterTypesForBucketedTypes()
        {
            //1000 to 1999: base framework and player character stuff
            AddTypes(typeof(GameSnapshot), BucketedTypeSerializationFunction.SerializedGameState);
            AddTypes(typeof(SaveFileContents), BucketedTypeSerializationFunction.SaveFileContents);
            AddTypes(typeof(IList), BucketedTypeSerializationFunction.IList);
            AddTypes(typeof(ITrivialTypeList), BucketedTypeSerializationFunction.TrivialList);
            AddTypes(typeof(SaveFileHeader), BucketedTypeSerializationFunction.SaveFileHeader);
            AddTypes(typeof(SerializedStateMap), BucketedTypeSerializationFunction.SerializedStateMap);
            
            //2000 to 2999: game components
            AddTypes(typeof(PermaDeathSetting), BucketedTypeSerializationFunction.PermaDeathSetting);
            AddTypes(typeof(GameTime), BucketedTypeSerializationFunction.GameTime);
            AddTypes(typeof(GameUpdateFlags), BucketedTypeSerializationFunction.GameUpdateFlags);
            AddTypes(typeof(GameStatistics), BucketedTypeSerializationFunction.GameStatistics);
            
            //3000 to 3999: gameplay stuff - enemies
            AddTypes(typeof(SimpleEnemySpawner.SerializedState), BucketedTypeSerializationFunction.SimpleEnemySpawner);
            AddTypes(typeof(SimpleTurretSpawner.SerializedState), BucketedTypeSerializationFunction.SimpleTurretSpawner);
            AddTypes(typeof(MoreHumanThanHumanEnemy.SerializedSnapshot), BucketedTypeSerializationFunction.MoreHumanThanHumanEnemy);
            
            //4000 to 4999: gameplay stuff - non enemy things
            AddTypes(typeof(PlayerCheckpoint.SerializedState), BucketedTypeSerializationFunction.PlayerCheckpoint);
            AddTypes(typeof(PickupInteractable.SerializedState), BucketedTypeSerializationFunction.PickupInteractable);
            AddTypes(typeof(DoorInteractable.SerializedState), BucketedTypeSerializationFunction.DoorInteractable);
            AddTypes(typeof(ShowStart.SerializedState), BucketedTypeSerializationFunction.ShowStart);
            AddTypes(typeof(WorldPortalInteractable.SerializedState), BucketedTypeSerializationFunction.WorldPortalInteractable);

            //5000 to 5999: player character data
            AddTypes(typeof(PlayerData), BucketedTypeSerializationFunction.PlayerData);
            AddTypes(typeof(PlayerInventoryComponent), BucketedTypeSerializationFunction.PlayerInventoryComponent);
        }

        private static void RegisterTypesForTrivialTypes()
        {
            //1000 to 1999: C# build-in value types
            AddTypes(typeof(sbyte), TrivialTypeSerializationID.SByte);
            AddTypes(typeof(byte), TrivialTypeSerializationID.Byte);
            AddTypes(typeof(short), TrivialTypeSerializationID.Short);
            AddTypes(typeof(ushort), TrivialTypeSerializationID.UShort);
            AddTypes(typeof(int), TrivialTypeSerializationID.Int);
            AddTypes(typeof(uint), TrivialTypeSerializationID.UInt);
            AddTypes(typeof(long), TrivialTypeSerializationID.Long);
            AddTypes(typeof(ulong), TrivialTypeSerializationID.ULong);
            AddTypes(typeof(char), TrivialTypeSerializationID.Char);
        
            //2000 to 2999: Unity GameObject-Component value types
            AddTypes(typeof(Vector2), TrivialTypeSerializationID.Vector2);
            AddTypes(typeof(Vector3), TrivialTypeSerializationID.Vector3);
            AddTypes(typeof(Vector4), TrivialTypeSerializationID.Vector4);
            AddTypes(typeof(Quaternion), TrivialTypeSerializationID.Quaternion);
        
            //3000 to 3999: Unity DOTS value types
        
            //4000 to 4999: custom types
            AddTypes(typeof(PrefabID), TrivialTypeSerializationID.PrefabID);
            AddTypes(typeof(InstanceID), TrivialTypeSerializationID.InstanceID);
            AddTypes(typeof(WorldChunkID), TrivialTypeSerializationID.WorldChunkID);
        }

        // normal class definition flow
        public delegate (bool, SerializedBucket) SerializerFunctionDelegate(Object instance, int functionHandle);

        public delegate (bool, Object) DeserializerFunctionDelegate(SerializedBucket bucket);

        private static Dictionary<BucketedTypeSerializationFunction, SerializerFunctionDelegate> s_SerializationFunctions;
        private static Dictionary<BucketedTypeSerializationFunction, DeserializerFunctionDelegate> s_DeserializationFunctions;

        private static Dictionary<System.Type, BucketedTypeSerializationFunction> s_BucketedTypesToFunctionMap;
        private static Dictionary<System.Type, TrivialTypeSerializationID> s_TrivialTypeToSerializationID;

        public static void ResetSerializationSystem()
        {
            s_SerializationFunctions = new Dictionary<BucketedTypeSerializationFunction, SerializerFunctionDelegate>();
            s_DeserializationFunctions = new Dictionary<BucketedTypeSerializationFunction, DeserializerFunctionDelegate>();
            s_BucketedTypesToFunctionMap = new Dictionary<Type, BucketedTypeSerializationFunction>();
            s_TrivialTypeToSerializationID = new Dictionary<Type, TrivialTypeSerializationID>();

            RegisterFunctionsForBucketedTypes();
            RegisterTypesForBucketedTypes();
            RegisterTypesForTrivialTypes();
        }

        public static TrivialTypeSerializationID GetTrivialTypeID<T>() where T : struct
        {
            if(s_TrivialTypeToSerializationID.TryGetValue(typeof(T), out var serializationID))
            {
                return serializationID;
            }

            return TrivialTypeSerializationID.Invalid;
        }

        public static bool Serialize(Object instance, out SerializedBucket bucket)
        {
            bucket = default;

            if (instance == null)
            {
                bucket = NullSerializer();
                return true;
            }

            Type type;
            if (instance is ITrivialTypeList)
            {
                type = typeof(ITrivialTypeList);
            }
            else if (instance is IList)
            {
                type = typeof(IList);
            }
            else
            {
                type = instance.GetType();
            }

            if (ValidateHandleFor(type, out BucketedTypeSerializationFunction handle) == false)
            {
                return false;
            }

            var result = s_SerializationFunctions[handle](instance, (int) handle);
            if (result.Item1 == false)
            {
                Debug.LogError($"Failed to serialize object of type {type} using handle {handle}");
                return false;
            }

            bucket = result.Item2;
            return true;
        }

        public static bool Deserialize(SerializedBucket bucket, out Object instance)
        {
            instance = null;

            if (bucket.Header.SerializationFunctionID == (int) BucketedTypeSerializationFunction.Null)
            {
                //NULL deserializer
                return true;
            }

            //@Allocation cleanup needed
            Assert.IsTrue(
                Enum.IsDefined(typeof(BucketedTypeSerializationFunction), bucket.Header.SerializationFunctionID));
            var handle = (BucketedTypeSerializationFunction) bucket.Header.SerializationFunctionID;

            if (ValidateHandleFor(handle) == false)
            {
                return false;
            }

            var result = s_DeserializationFunctions[handle](bucket);
            if (result.Item1 == false)
            {
                Debug.LogError(
                    $"Failed to deserialize object using handle {handle} [bucket version = {bucket.Header.Version}, size = {bucket.Header.BucketSize}]");
                return false;
            }

            if (result.Item2 is null)
            {
                Debug.LogError(
                    $"Failed to deserialize object using handle {handle} [bucket version = {bucket.Header.Version}, size = {bucket.Header.BucketSize}]. Returned object is null");
                return false;
            }

            instance = result.Item2;

            return true;
        }

        public static bool Deserialize<T>(SerializedBucket bucket, out T typedInstance)
        {
            typedInstance = default;

            if (Deserialize(bucket, out Object instance))
            {
                if (instance is T)
                {
                    typedInstance = (T) instance;
                    return true;
                }
                else
                {
                    Debug.LogError(
                        $"Failed to deserialize object as type {typeof(T)}, returned object type is {instance?.GetType()}. Used handle: {(BucketedTypeSerializationFunction) bucket.Header.SerializationFunctionID} [bucket version = {bucket.Header.Version}, size = {bucket.Header.BucketSize}]. Returned object is null");
                    return false;
                }
            }

            return false;
        }

        private static bool ValidateHandleFor(Type type, out BucketedTypeSerializationFunction handle)
        {
            handle = BucketedTypeSerializationFunction.Invalid;

            if (s_BucketedTypesToFunctionMap.ContainsKey(type) == false)
            {
                Debug.LogError($"Type {type} doesn't have registered handle");
                return false;
            }

            handle = s_BucketedTypesToFunctionMap[type];
            return ValidateHandleFor(handle);
        }

        private static bool ValidateHandleFor(BucketedTypeSerializationFunction handle)
        {
            if ((s_SerializationFunctions.ContainsKey(handle) == false)
                || (s_DeserializationFunctions.ContainsKey(handle) == false))
            {
                Debug.LogError($"Handle {handle} doesn't have registered functions");
                return false;
            }

            return true;
        }

        private static void AddFunctions(BucketedTypeSerializationFunction functionID, SerializerFunctionDelegate serializer,
            DeserializerFunctionDelegate deserializer)
        {
            Assert.IsFalse(s_SerializationFunctions.ContainsKey(functionID));
            Assert.IsFalse(s_DeserializationFunctions.ContainsKey(functionID));
            Assert.IsNotNull(serializer);
            Assert.IsNotNull(deserializer);

            s_SerializationFunctions.Add(functionID, serializer);
            s_DeserializationFunctions.Add(functionID, deserializer);
        }

        private static void AddTypes(System.Type type, BucketedTypeSerializationFunction functionID)
        {
            Assert.IsFalse(s_BucketedTypesToFunctionMap.ContainsKey(type));
            
            s_BucketedTypesToFunctionMap.Add(type, functionID);
        }

        private static void AddTypes(System.Type type, TrivialTypeSerializationID serializaitonID)
        {
#if UNITY_5_3_OR_NEWER
            UnityEngine.Assertions.Assert.IsFalse(s_BucketedTypesToFunctionMap.ContainsKey(type));
#endif
            
            s_TrivialTypeToSerializationID.Add(type, serializaitonID);
        }

        private static SerializedBucket NullSerializer()
        {
            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = (int) BucketedTypeSerializationFunction.Null,
                    Version = 1,
                    BucketSize = 0
                },
                DataBucket = new byte[0]
            };

            return bucket;
        }
        

        private static (bool, SerializedBucket) IListSerializer(object instance, int functionhandle)
        {
            Assert.IsTrue(instance is IList);

            var list = instance as IList;

            var bucket = new SerializedBucket();
            bucket.Header.SerializationFunctionID = functionhandle;
            bucket.Header.Version = 1;

            var bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(list.Count));

            for (int i = 0; i < list.Count; i++)
            {
                var entry = list[i];
                SerializationSystem.Serialize(entry, out SerializedBucket entryBucket);
                bucketData.AddRange(SerializationUtils.ToBytes(entryBucket));
            }

            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        private static (bool, object) IListDeserializer(SerializedBucket bucket)
        {
            Assert.AreEqual(bucket.Header.SerializationFunctionID, (int)BucketedTypeSerializationFunction.IList);

            IList list = new List<object>();
            var data = bucket.DataBucket;
            
            int readIndex = 0;
            int count = BitConverter.ToInt32(data, readIndex);
            readIndex += sizeof(int);

            for (int i = 0; i < count; i++)
            {
                readIndex += SerializationUtils.ToSerializedBucket(data, readIndex, out SerializedBucket entryBucket);
                SerializationSystem.Deserialize(entryBucket, out object entry);
                list.Add(entry);
            }

            return (true, list);
        }
    }

    public static class SerializationUtils
    {
        public static byte[] ToBytes(string text)
        {
            var stringBytes = Encoding.UTF8.GetBytes(text);
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(stringBytes.Length));
            wholeArray.AddRange(stringBytes);
            return wholeArray.ToArray();
        }

        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToString(byte[] byteArray, int startIndex, out string result)
        {
            int bytesReadCount = 0;
            int stringByteCount = BitConverter.ToInt32(byteArray, startIndex);
            bytesReadCount += sizeof(int);
            result = Encoding.UTF8.GetString(byteArray, startIndex + bytesReadCount, stringByteCount);
            bytesReadCount += stringByteCount;

            return bytesReadCount;
        }
        
        
        public static byte[] ToBytes(Quaternion quaternion)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(quaternion.x));
            wholeArray.AddRange(BitConverter.GetBytes(quaternion.y));
            wholeArray.AddRange(BitConverter.GetBytes(quaternion.z));
            wholeArray.AddRange(BitConverter.GetBytes(quaternion.w));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToQuaternion(byte[] byteArray, int startIndex, out Quaternion result)
        {
            int bytesReadCount = 0;
            result = default;
            result.x = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.y = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.z = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.w = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            
            return bytesReadCount;
        }
        
        
        public static byte[] ToBytes(Vector3 vec3)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(vec3.x));
            wholeArray.AddRange(BitConverter.GetBytes(vec3.y));
            wholeArray.AddRange(BitConverter.GetBytes(vec3.z));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToVector3(byte[] byteArray, int startIndex, out Vector3 result)
        {
            int bytesReadCount = 0;
            result = default;
            result.x = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.y = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.z = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            
            return bytesReadCount;
        }
        
        
        public static byte[] ToBytes(Vector2 vec3)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(vec3.x));
            wholeArray.AddRange(BitConverter.GetBytes(vec3.y));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToVector2(byte[] byteArray, int startIndex, out Vector2 result)
        {
            int bytesReadCount = 0;
            result = default;
            result.x = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            result.y = BitConverter.ToSingle(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            
            return bytesReadCount;
        }
        
        
        public static byte[] ToBytes<TKey, TValue>(Dictionary<TKey, TValue> dic) where TKey : struct
        {
            List<byte> wholeArray = new List<byte>();

            var keyList = dic.Keys.ToList().From();
            var valueList = dic.Values.ToList();

            SerializationSystem.Serialize(keyList, out var keysBucket);
            SerializationSystem.Serialize(valueList, out var valuesBucket);

            var keysBytes = ToBytes(keysBucket);
            var valueBytes = ToBytes(valuesBucket);
            
            wholeArray.AddRange(keysBytes);
            wholeArray.AddRange(valueBytes);
            
            return wholeArray.ToArray();
        }

        public static int ToDictionary<TKey, TValue>(byte[] byteArray, int startIndex,
            out Dictionary<TKey, TValue> result) where TKey : struct
        {
            int bytesReadCount = 0;
            
            bytesReadCount += ToSerializedBucket(byteArray, startIndex + bytesReadCount, out var keysBucket);
            bytesReadCount += ToSerializedBucket(byteArray, startIndex + bytesReadCount, out var valuesBucket);
            
            SerializationSystem.Deserialize(keysBucket, out System.Object keysInstance);
            SerializationSystem.Deserialize(valuesBucket, out System.Object valuesInstance);
            
            var keys = (TrivialTypeList<TKey>)keysInstance;
            var values = ((IList)valuesInstance).Cast<TValue>().ToList();
             
            Unity.Assertions.Assert.AreEqual(keys.Count, values.Count);

            result = new Dictionary<TKey, TValue>();
            
            for (int i = 0; i < keys.Count; i++)
            {
                result.Add(keys[i], values[i]);
            }

            return bytesReadCount;
        }
        
        
        public static byte[] ToBytes(SerializedBucket bucket)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(bucket.Header.SerializationFunctionID));
            wholeArray.AddRange(BitConverter.GetBytes(bucket.Header.Version));
            wholeArray.AddRange(BitConverter.GetBytes(bucket.Header.BucketSize));
            wholeArray.AddRange(bucket.DataBucket);
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToSerializedBucket(byte[] byteArray, int startIndex, out SerializedBucket result)
        {
            int bytesReadCount = 0;
            result = default;
            result.Header.SerializationFunctionID = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(int);
            result.Header.Version = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(int);
            result.Header.BucketSize = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(int);

            result.DataBucket = new byte[result.Header.BucketSize];
            Array.Copy(byteArray, startIndex + bytesReadCount, result.DataBucket, 0, result.Header.BucketSize);

            bytesReadCount += result.Header.BucketSize;

            return bytesReadCount;
        }
        
        
        
        public static byte[] ToBytes(PrefabID prefabID)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(prefabID.RawValue));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToPrefabID(byte[] byteArray, int startIndex, out PrefabID result)
        {
            int bytesReadCount = 0;
            result = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            
            return bytesReadCount;
        }
        
        
        
        public static byte[] ToBytes(InstanceID InstanceID)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(InstanceID.ID));
            wholeArray.AddRange(BitConverter.GetBytes(InstanceID.WorldID));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToInstanceID(byte[] byteArray, int startIndex, out InstanceID InstanceID)
        {
            int bytesReadCount = 0;
            ulong id = BitConverter.ToUInt64(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(ulong);
            int worldID = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(int);

            InstanceID = new InstanceID(id, worldID);
            
            return bytesReadCount;
        }
        
        
        
        public static byte[] ToBytes(WorldChunkID worldChunkID)
        {
            List<byte> wholeArray = new List<byte>();
            wholeArray.AddRange(BitConverter.GetBytes(worldChunkID.WorldID));
            wholeArray.AddRange(BitConverter.GetBytes(worldChunkID.ChunkID));
            return wholeArray.ToArray();
        }
        
        
        /// <summary>
        /// </summary>
        /// <returns>byte count read</returns>
        public static int ToWorldChunkID(byte[] byteArray, int startIndex, out WorldChunkID worldChunkID)
        {
            int bytesReadCount = 0;
            int worldID = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);
            int chunkID = BitConverter.ToInt32(byteArray, startIndex + bytesReadCount);
            bytesReadCount += sizeof(float);

            worldChunkID = new WorldChunkID(worldID, chunkID);
            
            return bytesReadCount;
        }
        
        public static TrivialTypeList<T> ReadTrivialTypeList<T>(ref int readIndex, byte[] dataArray) where T : struct
        {
            readIndex += ToSerializedBucket(dataArray, readIndex,
                out SerializedBucket serializedBucket);
            SerializationSystem.Deserialize(serializedBucket, out System.Object instance);
            var trivialTypeList = (TrivialTypeList<T>) instance;
            return trivialTypeList;
        }
        
        public static List<T> ReadObjectList<T>(ref int readIndex, byte[] dataArray)
        {
            readIndex += ToSerializedBucket(dataArray, readIndex, out SerializedBucket bucket);
            SerializationSystem.Deserialize(bucket, out System.Object instance);
            return ((IList)instance).Cast<T>().ToList();
        }
    }

}