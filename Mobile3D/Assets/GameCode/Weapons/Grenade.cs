﻿using System;
using UnityEngine;

public class Grenade : AShotEffect
{
    public Rigidbody RigidbodyComponent;
    [Space]
    public ExplosionEffect EffectPrefab;
    public float ExplosionDelay = 3.0f;

    public bool IsArmed = false;

    private float m_InternalClock = 0.0f;

    private void Update()
    {
        if (IsArmed)
        {
            m_InternalClock += Time.deltaTime;
            if (m_InternalClock >= ExplosionDelay)
            {
                var explosion = Instantiate(EffectPrefab, transform.position, Quaternion.identity);
                explosion.TriggerExplosion();
                Destroy(gameObject);
            }
        }
    }

    public void ResetState()
    {
        m_InternalClock = 0.0f;
    }

    public override void ShotCopy(Transform pc)
    {
        // when transform is player's feet
        // var grenade = Instantiate(this, pc.position + pc.up * 1.7f + pc.forward * 0.75f,
        //     Quaternion.identity);
        
        // when transform is player's camera
        var grenade = Instantiate(this, pc.position + pc.up * -0.20f + pc.forward * 0.75f,
            Quaternion.identity);

        grenade.IsArmed = true;
        grenade.RigidbodyComponent.velocity = (pc.forward + pc.up * 0.1f).normalized * 50.0f;
    }
}