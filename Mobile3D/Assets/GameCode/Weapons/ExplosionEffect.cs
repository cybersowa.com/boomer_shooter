﻿using System;
using GameCode.General;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
    public ParticleSystem Explosion;

    public float ExplosionRadius = 3.0f;
    public int DamageValue = 30;

    public float DamageDelay = 0.5f;
    private float m_DamageCounter = 0.0f;
    
    public void TriggerExplosion()
    {
        Explosion.Play();
        m_DamageCounter = DamageDelay;
    }

    private void TriggerExplosionDamage()
    {
        var collidersInRange = Physics.OverlapSphere(transform.position, ExplosionRadius, Const.Layers.DamageHitLayerMask);

        for (int i = 0; i < collidersInRange.Length; i++)
        {
            var c = collidersInRange[i];
            var hitBox = c.GetComponent<Hurtbox>();
            if (hitBox != null)
            {
                hitBox.AddDamageEntry(new DamageEntry()
                {
                    DamageValue = DamageValue
                });
            }
        }
    }

    private void Update()
    {
        if (m_DamageCounter > 0.0f)
        {
            m_DamageCounter -= Time.deltaTime;

            if (m_DamageCounter <= 0.0f)
            {
                TriggerExplosionDamage();
                m_DamageCounter = 0.0f;
            }
        }
    }

    private void OnParticleSystemStopped()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        var gizmoColor = Color.red;
        gizmoColor.a = 0.25f;
        Gizmos.color = gizmoColor;
        Gizmos.DrawSphere(transform.position, ExplosionRadius);
    }
}