﻿using System;
using GameCode.General;
using GameCode.Player;
using UnityEngine;

namespace GameCode.Weapons
{
    public class PlayerKillTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Const.Tags.Player))
            {
                var playerVitals = other.GetComponent<PlayerVitals>();
                if (playerVitals == null)
                {
                    return;
                }

                playerVitals.ReceiveDamage(playerVitals.HP + 1337);
            }
        }
    }
}