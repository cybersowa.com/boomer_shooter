using System.Collections;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

public class PistolTrail : AShotEffect
{
    public LineRenderer TrailLine;
    public ParticleSystem HitEffect;

    [Space]
    public int DamageValue = 10;
    public float MaxDistance = 30.0f;
    
    public float LifeTime = 5.0f;
    public float TrailVisibilityTime = 0.25f;
    private float m_InternalClock = 0.0f;
    

    // Update is called once per frame
    void Update()
    {
        m_InternalClock += Time.deltaTime;

        var isTrailVisible = TrailLine.gameObject.activeSelf;
        if (isTrailVisible && m_InternalClock > TrailVisibilityTime)
        {
            TrailLine.gameObject.SetActive(false);
        }
        else if(!isTrailVisible && m_InternalClock < TrailVisibilityTime)
        {
            TrailLine.gameObject.SetActive(true);
        }
        
        if (m_InternalClock >= LifeTime)
        {
            Destroy(gameObject);
        }
    }

    public override void ShotCopy(Transform pc)
    {
        var trail = Instantiate(this);
        Trigger(trail, pc);
    }

    private static void Trigger(PistolTrail trail, Transform shootSource)
    {
        if (Physics.Raycast(shootSource.position, shootSource.forward, out RaycastHit hitInfo, trail.MaxDistance, Const.Layers.DamageHitLayerMask))
        {
            trail.transform.position = hitInfo.point;
            trail.transform.forward = -shootSource.forward;
            trail.HitEffect.Play();
            trail.TrailLine.useWorldSpace = true;
            trail.TrailLine.SetPositions(new Vector3[]
            {
                hitInfo.point,
                shootSource.position - shootSource.up * 0.25f, 
            });

            var hitBox = hitInfo.collider.GetComponent<Hurtbox>();
            if (hitBox != null)
            {
                hitBox.AddDamageEntry(new DamageEntry()
                {
                    DamageValue = trail.DamageValue
                });
            }
        }
        else
        {
            trail.transform.position = shootSource.position + shootSource.forward * trail.MaxDistance;
            trail.transform.forward = -shootSource.forward;
            trail.TrailLine.SetPositions(new Vector3[]
            {
                trail.transform.position,
                shootSource.position - shootSource.up * 0.25f,
            });
        }

        trail.m_InternalClock = 0.0f;
    }
}
