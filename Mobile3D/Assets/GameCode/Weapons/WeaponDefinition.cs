﻿using UnityEngine;

namespace GameCode.Weapons
{
    [CreateAssetMenu(fileName = "WeaponDefinition", menuName = "Game/Weapons/WeaponDefinition", order = 0)]
    public class WeaponDefinition : ScriptableObject
    {
        public float WeaponCooldown = 0.25f;
        public AShotEffect ShotEffect;
    }
}