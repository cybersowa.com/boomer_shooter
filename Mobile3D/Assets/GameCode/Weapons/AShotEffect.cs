﻿using UnityEngine;

public abstract class AShotEffect : MonoBehaviour
{
    public abstract void ShotCopy(Transform pc);
}