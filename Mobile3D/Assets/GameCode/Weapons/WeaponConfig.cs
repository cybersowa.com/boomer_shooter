using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponConfig : ScriptableObject
{
}

[Serializable]
public class WeaponSlot
{
	public float WeaponCooldown = 0.25f;
	public AShotEffect ShotEffect;
}
