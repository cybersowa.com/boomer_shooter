using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarPresenter : MonoBehaviour
{
    public Image Bar;

    public void UpdateBar(float percentageValue)
    {
        Bar.fillAmount = percentageValue;
    }
}
