﻿using GameCode.LocalizationEngine;
using TMPro;
using UnityEngine;

namespace GameCode.UI.MainMenu
{
    public class GenericLocalizedLabelPresenter : MonoBehaviour
    {
        public LocalizedText Text;
        public TMP_Text Label;
    }
}