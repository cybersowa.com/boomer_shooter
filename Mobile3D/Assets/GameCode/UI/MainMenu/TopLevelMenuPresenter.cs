﻿using System;
using GameCode.SaveFiles;
using Tayx.Graphy;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class TopLevelMenuPresenter : MonoBehaviour, IHideablePanel
    {
        public MainMenuModel Model;

        [Space] 
        public AnimatedStateMachine StateMachine;
        public AnimatedState HiddenState;
        public AnimatedState ShownState;
        
        [Space]
        public CanvasGroup Root;
        public Button ContinueButton;
        public Button NewGameButton;
        public Button LoadGameButton;
        public Button OptionsButton;
        public Button ExitButton;

        [Space] 
        public TMP_Text LastGameInfo;

        public bool IsShown { get; private set; }

        public void Show()
        {
            IsShown = true;
            
            RegisterViewEvents();

            if (Model.TryGetLatestSaveFile(out SaveFileContents saveFileContents))
            {
                LastGameInfo.text = SaveFileUtils.GetGenericHeaderFormattedText(saveFileContents.Header);
                LastGameInfo.gameObject.SetActive(true);   
            }
            else
            {
                LastGameInfo.gameObject.SetActive(false);
            }
            
            StateMachine.RequestState(ShownState);
        }

        public void Hide()
        {
            IsShown = false;
            
            UnregisterViewEvents();
            
            StateMachine.RequestState(HiddenState);
        }

        private void RegisterViewEvents()
        {
            ContinueButton.onClick.AddListener(Model.LoadLatestSaveFile);
            NewGameButton.onClick.AddListener(Model.ShowNewGameMenu);
            LoadGameButton.onClick.AddListener(Model.ShowLoadGameMenu);
        }
        
        private void UnregisterViewEvents()
        {
            ContinueButton.onClick.RemoveAllListeners();
            NewGameButton.onClick.RemoveAllListeners();
            LoadGameButton.onClick.RemoveAllListeners();
        }
    }
}