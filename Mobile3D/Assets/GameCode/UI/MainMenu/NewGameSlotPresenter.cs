﻿using System.Collections.Generic;
using GameCode.LocalizationEngine;
using GameCode.SaveFiles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class NewGameSlotPresenter : MonoBehaviour
    {
        public MainMenuModel Model;
        
        [Space]
        public LocalizedText OccupiedSlotTextFormat;
        public LocalizedText EmptySlotTextFormat;
        public TMP_Text Label;
        public Button NewGameButton;

        private SlotData m_SlotData;

        public void Setup(SlotData slotData)
        {
            m_SlotData = slotData;

            if (m_SlotData.HasSaveContents)
            {
                SetupForOccupiedSlot(m_SlotData);
            }
            else
            {
                SetupForEmptySlot(m_SlotData);
            }
            
        }

        private void SetupForOccupiedSlot(SlotData slotData)
        {
            Label.text = SaveFileUtils.GetGenericHeaderFormattedText(slotData.SaveContents.Header);

            NewGameButton.onClick.AddListener(() =>
            {
                Model.ShopPopup("Do you want to override existing save slot?", new List<PopupPresenter.OptionRequest>()
                {
                    new PopupPresenter.OptionRequest()
                    {
                        Size = PopupPresenter.ButtonSize.Normal,
                        Text = "No",
                        OnClickCallback = () => { }
                    },
                    
                    new PopupPresenter.OptionRequest()
                    {
                        Size = PopupPresenter.ButtonSize.Small,
                        Text = "Yes",
                        OnClickCallback = () => {
                            int slotIndex = m_SlotData.Index;
                            Model.DeleteSlot(slotIndex);
                            Model.StartNewGameAtSlot(slotIndex); 
                        }
                    },
                });
            });
        }

        private void SetupForEmptySlot(SlotData slotData)
        {
            Label.text = "Empty Slot";
            NewGameButton.onClick.AddListener(() =>
            {
                int slotIndex = m_SlotData.Index;
                Model.StartNewGameAtSlot(slotIndex);
            });
        }

        public void CleanupState()
        {
            m_SlotData = null;
            NewGameButton.onClick.RemoveAllListeners();
        }
    }
}