﻿using System;
using System.Text;
using GameCode.LocalizationEngine;
using GameCode.SaveFiles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class LoadGameSlotPresenter : MonoBehaviour
    {
        public MainMenuModel Model;
        
        [Space]
        public LocalizedText TextFormat;
        public TMP_Text Label;
        public Button LoadButton;

        private SaveFileContents m_TargetFile;
        private int m_SlotIndex;

        public void Setup(int slotIndex, SaveFileContents targetFile)
        {
            m_TargetFile = targetFile;
            m_SlotIndex = slotIndex;

            string labelText = SaveFileUtils.GetGenericHeaderFormattedText(targetFile.Header);
            
            Label.text = labelText;
            LoadButton.onClick.AddListener(() =>
            {
                Model.LoadGame(m_SlotIndex, m_TargetFile);
            });
        }
        
        public void CleanupState()
        {
            m_TargetFile = null;
            LoadButton.onClick.RemoveAllListeners();
        }
    }
}