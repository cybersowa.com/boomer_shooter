﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GameCode.Config;
using GameCode.GameState;
using GameCode.General;
using GameCode.Levels;
using GameCode.SaveFiles;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.UI.MainMenu
{
    public class MainMenuModel : MonoBehaviour
    {
        public string DemoSceneName = "SampleEnvironment";

        [Space]
        public TopLevelMenuPresenter TopLevelMenuPresenter;
        public LoadGameMenuPresenter LoadGameMenuPresenter;
        public NewGameMenuPresenter NewGameMenuPresenter;
        public PopupPresenter PopupPresenter;

        private List<IHideablePanel> m_HiddenPanels = new List<IHideablePanel>();
        private List<IHideablePanel> m_ShownPanels = new List<IHideablePanel>();
        
        private List<SlotData> m_Slots;

        private Dictionary<System.Type, IGameComponent> m_NewGameGameComponentsConfiguration { get; set; } = new Dictionary<System.Type, IGameComponent>();

        public List<SlotData> Slots => m_Slots;

        public bool TryGetGameComponent<T>(out T component) where T : class 
        {
            component = default;
            
            if (m_NewGameGameComponentsConfiguration.TryGetValue(typeof(T),
                out IGameComponent iComponent))
            {
                component = (T) iComponent;
                return true;
            }

            return false;
        }

        public void SetGameComponent<T>(T component) where T : IGameComponent
        {
            m_NewGameGameComponentsConfiguration[typeof(T)] = component;
        }
        
        private void OnEnable()
        {
            Debug.Log($"{nameof(Application.persistentDataPath)} = {Application.persistentDataPath}");

            m_NewGameGameComponentsConfiguration.Clear();
            
            LoadAllSlots(out m_Slots, Const.SaveFileSlotCount);
            RegisterPanels();
            
            PopupPresenter.Hide();

            ShowTopLevelMenu();
        }

        private void OnDisable()
        {
            HideAllPanel();
            UnregisterPanels();
        }
        

        private void LoadAllSlots(out List<SlotData> slots, int slotCount)
        {
            slots = new List<SlotData>(slotCount);
            while (slots.Count < slotCount)
            {
                slots.Add(new SlotData(){Index = slots.Count});
            }

            var allPaths = SaveFileUtils.GetAllSaveFilePaths();
            var allFileNames = SaveFileUtils.ConvertPathsToFileNames(allPaths);
            var allSavedSlots = SaveFileUtils.GetSlotNumbers(allFileNames);

            for (int i = allSavedSlots.Count - 1; i >= 0; i--)
            {
                if(allSavedSlots[i] >= slotCount) allSavedSlots.RemoveAt(i);
            }

            for (int i = 0; i < allSavedSlots.Count; i++)
            {
                int slotNo = allSavedSlots[i];
                var slot = slots[slotNo];
                slot.Index = i;
                if (SaveFileUtils.LoadSaveFromSlot(slotNo, out var saveContents))
                {
                    slot.SaveContents = saveContents;
                }
            }
        }

        public void ShowTopLevelMenu()
        {
            HideAllPanel();
            ShowPanel(TopLevelMenuPresenter);
        }
        
        public void ShowNewGameMenu()
        {
            HideAllPanel();
            ShowPanel(NewGameMenuPresenter);
        }
        
        public void ShowLoadGameMenu()
        {
            HideAllPanel();
            ShowPanel(LoadGameMenuPresenter);
        }

        public void ShopPopup(string message, List<PopupPresenter.OptionRequest> options)
        {
            PopupPresenter.Setup(message, options);
            PopupPresenter.Show();
        }

        private void ShowPanel(IHideablePanel panel)
        {
            panel.Show();
            m_HiddenPanels.Remove(panel);
            m_ShownPanels.Add(panel);
        }
        
        private void HideAllPanel()
        {
            for (int i = m_ShownPanels.Count - 1; i >= 0; i--)
            {
                m_ShownPanels[i].Hide();
                m_HiddenPanels.Add(m_ShownPanels[i]);
                m_ShownPanels.RemoveAt(i);
            }
        }

        public bool GetHasSaveForContinue()
        {
            return SaveFileUtils.GetAllSaveFilePaths().Count > 0;
        }

        public void LoadDemoScene()
        {
            //TODO: DO STUFF!
            
            // GameSnapshot serializedGameState = new GameSnapshot()
            // {
            //     ForceFirstCheckpoint = true,
            //     WorldName = DemoSceneName,
            //     GameComponents = m_NewGameGameplayConfiguration.Values.ToList(),
            //     AllManagersStates = new List<ALevelStaticManger.ASerializedState>()
            // };
            //
            // GameState.Game.InDemoMode = true;
            // var loadingScreen = LoadingScreen.LoadingScreen.Instance;
            //
            // loadingScreen.StartLoading(
            //     () => GameState.Game.LoadSnapshot(serializedGameState),
            //     () => { return GameState.Game.CurrentLoadedState; }
            // );
        }

        public void StartNewGameAtSlot(int slotIndex)
        {
            //TODO: just make factory static function...
            // SerializedGameState serializedGameState = new SerializedGameState()
            // {
            //     ForceFirstCheckpoint = true,
            //     WorldName = DemoSceneName,
            //     GameComponents = m_NewGameGameplayConfiguration.Values.ToList(),
            //     AllManagersStates = new List<ALevelStaticManger.ASerializedState>()
            // };

            GameSnapshot serializedGameState = GetSnapshotForNewGame();

            foreach (var component in m_NewGameGameComponentsConfiguration)
            {
                serializedGameState.GameComponents.Add(component.Key, component.Value);
            }

            GameState.Game.InDemoMode = false;
            GameState.Game.ActiveSaveSlot = slotIndex;
            var loadingScreen = LoadingScreen.LoadingScreen.Instance;

            loadingScreen.StartLoading(
                () => GameState.Game.LoadSnapshot(serializedGameState),
                () => { return GameState.Game.CurrentLoadedState; }
            );
        }

        /// <summary>
        /// TODO: change interpretation of latest from highest number to most recently played (i.e. -> check date and time of last modified in file header)
        /// </summary>
        public void LoadLatestSaveFile()
        {
            var filePaths = SaveFileUtils.GetAllSaveFilePaths();
            var fileNames = SaveFileUtils.ConvertPathsToFileNames(filePaths);
            int saveFileSlot = SaveFileUtils.GetHighestSaveNumber(fileNames);

            if (SaveFileUtils.LoadSaveFromSlot(saveFileSlot, out SaveFileContents saveFileContents))
            {
                GameState.Game.InDemoMode = false;
                GameState.Game.ActiveSaveSlot = saveFileSlot;
                var loadingScreen = LoadingScreen.LoadingScreen.Instance;

                loadingScreen.StartLoading(
                    () => GameState.Game.LoadSnapshot(saveFileContents.RuntimeState),
                    () => { return GameState.Game.CurrentLoadedState; }
                );
                
                return;
            }
            
            Assert.IsFalse(true, "Could not load latest slot");
        }

        public bool TryGetLatestSaveFile(out SaveFileContents saveFileContents)
        {
            saveFileContents = null;
            for (int i = m_Slots.Count - 1; i >= 0; i--)
            {
                if (m_Slots[i].HasSaveContents)
                {
                    saveFileContents = m_Slots[i].SaveContents;
                    return true;
                }
            }

            return false;
        }
        
        public void LoadGame(int slotIndex, SaveFileContents saveFileContents)
        {
            GameState.Game.InDemoMode = false;
            GameState.Game.ActiveSaveSlot = slotIndex;
            var loadingScreen = LoadingScreen.LoadingScreen.Instance;

            loadingScreen.StartLoading(
                () => GameState.Game.LoadSnapshot(saveFileContents.RuntimeState),
                () => { return GameState.Game.CurrentLoadedState; }
            );
        }
        
        
        private void RegisterPanels()
        {
            TopLevelMenuPresenter.Hide();
            LoadGameMenuPresenter.Hide();
            NewGameMenuPresenter.Hide();
            m_HiddenPanels.Add(TopLevelMenuPresenter);
            m_HiddenPanels.Add(LoadGameMenuPresenter);
            m_HiddenPanels.Add(NewGameMenuPresenter);
        }

        private void UnregisterPanels()
        {
            m_HiddenPanels.Clear();
            m_ShownPanels.Clear();
        }

        public void DeleteSlot(int slotIndex)
        {
            Assert.IsTrue(slotIndex >= 0);
            Assert.IsTrue(slotIndex < Const.SaveFileSlotCount);
            Assert.IsTrue(m_Slots.Count == Const.SaveFileSlotCount);

            var slot = m_Slots[slotIndex];
            SaveFileUtils.DeleteSaveFileForSlot(slotIndex);
        }
        
        
        public static GameSnapshot GetSnapshotForNewGame()
        {
            var preset = Resources.Load<GameSnapshotPreset>("NewGameState");
            return preset.CreateSnapshot();
        }
    }

    public class SlotData
    {
        public int Index;
        public bool HasSaveContents => SaveContents != null;
        public SaveFileContents SaveContents;
    }
}