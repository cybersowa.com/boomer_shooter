﻿using System;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class LoadGameMenuPresenter : MonoBehaviour, IHideablePanel
    {
        public MainMenuModel Model;

        [Space] 
        public AnimatedStateMachine StateMachine;
        public AnimatedState HiddenState;
        public AnimatedState ShownState;

        [Space]
        public GenericLocalizedLabelPresenter Header;

        [Space]
        public LoadGameSlotPresenter UsedSlotPrototype;
        public GenericLocalizedLabelPresenter EmptySlotPrototype;

        [Space]
        public Button BackButton; 
        
        private List<LoadGameSlotPresenter> m_UsedSlots;
        private List<GenericLocalizedLabelPresenter> m_EmptySlots;

        public bool IsShown { get; private set; }
        
        private void Awake()
        {
            UsedSlotPrototype.gameObject.SetActive(false);
            EmptySlotPrototype.gameObject.SetActive(false);

            m_UsedSlots = new List<LoadGameSlotPresenter>(Const.SaveFileSlotCount);
            m_EmptySlots = new List<GenericLocalizedLabelPresenter>(Const.SaveFileSlotCount);

            for (int i = 0; i < Const.SaveFileSlotCount; i++)
            {
                var usedSlot = Instantiate(UsedSlotPrototype, UsedSlotPrototype.transform.parent);
                usedSlot.name = $"[{i}] Used Load Game Slot";
                var emptySlot = Instantiate(EmptySlotPrototype, EmptySlotPrototype.transform.parent);
                emptySlot.name = $"[{i}] Empty Load Game Slot";
                
                m_UsedSlots.Add(usedSlot);
                m_EmptySlots.Add(emptySlot);
            }
        }

        public void Show()
        {
            // RegisterViewEvents();
            //
            // if (Model.TryGetLatestSaveFile(out SaveFileContents saveFileContents))
            // {
            //     LastGameInfo.text = saveFileContents.GenericHeaderData;
            //     LastGameInfo.gameObject.SetActive(true);   
            // }
            // else
            // {
            //     LastGameInfo.gameObject.SetActive(false);
            // }

            IsShown = true;

            RegisterEvents();
            
            StateMachine.RequestState(ShownState);
        }

        public void Hide()
        {
            // UnregisterViewEvents();
            
            IsShown = false;
            UnregisterEvents();
            StateMachine.RequestState(HiddenState);
        }

        private void RegisterEvents()
        {
            BackButton.onClick.AddListener(Model.ShowTopLevelMenu);

            for (int i = 0; i < Model.Slots.Count; i++)
            {
                var slot = Model.Slots[i];
                if (slot.HasSaveContents)
                {
                    m_UsedSlots[i].Setup(slot.Index, slot.SaveContents);
                    m_UsedSlots[i].gameObject.SetActive(true);
                    m_EmptySlots[i].gameObject.SetActive(false);
                }
                else
                {
                    m_UsedSlots[i].gameObject.SetActive(false);
                    m_EmptySlots[i].gameObject.SetActive(true);
                }
            }
        }

        private void UnregisterEvents()
        {
            BackButton.onClick.RemoveAllListeners();

            for (int i = 0; i < m_UsedSlots.Count; i++)
            {
                m_UsedSlots[i].CleanupState();
            }
        }
    }
}