﻿namespace GameCode.UI.MainMenu
{
    public interface IHideablePanel
    {
        public bool IsShown { get; }
        public void Show();
        public void Hide();
    }
}