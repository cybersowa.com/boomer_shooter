﻿using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class ButtonPanelPresenter : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text m_Text;
        
        [SerializeField]
        private Button m_Button;

        public Button Button => m_Button;

        public string Text
        {
            get => m_Text.text;
            set => m_Text.text = value;
        }
    }
}