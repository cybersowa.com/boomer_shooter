﻿using System.Collections.Generic;
using GameCode.GameState;
using GameCode.General;
using UnityEngine;
using UnityEngine.UI;

namespace GameCode.UI.MainMenu
{
    public class NewGameMenuPresenter : MonoBehaviour, IHideablePanel
    {
        public MainMenuModel Model;

        [Space] 
        public AnimatedStateMachine StateMachine;
        public AnimatedState HiddenState;
        public AnimatedState ShownState;

        [Space]
        public GenericLocalizedLabelPresenter Header;
        
        [Space]
        public NewGameSlotPresenter SlotPrototype;

        [Header("Game Config")]
        public Toggle PermadeathToggle;
        
        [Space]
        public Button BackButton;

        private List<NewGameSlotPresenter> m_Slots;
        
        public bool IsShown { get; private set; }
        
        private void Awake()
        {
            SlotPrototype.gameObject.SetActive(false);

            m_Slots = new List<NewGameSlotPresenter>(Const.SaveFileSlotCount);

            for (int i = 0; i < Const.SaveFileSlotCount; i++)
            {
                var slot = Instantiate(SlotPrototype, SlotPrototype.transform.parent);
                slot.name = $"[{i}] New Game Slot";
                
                m_Slots.Add(slot);
            }
        }
        
        public void Show()
        {
            IsShown = true;

            SetConfigPanel();
            RegisterViewEvents();
            
            StateMachine.RequestState(ShownState);
        }

        public void Hide()
        {
            IsShown = false;
            
            UnregisterViewEvents();
            
            StateMachine.RequestState(HiddenState);
        }

        private void SetConfigPanel()
        {
            if (Model.TryGetGameComponent<PermaDeathSetting>(out PermaDeathSetting component))
            {
                PermadeathToggle.isOn = component.Value;
            }
            else
            {
                PermadeathToggle.isOn = PermaDeathSetting.DefaultValue;
            }
        }

        private void RegisterViewEvents()
        {
            BackButton.onClick.AddListener(Model.ShowTopLevelMenu);

            for (int i = 0; i < Model.Slots.Count; i++)
            {
                var slot = Model.Slots[i];
                m_Slots[i].Setup(slot);
                m_Slots[i].gameObject.SetActive(true);
            }
            
            PermadeathToggle.onValueChanged.AddListener((x) =>
            {
                Model.SetGameComponent(new PermaDeathSetting()
                {
                    Value = x
                });
            });
        }

        private void UnregisterViewEvents()
        {
            BackButton.onClick.RemoveAllListeners();

            for (int i = 0; i < m_Slots.Count; i++)
            {
                m_Slots[i].CleanupState();
            }
            
            PermadeathToggle.onValueChanged.RemoveAllListeners();
        }
    }
}