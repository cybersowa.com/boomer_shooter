using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.UI.MainMenu;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class PopupPresenter : MonoBehaviour, IHideablePanel
{
    public enum ButtonSize
    {
        Normal,
        Small
    }
    
    public class OptionRequest
    {
        public ButtonSize Size;
        public string Text;
        public UnityAction OnClickCallback;
    }
    
    public TMP_Text MessageLabel;
    
    [Space]
    public ButtonPanelPresenter NormalButtonPrototype;
    public ButtonPanelPresenter SmallButtonPrototype;

    private List<ButtonPanelPresenter> m_SpawnedButtons = new List<ButtonPanelPresenter>();
    
    public bool IsShown { get; }
    
    private void Awake()
    {
        NormalButtonPrototype.gameObject.SetActive(false);
        SmallButtonPrototype.gameObject.SetActive(false);
    }

    public void Setup(string message, List<OptionRequest> options)
    {
        MessageLabel.text = message;

        for (int i = 0; i < options.Count; i++)
        {
            var option = options[i];
            
            ButtonPanelPresenter button = null;
            
            if (option.Size == ButtonSize.Normal)
            {
                button = Instantiate(NormalButtonPrototype, NormalButtonPrototype.transform.parent);
            }
            else if (option.Size == ButtonSize.Small)
            {
                button = Instantiate(SmallButtonPrototype, SmallButtonPrototype.transform.parent);
            }
            
            m_SpawnedButtons.Add(button);

            button.Text = option.Text;
            button.Button.onClick.AddListener(Hide);
            button.Button.onClick.AddListener(option.OnClickCallback);
            
            button.gameObject.SetActive(true);
        }
    }
    
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        Cleanup();
    }

    private void Cleanup()
    {
        for (int i = 0; i < m_SpawnedButtons.Count; i++)
        {
            Destroy(m_SpawnedButtons[i].gameObject);
        }

        m_SpawnedButtons.Clear();
    }
}
