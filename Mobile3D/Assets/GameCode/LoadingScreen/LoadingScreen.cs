﻿using System;
using System.Collections;
using GameCode.GameState;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace GameCode.LoadingScreen
{
    public class LoadingScreen : MonoBehaviour
    {
        private const string LoadingScreenFolder = "LoadingScreen";
        private const string PrefabName = "LoadingScreen";
        private const string ResourcePrefabPath = LoadingScreenFolder + "/" + PrefabName;
        
        static private LoadingScreen s_Instance;

        static public LoadingScreen Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = FindObjectOfType<LoadingScreen>();
                }
                
                if (s_Instance == null)
                {
                    var loadingScreenPrefab = Resources.Load<LoadingScreen>(ResourcePrefabPath);
                    s_Instance = Instantiate<LoadingScreen>(loadingScreenPrefab);
                    DontDestroyOnLoad(s_Instance.gameObject);
                }
                
                
                Assert.IsNotNull(s_Instance, "Make sure that LoadingScreen scene is loaded at the beginning of the app and is never unloaded");

                return s_Instance;
            }
        }

        public AnimatedStateMachine StateMachine;
        public AnimatedState Idle;
        public AnimatedState Loading;

        [Space] public Slider LoadingSlider;

        private Coroutine m_CurrentRoutine = null;
        private Action m_OnLoadingStart = null;
        private Func<float> m_LoadedPercentageProvider = null;


        private void Awake()
        {
            StateMachine.RequestState(Idle, false);
        }

        public void StartLoading(Action onLoadingStart, Func<float> loadedPercentageProvider)
        {
            Assert.IsNull(m_CurrentRoutine, "Don't start new loading task until previous is done");

            GameUpdateFlags gameFlagsComponent = Game.GetOrCreate<GameUpdateFlags>();
            gameFlagsComponent.StateFlags |= GameUpdateFlags.Flags.Paused;

            m_OnLoadingStart = onLoadingStart;
            m_LoadedPercentageProvider = loadedPercentageProvider;
            
            m_CurrentRoutine = StartCoroutine(StartLoadingRoutine());
        }

        private IEnumerator StartLoadingRoutine()
        {
            LoadingSlider.gameObject.SetActive(false);
            StateMachine.RequestState(Loading);
            var frameWait = new WaitForEndOfFrame();

            while (StateMachine.CurrentTransition != null)
            {
                yield return frameWait;
            }

            yield return new WaitForSecondsRealtime(0.1f);

            m_OnLoadingStart?.Invoke();

            
            LoadingSlider.value = 0.0f;
            float percentageLoaded = 0.0f;
            while ((percentageLoaded = m_LoadedPercentageProvider()) < 1.0f)
            {
                LoadingSlider.value = percentageLoaded;
                yield return frameWait;
                if (percentageLoaded > 0.05f)
                {
                    LoadingSlider.gameObject.SetActive(true);
                }
            }
            LoadingSlider.value = 1.0f;
            LoadingSlider.gameObject.SetActive(false);

            StateMachine.RequestState(Idle);

            m_CurrentRoutine = null;
            m_OnLoadingStart = null;
            m_LoadedPercentageProvider = null;
            
            GameUpdateFlags gameFlagsComponent = Game.GetOrCreate<GameUpdateFlags>();
            gameFlagsComponent.StateFlags &= ~GameUpdateFlags.Flags.Paused;
            
            yield return null;
        }
    }
}