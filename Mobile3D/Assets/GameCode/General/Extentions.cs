﻿using System.Text;
using UnityEngine;

namespace GameCode.General
{
    public static class Extentions
    {
        public static string GetPathInHierarchy(this Component component)
        {
            var transform = component.transform;
            StringBuilder path = new StringBuilder();
            path.Append(transform.name);

            while (transform.parent != null)
            {
                transform = transform.parent;
                path.Insert(0, "\\");
                path.Insert(0, transform.name);
            }

            return path.ToString();
        }
    }
}