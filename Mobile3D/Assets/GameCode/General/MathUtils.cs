﻿using System;
using UnityEngine;

namespace GameCode.General
{
    public static class MathUtils
    {
        public static Vector2 ToVec2XZ(this Vector3 vector3)
        {
            return new Vector2(vector3.x, vector3.z);
        }

        public static Vector2Int ToIntFloor(this Vector2 vector2, float offset = 0.0f)
        {
            return new Vector2Int(
                Mathf.FloorToInt(vector2.x + offset),
                Mathf.FloorToInt(vector2.y + offset)
            );
        }

        public static Vector2Int RoundToInt(this Vector2 vector2)
        {
            return new Vector2Int(
                Mathf.RoundToInt(vector2.x),
                Mathf.RoundToInt(vector2.y)
            );
        }

        public static Vector2Int GetClampedExclusive(this Vector2Int vector2Int, int xMin, int xMax, int yMin, int yMax)
        {
            return new Vector2Int(
                Mathf.Clamp(vector2Int.x, xMin, xMax-1),
                Mathf.Clamp(vector2Int.y, yMin, yMax-1)
            );
        }

        public static Vector2Int GetClampedExclusive(this Vector2Int vector2Int, int xMax, int yMax)
        {
            return new Vector2Int(
                Mathf.Clamp(vector2Int.x, 0, xMax-1),
                Mathf.Clamp(vector2Int.y, 0, yMax-1)
            );
        }
    }
}