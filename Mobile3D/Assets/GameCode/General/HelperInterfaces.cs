﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.General
{
    public interface IGameObjectInterface
    {
        GameObject gameObject { get; }
    }
    
    public interface IValidRestrictions : IGameObjectInterface
    {
        public enum Outcome
        {
            NoChange,
            Updated,
            Failed
        }
        
        (Outcome, string) OnTryValidate(IValidationContext ctx);

    }

    public interface IValidationContext
    {
        void AddToDirtyList(Component comp);
    }

    public static class SetupValidationHelper
    {
        public static (IValidRestrictions.Outcome, string) NoChangeWithoutMessage() => (IValidRestrictions.Outcome.NoChange, "");
        public static (IValidRestrictions.Outcome, string) ChangedWithoutMessage() => (IValidRestrictions.Outcome.Updated, "");
        public static (IValidRestrictions.Outcome, string) FailWithoutMessage() => (IValidRestrictions.Outcome.Failed, "");
        public static (IValidRestrictions.Outcome, string) NoChange(string message) => (IValidRestrictions.Outcome.NoChange, message);
        public static (IValidRestrictions.Outcome, string) Changed(string message) => (IValidRestrictions.Outcome.Updated, message);
        public static (IValidRestrictions.Outcome, string) Fail(string message) => (IValidRestrictions.Outcome.Failed, message);
        
        
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="???"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>true if objects on scene differs from what is in list</returns>
        public static bool SceneObjectListValidation<T>(Scene scene, List<T> originalList, out List<T> objectsOnScene) where T : Component
        {
            objectsOnScene = new List<T>();
            
            for (int i = 0; i < scene.rootCount; i++)
            {
                objectsOnScene.AddRange(
                    scene.GetRootGameObjects()[i].GetComponentsInChildren<T>(true)
                );
            }
            
            bool neededUpdate = false;
            if (objectsOnScene.Count != originalList.Count)
            {
                neededUpdate = true;
            }
            else
            {
                for (int i = 0; i < originalList.Count; i++)
                {
                    if (objectsOnScene.Contains(originalList[i]) == false)
                    {
                        neededUpdate = true;
                        break;
                    }
                }
            }

            return neededUpdate;
        }
    }
}