﻿using System.Collections.Generic;
using UnityEngine;

namespace GameCode.General
{
    public static class Const
    {
        public const string MenuNameGame = "Game/";
        public const string MenuNamePoolingGroup = MenuNameGame + "Pooling/";
        public const string MenuNameProject = "Project/";
        public const string MenuNameEditor = "Editor/";
        public const string MenuNameGameState = MenuNameGame + "State/";

        public const int SaveFileSlotCount = 10;
        
        public static class Layers
        {
            public const string HurtboxLayerName = "Hurtbox";
            public static readonly int HurtboxLayer = LayerMask.NameToLayer(HurtboxLayerName);
            
            public const string HitboxLayerName = "Hitbox";
            public static readonly int HitboxLayer = LayerMask.NameToLayer(HitboxLayerName);
            
            public const string CharacterControllerLayerName = "CharacterController";
            public static readonly int CharacterControllerLayer = LayerMask.NameToLayer(CharacterControllerLayerName);

            public static readonly int DamageHitLayerMask = int.MaxValue - (1 << CharacterControllerLayer);
        }
        
        
        public static class Tags
        {
            public const string Player = "Player";
            public const string WorldManager = "WorldManager";
            public const string ChunkManager = "ChunkManager";

            public readonly static string[] AllTags = new string[]
            {
                Player,
                WorldManager,
                ChunkManager,
            };
        }
    }
}