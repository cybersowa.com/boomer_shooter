﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.LocalizationEngine
{
    public abstract class ALocalizedAsset : ScriptableObject
    {
        /// <summary>
        /// Used by editor tools
        /// </summary>
        public abstract System.Type AssetType { get; }

        /// <summary>
        /// Used by editor tools
        /// </summary>
        public abstract IList AssetList { get; }
    }

    public abstract class ALocalizedAsset<T> : ALocalizedAsset
    {
        [SerializeField]
        private List<T> m_Assets;

        /// <summary>
        /// Used by editor tools
        /// </summary>
        public override System.Type AssetType => typeof(T);
        
        /// <summary>
        /// Used by editor tools
        /// </summary>
        public override IList AssetList => m_Assets;
        
        public T Get(Lang lang)
        {
            Assert.IsTrue(m_Assets.Count > 0);
            
            int langIndex = (int) lang;
            var asset = m_Assets[0];//default to Lang.Fallback
            
            if (m_Assets.IsIndexValid(langIndex))
            {
                if (m_Assets[langIndex] != null)
                {
                    asset = m_Assets[langIndex];
                }
            }

            return asset;
        }
    }
}