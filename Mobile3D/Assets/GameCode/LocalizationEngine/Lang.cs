﻿namespace GameCode.LocalizationEngine
{
    public enum Lang
    {
        Fallback = 0,
        English,
        Czech,
        Polish
    }
}