﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;

namespace GameCode.LocalizationEngine.Editor
{
    [CustomEditor(typeof(LocalizedText))]
    public class LocalizedTextEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var langNames = Enum.GetNames(typeof(Lang));
            
            var asset = target as LocalizedText;
            EnsureBackingListHasCorrectNumberOfEntries(asset, langNames);
            
            GUILayout.BeginVertical();
            GUILayout.Label("Localization direction notes:");
            asset.LocalizationDirectionNotes = GUILayout.TextArea(asset.LocalizationDirectionNotes);
            {//show editor for dynamic parts of text
                for (int i = 0; i < asset.VariablesUsed.Count; i++)
                {//show what's already there
                    var oldVar = asset.VariablesUsed[i];
                    if (oldVar == null)
                    {
                        continue;
                    }

                    asset.VariablesUsed[i] = EditorGUILayout.ObjectField(
                        $"{{{i}}} | {oldVar.name} | {oldVar.Type}",
                        oldVar,
                        typeof(LocalizationVariable)
                    ) as LocalizationVariable;
                }

                for (int i = asset.VariablesUsed.Count - 1; i >= 0; i--)
                {//remove null entries
                    if (asset.VariablesUsed[i] == null)
                    {
                        asset.VariablesUsed.RemoveAt(i);
                    }
                }
                
                var newEntry = EditorGUILayout.ObjectField(
                    "{add variable?}",
                    null,
                    typeof(LocalizationVariable)
                ) as LocalizationVariable;

                if (newEntry != null)
                {
                    asset.VariablesUsed.Add(newEntry);
                }
            }
            GUILayout.Space(20);
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            for (int i = 0; i < langNames.Length; i++)
            {
                GUILayout.BeginHorizontal();

                GUILayout.Label(langNames[i], GUILayout.Width(100));
                
                string text = asset.Texts[i];
                asset.Texts[i] = GUILayout.TextArea(text);
                
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
            
            EditorUtility.SetDirty(asset);
        }

        private void OnEnable()
        {
            var asset = target as LocalizedText;
            if (string.IsNullOrEmpty(asset.GUID))
            {
                asset.GUID = ID.GenerateNewStringUID();
            }
        }

        private static void EnsureBackingListHasCorrectNumberOfEntries(LocalizedText asset, string[] langNames)
        {
            while (asset.Texts.Count > langNames.Length)
            {
                asset.Texts.RemoveAt(asset.Texts.Count - 1);
            }

            while (asset.Texts.Count < langNames.Length)
            {
                asset.Texts.Add(String.Empty);
            }

            for (int i = 0; i < asset.Texts.Count; i++)
            {
                if (asset.Texts[i] == null)
                {
                    asset.Texts[i] = string.Empty;
                }
            }
        }
    }
}