﻿using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

namespace GameCode.LocalizationEngine.Editor
{
    [CustomEditor(typeof(ALocalizedAsset), true)]
    public class ALocalizedAssetEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var langNames = Enum.GetNames(typeof(Lang));
            
            var asset = target as ALocalizedAsset;
            EnsureBackingListHasCorrectNumberOfEntries(asset, langNames);

            var assetType = asset.AssetType;

            GUILayout.BeginVertical();
            for (int i = 0; i < langNames.Length; i++)
            {
                GUILayout.BeginHorizontal();

                asset.AssetList[i] = EditorGUILayout.ObjectField(langNames[i], (UnityEngine.Object)asset.AssetList[i], assetType, asset);
                
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
            
            EditorUtility.SetDirty(asset);
        }

        private static void EnsureBackingListHasCorrectNumberOfEntries(ALocalizedAsset asset, string[] langNames)
        {
            while (asset.AssetList.Count > langNames.Length)
            {
                asset.AssetList.RemoveAt(asset.AssetList.Count - 1);
            }

            while (asset.AssetList.Count < langNames.Length)
            {
                asset.AssetList.Add(null);
            }
        }
    }
}