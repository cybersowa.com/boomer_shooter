﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameCode.General;
using GameCode.Utils;
using UnityEngine.Assertions;

namespace GameCode.LocalizationEngine
{
    [CreateAssetMenu(fileName = nameof(LocalizedText), menuName = Const.MenuNameGame + nameof(LocalizedText))]
    public class LocalizedText : ScriptableObject
    {
        [SerializeField] private string m_GUID = "";
        [SerializeField] private string m_LocalizationDirectionNotes = "";
        [SerializeField] private List<LocalizationVariable> m_VariablesUsed = new List<LocalizationVariable>();
        [SerializeField] private List<string> m_Texts = new List<string>();

        /// <summary>
        /// Meant for editor code only
        /// </summary>
        public List<string> Texts => m_Texts;
        
        /// <summary>
        /// Meant for editor code only
        /// </summary>
        public List<LocalizationVariable> VariablesUsed => m_VariablesUsed;
        
        /// <summary>
        /// Meant for editor code only
        /// </summary>
        public string GUID
        {
            get { return m_GUID; }
            set { m_GUID = value; }
        }
        
        /// <summary>
        /// Meant for editor code only
        /// </summary>
        public string LocalizationDirectionNotes
        {
            get { return m_LocalizationDirectionNotes; }
            set { m_LocalizationDirectionNotes = value; }
        }

        public string Get(Lang lang)
        {
            //1. Validate if passed params match params defined in the LocalizedText itself
            
            var stringFormat = GetFormat(lang);

            object[] formatParams = new object[m_VariablesUsed.Count];

            for (int i = 0; i < m_VariablesUsed.Count; i++)
            {
                if (Application.isPlaying)
                {
                    formatParams[i] = LocalizationVariables.Get(m_VariablesUsed[i]);
                }
                else
                {
                    formatParams[i] = $"[{m_VariablesUsed[i].name}|{m_VariablesUsed[i].Type}]";
                }
            }
            
            return string.Format(stringFormat, formatParams);
        }
        
        private string GetFormat(Lang lang)
        {
            Assert.IsTrue(m_Texts.Count > 0);
            
            int langIndex = (int) lang;
            var text = m_Texts[0];//default to Lang.Fallback
            
            if (m_Texts.IsIndexValid(langIndex))
            {
                if (string.IsNullOrEmpty(m_Texts[langIndex]) == false)
                {
                    text = m_Texts[langIndex];
                }
            }

            return text;
        }

        public bool ValidateIfParamCountMatches(ref StringBuilder sb)
        {
            bool isValied = true;
            
            int expectedParamOpenings = Regex.Matches(m_Texts[0], "{").Count;
            int expectedParamEndings = Regex.Matches(m_Texts[0], "}").Count;
            if (expectedParamOpenings != expectedParamEndings)
            {
                isValied = false;
                sb.AppendLine(
                    $"[({name})] left({expectedParamOpenings})/right({expectedParamEndings}) bracket count doesn't match");
            }
            
            var langNames = Enum.GetNames(typeof(Lang));

            for (int i = 0; i < m_Texts.Count; i++)
            {
                var text = m_Texts[i];
                if (string.IsNullOrEmpty(text)) continue;
                
                int paramOpenings = Regex.Matches(text, "{").Count;
                int paramEndings = Regex.Matches(text, "}").Count;
                
                if (expectedParamOpenings != paramOpenings)
                {
                    isValied = false;
                    sb.AppendLine(
                        $"[({name})][({langNames[i]})] opening bracket count doesn't match: count ({paramOpenings}), expected ({expectedParamOpenings})");
                }
                
                if (expectedParamEndings != paramEndings)
                {
                    isValied = false;
                    sb.AppendLine(
                        $"[({name})][({langNames[i]})] ending bracket count doesn't match: count ({paramEndings}), expected ({expectedParamEndings})");
                }
            }

            return isValied;
        }
    }
}