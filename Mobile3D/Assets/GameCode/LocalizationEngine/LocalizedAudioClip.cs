﻿using System;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

namespace GameCode.LocalizationEngine
{
    [CreateAssetMenu(fileName = nameof(LocalizedAudioClip), menuName = Const.MenuNameGame + nameof(LocalizedAudioClip))]
    public class LocalizedAudioClip : ALocalizedAsset<AudioClip>
    {
    }
}