﻿using System;
using System.Collections.Generic;
using GameCode.General;
using UnityEngine;

using VariableKeyType = GameCode.LocalizationEngine.LocalizationVariable;

namespace GameCode.LocalizationEngine
{
    /// <summary>
    /// TODO: this should have an integer based guid for fast runtime lookup
    /// </summary>
    [CreateAssetMenu(fileName = nameof(LocalizationVariable), menuName = Const.MenuNameGame + nameof(LocalizationVariable))]
    public class LocalizationVariable : ScriptableObject
    {
        [SerializeField] private string m_GUID = "";
        public string m_LocalizationDirectionNotes = "";
        public LocalizationVariableType Type;
        
        /// <summary>
        /// Meant for editor code only
        /// </summary>
        public string GUID
        {
            get { return m_GUID; }
            set { m_GUID = value; }
        }
    }

    public enum LocalizationVariableType
    {
        Text,
        Number,
        Enumeration,
    }
    
    public static class LocalizationVariables
    {
        private static Dictionary<VariableKeyType, Func<string>> s_VariablesMap = new Dictionary<VariableKeyType, Func<string>>();

        public static void Set(VariableKeyType key, Func<string> provider)
        {
            s_VariablesMap[key] = provider;
        }

        public static string Get(VariableKeyType key, string defaultOutput = "")
        {
            if (s_VariablesMap.ContainsKey(key))
            {
                return s_VariablesMap[key]();
            }

            return defaultOutput;
        }

        public static void Clear(VariableKeyType key)
        {
            s_VariablesMap.Remove(key);
        }

        public static void ClearAll()
        {
            s_VariablesMap.Clear();
        }
    }
}