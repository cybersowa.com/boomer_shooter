﻿using System;
using System.Collections.Generic;
using GameCode.GameState;
using GameCode.Levels;
using UnityEngine;

namespace GameCode.DemoMenu
{
    [Obsolete]
    public class StartDemoSceneButton : MonoBehaviour
    {
        public string SceneName = "SampleEnvironment";
        public void OnClicked()
        {
            // GameSnapshot serializedGameState = new GameSnapshot()
            // {
            //     ForceFirstCheckpoint = true,
            //     WorldName = SceneName,
            //     GameComponents = new List<IGameComponent>(),
            //     AllManagersStates = new List<ALevelStaticManger.ASerializedState>()
            // };
            //
            //
            // var loadingScreen = LoadingScreen.LoadingScreen.Instance;
            //
            // loadingScreen.StartLoading(
            //     () => GameState.Game.LoadSnapshot(serializedGameState),
            //     () => { return GameState.Game.CurrentLoadedState; }
            // );
        }
    }
}