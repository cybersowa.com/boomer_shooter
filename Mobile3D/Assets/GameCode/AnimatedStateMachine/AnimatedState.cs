﻿using UnityEngine;
using UnityEngine.Playables;

public class AnimatedState : MonoBehaviour
{
    public PlayableDirector StateAnimation;

    public virtual void OnTransitionToStart()
    {
    }
    
    public virtual void OnTransitionToEnd()
    {
    }
    
    public virtual void OnTransitionToUpdate()
    {
    }

    public virtual void OnStateStart()
    {
        StateAnimation.Play();
    }
    
    public virtual void OnStateEnd()
    {
        StateAnimation.Stop();
    }
    
    public virtual void OnStateUpdate()
    {
    }
}