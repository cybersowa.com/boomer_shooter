using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// Assumptions:
/// Current transition has to change before entering next one
/// which means there can be delay before applying state,
/// the desired state is preserved
/// </summary>
public class AnimatedStateMachine : MonoBehaviour
{
    [Serializable]
    public class StateDefinition
    {
        public AnimatedState State;
        public List<StateTransition> TransitionsTo;
    }
    
    [Serializable]
    public class StateTransition
    {
        public AnimatedState TargetState;
        public PlayableDirector Transition;
    }

    private struct StateTransitionRequest
    {
        public AnimatedState TargetState;
        public bool PlayTransition;
    }

    private StateTransitionRequest TransitionRequest;
    
    public List<StateDefinition> States = new List<StateDefinition>();
    
    public StateTransition CurrentTransition { get; private set; }
    public AnimatedState CurrentState { get; private set; }

    public void RequestState(AnimatedState state, bool playTransition = true)
    {
        TransitionRequest.TargetState = state;
        TransitionRequest.PlayTransition = playTransition;

        if (CurrentState == TransitionRequest.TargetState)
        {
            return;
        }

        if (CurrentTransition != null)
        {
            return;
        }

        TriggerRequest();
    }

    private void TriggerRequest()
    {
        if (CurrentState != null)
        {
            CurrentState.OnStateEnd();
        }
        
        var prevState = CurrentState;
        CurrentState = TransitionRequest.TargetState;
        
        if (TransitionRequest.PlayTransition && TryGetTransition(prevState, CurrentState, out StateTransition transition))
        {
            CurrentTransition = transition;
            CurrentTransition.Transition.stopped += OnTransitionFinished;
            CurrentTransition.Transition.Play();
            CurrentState.OnTransitionToStart();
        }
        else
        {
            CurrentState.OnStateStart();
        }
    }

    public void OnTransitionFinished(PlayableDirector transitionDirector)
    {
        CurrentTransition.Transition.stopped -= OnTransitionFinished; 
        CurrentTransition = null;
        CurrentState.OnStateStart();
    }

    private bool TryGetTransition(AnimatedState fromState, AnimatedState toState, out StateTransition transition)
    {
        for (int i = 0; i < States.Count; i++)
        {
            if (States[i].State == fromState)
            {
                var transitionsTo = States[i].TransitionsTo;
                for (int k = 0; k < transitionsTo.Count; k++)
                {
                    if (transitionsTo[k].TargetState == toState)
                    {
                        transition = transitionsTo[k];
                        return true;
                    }
                }
                
                break;
            }
        }

        transition = null;
        return false;
    }
}
