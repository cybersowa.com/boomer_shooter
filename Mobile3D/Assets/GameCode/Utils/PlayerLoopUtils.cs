﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.LowLevel;
using UnityEngine.PlayerLoop;

/// <summary>
/// https://medium.com/@thebeardphantom/unity-2018-and-playerloop-5c46a12a677
/// reposted at:
/// https://blog.beardphantom.com/post/190674647054/unity-2018-and-playerloop
/// </summary>
public static class PlayerLoopUtils
{
    public static bool AddSystemAfter<T>(ref PlayerLoopSystem system, PlayerLoopSystem toAdd)
    {
        if (system.subSystemList == null)
        {
            return false;
        }

        int systemIndex = Array.FindIndex(system.subSystemList, loopSystem => loopSystem.type == typeof(T));
        if (systemIndex >= 0)
        {
            bool successfullyAdded = false;
            
            PlayerLoopSystem[] newSystemList = new PlayerLoopSystem[system.subSystemList.Length + 1];
            int offset = 0;
            for (int i = 0; i < system.subSystemList.Length; i++)
            {
                newSystemList[i + offset] = system.subSystemList[i];
                
                if (i == systemIndex)
                {
                    offset += 1;
                    newSystemList[i + offset] = toAdd;// w8, won't that add system Before current one?
                    successfullyAdded = true;
                }
            }

            if (successfullyAdded)
            {
                system.subSystemList = newSystemList;
                return true;
            }
        }

        for (int i = 0; i < system.subSystemList.Length; i++)
        {
            if (AddSystemAfter<T>(ref system.subSystemList[i], toAdd))
            {
                return true;
            }
        }

        return false;
    }
    
    
    public static void RecursivePlayerLoopPrint(PlayerLoopSystem def, StringBuilder sb, int depth)
    {
        if (depth == 0)
        {
            sb.AppendLine("ROOT NODE");
        }
        else if (def.type != null)
        {
            for (int i = 0; i < depth; i++)
            {
                sb.Append("\t");
            }
            sb.AppendLine(def.type.Name);
        }
        if (def.subSystemList != null)
        {
            depth++;
            foreach (var s in def.subSystemList)
            {
                RecursivePlayerLoopPrint(s, sb, depth);
            }
            depth--;
        }
    }
}
