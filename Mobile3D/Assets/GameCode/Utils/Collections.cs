﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.Levels.Metadata;
using UnityEngine.Assertions;

namespace GameCode.Utils
{
    public class RingBuffer<T>
    {
        private T[] m_Buffer;
        private int m_FillAmount;
        private int m_HeadIndex;

        private int BufferSize => m_Buffer.Length;

        public int Count => m_FillAmount;
        
        public RingBuffer(int size)
        {
            m_Buffer = new T[size];
            m_FillAmount = 0;
            m_HeadIndex = -1;
        }

        public void Add(T entry)
        {
            if (m_FillAmount <= BufferSize)
            {
                m_FillAmount++;
            }

            m_HeadIndex = (m_HeadIndex + 1) % BufferSize;

            m_Buffer[m_HeadIndex] = entry;
        }

        public T this[int index]
        {
            get
            {
                Assert.IsTrue(index < Count && index >= 0);
                int bufferIndex = m_HeadIndex - index;
                if (bufferIndex < 0) bufferIndex = BufferSize + bufferIndex;
                return m_Buffer[bufferIndex];
            }
        }
    }

    public interface ITrivialTypeList : IList
    {
        public TrivialTypeSerializationID SerializationID { get; }
        public TrivialTypeList<T> As<T>() where T : struct;

        public void AddContentsToByteList(List<byte> target);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="byteArray"></param>
        /// <param name="readStartIndex"></param>
        /// <param name="count"></param>
        /// <returns>number of bytes read</returns>
        public int ReadContentsFromByteArray(byte[] byteArray, int readStartIndex, int count);
    }
    
    public class TrivialTypeList<T> : List<T>, ITrivialTypeList where T : struct
    {
        public List<T> To()
        {
            var list = new List<T>(this.Count);
            list.AddRange(this);
            return list;
        }
            
        public TrivialTypeSerializationID SerializationID => SerializationSystem.GetTrivialTypeID<T>();
        public TrivialTypeList<T1> As<T1>() where T1 : struct
        {
            if (typeof(T) != typeof(T1))
            {
                throw new InvalidCastException();
            }

            return this as TrivialTypeList<T1>;
        }
        

        public void AddContentsToByteList(List<byte> target)
        {
            var selfType = typeof(T);
            
            if (selfType == typeof(int))
            {
                var intList = this as TrivialTypeList<int>;
                for (int i = 0; i < Count; i++)
                {
                    target.AddRange(SerializationUtils.ToBytes(intList[i]));
                }
            }
            else if(selfType == typeof(WorldChunkID))
            {
                var worldChunkIDList = this as TrivialTypeList<WorldChunkID>;
                for (int i = 0; i < Count; i++)
                {
                    target.AddRange(SerializationUtils.ToBytes(worldChunkIDList[i]));
                }
            }
            else
            {
                throw new NotImplementedException();
            }

        }

        public int ReadContentsFromByteArray(byte[] byteArray, int startIndex, int count)
        {
            var selfType = typeof(T);

            int bytesRead = 0;

            if (selfType == typeof(int))
            {
                var intList = this as TrivialTypeList<int>;
                for (int i = 0; i < count; i++)
                {
                    var intEntry = BitConverter.ToInt32(byteArray, startIndex + bytesRead);
                    bytesRead += sizeof(int);
                    intList.Add(intEntry);
                }
            }
            else if(selfType == typeof(WorldChunkID))
            {
                var worldChunkIDList = this as TrivialTypeList<WorldChunkID>;
                for (int i = 0; i < count; i++)
                {
                    bytesRead +=
                        SerializationUtils.ToWorldChunkID(byteArray, startIndex + bytesRead, out var worldChunkID);
                    worldChunkIDList.Add(worldChunkID);
                }
            }
            else
            {
                throw new NotImplementedException();
            }
            
            return bytesRead;
        }
    }

    public static class TrivialTypeListUtils
    {
        public static TrivialTypeList<T> From<T>(this List<T> list) where T : struct
        {
            var ttl = new TrivialTypeList<T>();
            ttl.AddRange(list);
            return ttl;
        }
        
        public static ITrivialTypeList New(TrivialTypeSerializationID serializationID)
        {
            switch (serializationID)
            {
                case TrivialTypeSerializationID.Int:
                    return new TrivialTypeList<int>();
                case TrivialTypeSerializationID.WorldChunkID:
                    return new TrivialTypeList<WorldChunkID>();
            }

            throw new NotImplementedException(
                $"{serializationID.ToString()} Numeric_value:{(int) serializationID} is currently not implemented for creating a TrivialTypeList");
        }
        
        public static (bool, SerializedBucket) Serializer(object instance, int functionHandle)
        {
            Assert.IsTrue(instance is ITrivialTypeList);
            var list = instance as ITrivialTypeList;
            
            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes((int)list.SerializationID));
            bucketData.AddRange(BitConverter.GetBytes(list.Count));
            list.AddContentsToByteList(bucketData);
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();
            
            return (true, bucket);
        }

        public static (bool, object) Deserializer(SerializedBucket bucket)
        {
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            var serializedTypeID = (TrivialTypeSerializationID)BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);
            
            var count = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            var list = New(serializedTypeID);
            readIndex += list.ReadContentsFromByteArray(dataArray, readIndex, count);
            
            return (true, list);
        }
    }
}