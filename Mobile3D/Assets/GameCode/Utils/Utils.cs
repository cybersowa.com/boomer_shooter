﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.Utils
{
    public static class ID
    {
        public static int GenerateNewIntUID()
        {
            return Guid.NewGuid().GetHashCode();
        }
        
        public static string GenerateNewStringUID()
        {
            return Guid.NewGuid().ToString();
        }
    }
    
    public static class Lists
    {
        public static bool IsIndexValid(this IList list, int index)
        {
            if (index < 0)
            {
                return false;
            }

            if (index >= list.Count)
            {
                return false;
            }

            return true;
        }

        public static int RemoveAll(this IList list, IList thatAreIn)
        {
            int removedCount = 0;
            for (int i = list.Count - 1; i >= 0; i--)
            {
                for (int j = 0; j < thatAreIn.Count; j++)
                {
                    if (list[i] == thatAreIn[j])
                    {
                        list.RemoveAt(i);
                        removedCount++;
                        break;
                    }
                }
            }

            return removedCount;
        }

        public static bool Contains<T>(this T[,] array2d, Vector2Int index)
        {
            if (index.x < 0 || index.y < 0)
            {
                return false;
            }

            if (index.x >= array2d.GetLength(0) || index.y >= array2d.GetLength(1))
            {
                return false;
            }

            return true;
        }

        public static T Get<T>(this T[,] array2d, Vector2Int index)
        {
            return array2d[index.x, index.y];
        }

        public static bool InRange(this Vector2 vec2, float val)
        {
            if (Mathf.Max(vec2.x, vec2.y) < val)
            {
                return false;
            }
            
            if (Mathf.Min(vec2.x, vec2.y) > val)
            {
                return false;
            }

            return true;
        }

        public static int LastIndex(this IList list)
        {
            return list.Count - 1;
        }
    }

    public static class Dictionaries
    {
        public static Dictionary<TKey, TValue> DeepCopy<TKey, TValue>(this Dictionary<TKey, TValue> dict) where TValue : ICloneable
        {
            var clone = new Dictionary<TKey, TValue>();
            foreach (var entry in dict)
            {
                clone.Add(entry.Key, (TValue)entry.Value.Clone());
            }

            return clone;
        }
    }

    public static class Transforms
    {
        public static void ResetLocalPosRotScale(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
    }
    
    /// <summary>
    /// From: https://stackoverflow.com/questions/1410012/parsing-formatted-string
    /// </summary>
    public static class StringExtensions
    {
        public static string[] ParseExact(
            this string data, 
            string format)
        {
            return ParseExact(data, format, false);
        }

        public static string[] ParseExact(
            this string data, 
            string format, 
            bool ignoreCase)
        {
            string[] values;

            if (TryParseExact(data, format, out values, ignoreCase))
                return values;
            else
                throw new ArgumentException("Format not compatible with value.");
        }

        public static bool TryExtract(
            this string data, 
            string format, 
            out string[] values)
        {
            return TryParseExact(data, format, out values, false);
        }

        public static bool TryParseExact(
            this string data, 
            string format, 
            out string[] values, 
            bool ignoreCase)
        {
            int tokenCount = 0;
            format = Regex.Escape(format).Replace("\\{", "{");

            for (tokenCount = 0; ; tokenCount++)
            {
                string token = string.Format("{{{0}}}", tokenCount);
                if (!format.Contains(token)) break;
                format = format.Replace(token,
                    string.Format("(?'group{0}'.*)", tokenCount));
            }

            RegexOptions options = 
                ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None;

            Match match = new Regex(format, options).Match(data);

            if (tokenCount != (match.Groups.Count - 1))
            {
                values = new string[] { };
                return false;
            }
            else
            {
                values = new string[tokenCount];
                for (int index = 0; index < tokenCount; index++)
                    values[index] = 
                        match.Groups[string.Format("group{0}", index)].Value;
                return true;
            }
        }
    }
}