﻿using UnityEngine;

public class AAxisInputSource : MonoBehaviour
{
    public Vector2 InputValue
    {
        get;
        protected set;
    }
}