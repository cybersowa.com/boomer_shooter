using System;
using System.Collections;
using System.Collections.Generic;
using Tayx.Graphy.Utils.NumString;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchPanel : AAxisInputSource
{
    public bool AsTouchPad = false;
    private Vector2 m_TouchPadInput;
    
    [Space]
    
    public bool UseFixedJoystickCenter = false;
    public Vector2 FixedJoystickCenter;
    
    [Space]

    public Image PanelBackground;
    public Gradient InputDebugGradient;
    public float DeadZone = 10f;
    public float TouchRange = 100f;
    public AnimationCurve InputProgressionCurve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
    public float FinalInputMultiplier = 1.0f;
    
    private bool m_HasTouch = false;
    private Vector2 m_TouchStartPoint;

    private void OnEnable()
    {
        FixedJoystickCenter = transform.position;
    }

    void Update()
    {
        if (m_HasTouch)
        {
            if (AsTouchPad)
            {
                InputValue = m_TouchPadInput;
                m_TouchPadInput = Vector2.zero;
            }
        }
        else
        {
            InputValue = Vector2.zero;
        }

        if (PanelBackground != null)
        {
            PanelBackground.color = InputDebugGradient.Evaluate(InputValue.magnitude / FinalInputMultiplier);
        }
    }

    public void OnDragStart(BaseEventData baseEvent)
    {
        if (baseEvent is PointerEventData pEvent)
        {
            m_HasTouch = true;

            if (AsTouchPad)
            {
                InputValue = Vector2.zero;
                m_TouchStartPoint = pEvent.position;
            }
            else{
                if (UseFixedJoystickCenter)
                {
                    m_TouchStartPoint = FixedJoystickCenter;
                }
                else
                {
                    m_TouchStartPoint = pEvent.position;
                }
                 
                InputValue = GetVirtualJoystickValue(pEvent.position);   
            }
        }
    }

    public void OnDrag(BaseEventData baseEvent)
    {
        if (m_HasTouch == false)
        {
            return;
        }
        
        if (baseEvent is PointerEventData pEvent)
        {
            if (AsTouchPad)
            {
                var pointerDelta = pEvent.position - m_TouchStartPoint;
                m_TouchPadInput = pointerDelta * FinalInputMultiplier;
                m_TouchStartPoint = pEvent.position;
            }
            else
            {
                InputValue = GetVirtualJoystickValue(pEvent.position);
            }
        }
    }

    public void OnDragEnd(BaseEventData baseEvent)
    {
        if (baseEvent is PointerEventData pEvent)
        {
            m_HasTouch = false;
            InputValue = Vector2.zero;
            m_TouchPadInput = Vector2.zero;
        }
    }

    private Vector2 GetVirtualJoystickValue(Vector2 pointerPosition)
    {
        Vector2 inputValue;
        
        var pointerDelta = pointerPosition - m_TouchStartPoint;
        float deltaMagnitude = pointerDelta.magnitude;
        deltaMagnitude = (deltaMagnitude - DeadZone) / (TouchRange - DeadZone);
        deltaMagnitude = Mathf.Clamp01(deltaMagnitude);
        deltaMagnitude = InputProgressionCurve.Evaluate(deltaMagnitude);
        inputValue = pointerDelta.normalized * deltaMagnitude;
        inputValue *= FinalInputMultiplier;

        return inputValue;
    }
}
