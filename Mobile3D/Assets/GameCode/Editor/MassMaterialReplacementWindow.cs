using System.Collections;
using System.Collections.Generic;
using GameCode.Editor.Global;
using GameCode.Editor.InspectorUtils;
using GameCode.Utils;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class MassMaterialReplacementWindow : EditorWindow
{
    [MenuItem(EditorConsts.MassUpdateRoot+nameof(MassMaterialReplacementWindow))]
    private static void ShowWindow()
    {
        var window = GetWindow<MassMaterialReplacementWindow>();
        window.titleContent = new GUIContent("MassMaterialReplacementWindow");
        window.Show();
    }

    private List<SceneAsset> m_SceneAssets = new List<SceneAsset>();
    private Material m_OldMaterial;
    private Material m_NewMaterial;

    private string m_LastMessage = "";
    
    private void OnGUI()
    {
        InspectorUtils.DisplayList("Scenes:", m_SceneAssets, true);
        GUILayout.Space(10);
        m_OldMaterial = InspectorUtils.ObjectField("Material To Be Replaced:", m_OldMaterial);
        m_NewMaterial = InspectorUtils.ObjectField("New Material To Assign:", m_NewMaterial);

        bool areParamsValid = m_SceneAssets.Count > 0
                              && m_OldMaterial != null
                              && m_NewMaterial != null;
        
        if (areParamsValid && GUILayout.Button("Replace Materials"))
        {
            m_LastMessage = "Updating materials...";
            int counter = 0;
            
            for (int sceneIndex = 0; sceneIndex < m_SceneAssets.Count; sceneIndex++)
            {
                var sceneAsset = m_SceneAssets[sceneIndex];

                var sceneAssetPath = AssetDatabase.GetAssetPath(sceneAsset);
                EditorSceneManager.OpenScene(sceneAssetPath);
                var scene = EditorSceneManager.GetActiveScene();
                var rootObjects = scene.GetRootGameObjects();

                foreach (var root in rootObjects)
                {
                    var allRenderers = root.GetComponentsInChildren<Renderer>(true);
                    foreach (var renderer in allRenderers)
                    {
                        if (renderer.sharedMaterial == m_OldMaterial)
                        {
                            renderer.sharedMaterial = m_NewMaterial;
                            EditorUtility.SetDirty(renderer);
                            EditorUtility.SetDirty(renderer.gameObject);
                            counter++;
                        }
                        
                        for (int mIndex = 0; mIndex < renderer.sharedMaterials.Length; mIndex++)
                        {
                            if (renderer.sharedMaterials[mIndex] == m_OldMaterial)
                            {
                                renderer.sharedMaterials[mIndex] = m_NewMaterial;
                                EditorUtility.SetDirty(renderer);
                                EditorUtility.SetDirty(renderer.gameObject);
                                counter++;
                            }
                        }
                    }
                }

                if (EditorSceneManager.SaveOpenScenes() == false)
                {
                    m_LastMessage += $"\n{sceneAsset.name} Wasn't properly saved.";
                }
            }
            
            m_LastMessage += $"\n{counter} renderer components were updated.";
        }

        if (string.IsNullOrWhiteSpace(m_LastMessage) == false)
        {
            GUILayout.TextArea(m_LastMessage);
        }
    }
}
