﻿using GameCode.ActorsDeclarations;
using GameCode.Enemies;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.ActorSystem
{
    [CustomPropertyDrawer(typeof(ActorPrefabReference))]
    public class ActorPrefabReferenceDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property,
            GUIContent label)
        {

            var idProperty = property.FindPropertyRelative(nameof(ActorPrefabReference.ID)).FindPropertyRelative("m_RawValue");
            var refID = idProperty.intValue;
            var prefix = $"Prefab reference ID: {refID} ";

            ActorPrefabRegistry.Instance.TryGet(refID, out var actorPrefab);
            actorPrefab = EditorGUI.ObjectField(position, prefix, actorPrefab, typeof(global::ASessionActor), false) as global::ASessionActor;

            EditorGUI.BeginProperty(position, label, property);
            
            refID = actorPrefab == null ? PrefabID.Empty : actorPrefab.PrefabID;
            idProperty.intValue = refID;
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var idProperty = property.FindPropertyRelative(nameof(ActorPrefabReference.ID)).FindPropertyRelative("m_RawValue");
            return base.GetPropertyHeight(idProperty, label);
        }
    }
}