﻿namespace GameCode.Editor.ActorSystem
{
    public abstract partial class ASessionActor
    {
        public abstract void DrawInspectorGUI();
    }
    
    public abstract partial class ASerializableState
    {
        public abstract void DrawInspectorGUI();
    }
}