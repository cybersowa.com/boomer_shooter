﻿using System.Collections.Generic;
using GameCode.ActorsDeclarations;
using GameCode.Editor.UnityAssetDatabase;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.ActorSystem
{
    [CustomEditor(typeof(ActorPrefabRegistry))]
    public class ActorPrefabRegistryInspector : UnityEditor.Editor
    {
        private SerializedObject m_ActorPrefabRegistrySerializedObject;
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            bool markDirty = false;
            
            if (m_ActorPrefabRegistrySerializedObject == null)
            {
                m_ActorPrefabRegistrySerializedObject = new SerializedObject(target);
            }

            if (GUILayout.Button("Find and Register All Actors"))
            {
                var allActors = AssetDatabaseUtils.FindAllAssetsOfTypeAndDerrived<global::ASessionActor>();
                ActorPrefabRegistry registry = (ActorPrefabRegistry) target;
                
                registry.Editor_ActorPrefabList.Clear();
                registry.Editor_ActorPrefabList.AddRange(allActors);
                
                //ensure all have valid guids
                Dictionary<PrefabID, global::ASessionActor> dict = new Dictionary<PrefabID, global::ASessionActor>();
                for (int i = 0; i < allActors.Count; i++)
                {
                    var actorPrefab = allActors[i];

                    while (actorPrefab.PrefabID == PrefabID.Empty || dict.ContainsKey(actorPrefab.PrefabID))
                    {
                        actorPrefab.Editor_SetPrefabID(ID.GenerateNewIntUID());
                        EditorUtility.SetDirty(actorPrefab);
                    }
                }
                
                registry.Editor_InitRuntimeCache();

                markDirty = true;
            }
            
            if(markDirty)
            {
                m_ActorPrefabRegistrySerializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
                Repaint();
            }
        }
    }
}