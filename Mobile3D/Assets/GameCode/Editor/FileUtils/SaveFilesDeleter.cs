﻿using System.IO;
using GameCode.Editor.Global;
using GameCode.SaveFiles;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.FileUtils
{
    public static class SaveFilesDeleter
    {
        [MenuItem(EditorConsts.UnsafeSaveFileToolbarRoot + nameof(DeleteAllSaveFiles))]
        private static void DeleteAllSaveFiles()
        {
            var allPaths = SaveFileUtils.GetAllSaveFilePaths();
            for (int i = 0; i < allPaths.Count; i++)
            {
                if (File.Exists(allPaths[i]))
                {
                    File.Delete(allPaths[i]);
                    Debug.Log($"<color=yellow>{allPaths[i]}</color>");
                }
                else
                {
                    Debug.LogError($"<color=cyan>File {allPaths[i]} doesn't exist even though SaveFileUtils returned such path</color>");
                }
            }
        }
    }
}