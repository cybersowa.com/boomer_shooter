﻿using System.IO;
using System.Text;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.FileUtils
{
    public static class ProjectFileUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="fileName">Without .asset extention as it will be added automatically</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetOrCreateAsset<T> (string folderPath, string fileName) where T : ScriptableObject
        {
            string targetAssetPath = folderPath + "/" + fileName + ".asset";
            T asset = AssetDatabase.LoadAssetAtPath<T>(targetAssetPath);

            if (asset != null)
            {
                return asset;
            }
            
            asset = ScriptableObject.CreateInstance<T> ();
 
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (targetAssetPath);
            AssetDatabase.CreateAsset (asset, assetPathAndName);
 
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return asset;
        }

        public static void EnsureFolderInAssetsExist(string folderInAssetsPath)
        {
            // Leaving this to not forget the lesson:  AssetDatabase.CreateFolder can simply not work
            // without a good reason
            // while Directory.CreateDirectory will work
            // even when using local (project relative) Directory.CreateDirectory will work fine and automate creation recursively
            // char dirSeparator = '\\';
            // char optionalDirSeparator = '/';
            //
            // folderInAssetsPath = folderInAssetsPath.Replace(optionalDirSeparator, dirSeparator);
            // if (folderInAssetsPath.StartsWith(dirSeparator.ToString()))
            // {
            //     folderInAssetsPath = folderInAssetsPath.Substring(1);
            // }
            //
            // var folders = folderInAssetsPath.Split(dirSeparator);
            // Assert.IsTrue(folders.Length > 0);
            // Assert.IsTrue(folders[0].Equals("Assets"));
            //
            // string intermediatePath = "";
            // for (int i = 0; i < folders.Length; i++)
            // {
            //     string nextFolderPath = intermediatePath + folders[i] + dirSeparator;
            //     if (AssetDatabase.IsValidFolder(nextFolderPath) == false)
            //     {
            //         AssetDatabase.CreateFolder(intermediatePath, folders[i]);
            //         AssetDatabase.SaveAssets();
            //         AssetDatabase.Refresh();
            //     }
            //
            //     intermediatePath = nextFolderPath;
            // }

            Directory.CreateDirectory(folderInAssetsPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}