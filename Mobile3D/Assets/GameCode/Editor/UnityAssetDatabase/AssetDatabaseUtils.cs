﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.UnityAssetDatabase
{
    public static class AssetDatabaseUtils
    {
        /// <summary>
        /// https://answers.unity.com/questions/486545/getting-all-assets-of-the-specified-type.html
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> FindAllAssetsOfType<T>() where T : UnityEngine.Object
        {
            List<T> assets = new List<T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for( int i = 0; i < guids.Length; i++ )
            {
                string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
                T asset = AssetDatabase.LoadAssetAtPath<T>( assetPath );
                if( asset != null )
                {
                    assets.Add(asset);
                }
            }
            return assets;
        } 
        
        public static List<T> FindAllAssetsOfTypeAndDerrived<T>() where T : UnityEngine.Object
        {
            //fucking hate those APIs. This code won't work as expected. It finds onlyt "loaded" stuff.
            //need to replace it at some point (maybe using this: https://stackoverflow.com/questions/857705/get-all-derived-types-of-a-type )
            //for reference: https://forum.unity.com/threads/resources-findobjectsoftypeall-not-working-properly.346371/
            return Resources.FindObjectsOfTypeAll<T>().ToList();
        }
        
        
    }
}