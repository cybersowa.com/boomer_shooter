﻿using UnityEditor;

namespace GameCode.Editor.Global
{
    public static class EditorConsts
    {
        public const string ToolToolbarRoot = "GameTools/";
        public const string ValidationToolbarRoot = ToolToolbarRoot + "Validation/";
        public const string MassUpdateRoot = ToolToolbarRoot + "MassUpdate/";
        public const string SaveFileToolbarRoot = ToolToolbarRoot + "SaveFiles/";
        public const string UnsafeSaveFileToolbarRoot = SaveFileToolbarRoot + "Unsafe/";
    }
}