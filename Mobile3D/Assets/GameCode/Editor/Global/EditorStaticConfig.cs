﻿using System;
using System.Collections.Generic;
using GameCode.GameState;
using GameCode.General;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Editor.Global
{
    [InitializeOnLoad]
    public static class EditorStaticConfig
    {
        static EditorStaticConfig()
        {
            SetPlaymodeInitScene();
            EnsureTags();
            EditorApplication.playModeStateChanged += EditorOnPlayModeStateChanged;
        }

        // public static void EnterPlaymodeWithState(GameSnapshot gameState)
        // {
        //     var gameStateHolder = GetGameStateHolder();
        //     Assert.IsNotNull(gameStateHolder);
        //     gameStateHolder.GameState = gameState;
        //     gameStateHolder.Ignore = false;
        //     EditorUtility.SetDirty(gameStateHolder);
        //     EditorApplication.EnterPlaymode();
        // }

        private static void EditorOnPlayModeStateChanged(PlayModeStateChange stateChange)
        {
            if (stateChange == PlayModeStateChange.EnteredPlayMode)
            {
                var gameStateHolder = GetGameStateHolder();
                Assert.IsNotNull(gameStateHolder);
                
                //TODO: bring back editor play-from-selected-checkpoint feature
                // if (gameStateHolder.Ignore == false)
                // {
                //     GameState.Game.LoadSnapshot(gameStateHolder.GameState);
                //     gameStateHolder.GameState = null;
                //     gameStateHolder.Ignore = true;
                //     EditorUtility.SetDirty(gameStateHolder);
                // }
            }
        }

        private static GameSnapshotPreset GetGameStateHolder()
        {
            return Resources.Load<GameSnapshotPreset>("GameState");
        }

        [MenuItem("Editor/Add missing tags to tag manager")]
        private static void EnsureTags()
        {
            var desiredTags = new List<string>();
            desiredTags.AddRange(Const.Tags.AllTags);
                
            SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            SerializedProperty tagsProp = tagManager.FindProperty("tags");
                
            var currentTags = new List<string>();
            for (int i = 0; i < tagsProp.arraySize; i++)
            {
                SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
                currentTags.Add(t.stringValue);
            }
                
            for (int i = 0; i < desiredTags.Count; i++)
            {
                if (currentTags.Contains(desiredTags[i]) == false)
                {
                    tagsProp.InsertArrayElementAtIndex(0);
                    SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
                    n.stringValue = desiredTags[i];
                    Debug.Log($"<color=cyan>[Tags]</color> added {desiredTags[i]} to tag manager");
                }
            }
                
            tagManager.ApplyModifiedProperties();
        }


        [MenuItem("Editor/Reset Playmode init scene")]
        public static void SetPlaymodeInitScene()
        {
            if (EditorBuildSettings.scenes.Length <= 1)
            {
                return;
            }

            var scene = EditorBuildSettings.scenes[0];
            SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                
            EditorSceneManager.playModeStartScene = sceneAsset;
        }
    }
}