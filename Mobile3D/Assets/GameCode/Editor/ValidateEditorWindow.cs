﻿using System;
using System.Collections.Generic;
using System.Text;
using GameCode.Editor.Global;
using GameCode.General;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.Editor
{
    public class ValidateEditorWindow : EditorWindow
    {
        [MenuItem(EditorConsts.ValidationToolbarRoot+nameof(ValidateEditorWindow))]
        private static void ShowWindow()
        {
            var window = GetWindow<ValidateEditorWindow>();
            window.titleContent = new GUIContent("ValidateEditorWindow");
            window.Show();
        }

        private GameObject m_Target;
        private string m_LastMessage;
        
        private void OnGUI()
        {
            m_Target = EditorGUILayout.ObjectField(m_Target, typeof(GameObject), true) as GameObject;

            if (m_Target != null)
            {
                if (GUILayout.Button("Validate Object"))
                {
                    StringBuilder sb = new StringBuilder();
                    
                    var allComponents = m_Target.GetComponents<IValidRestrictions>();

                    ValidateComponents(allComponents, sb);

                    m_LastMessage = sb.ToString();
                }
                
                if (GUILayout.Button("Validate Hierarchy"))
                {
                    StringBuilder sb = new StringBuilder();
                    
                    var allComponents = m_Target.GetComponentsInChildren<IValidRestrictions>(true);

                    ValidateComponents(allComponents, sb);

                    m_LastMessage = sb.ToString();
                }
            }

            GUILayout.TextArea(m_LastMessage);
        }

        private static void ValidateComponents(IValidRestrictions[] allComponents, StringBuilder sb)
        {
            var validationContext = new ValidationContext();
            
            for (int i = 0; i < allComponents.Length; i++)
            {
                try
                {
                    var outcome = allComponents[i].OnTryValidate(validationContext);

                    switch (outcome.Item1)
                    {
                        case IValidRestrictions.Outcome.NoChange:
                            sb.AppendLine(
                                $"[{i}] {allComponents[i].GetType()} - no change. Message: {outcome.Item2}"
                            );
                            break;
                        case IValidRestrictions.Outcome.Updated:
                            sb.AppendLine(
                                $"[{i}] {allComponents[i].GetType()} has beed updated. Message: {outcome.Item2}"
                            );
                            EditorUtility.SetDirty(allComponents[i].gameObject);
                            break;
                        case IValidRestrictions.Outcome.Failed:
                            sb.AppendLine(
                                $"!Fail while trying to validate [{i}] {allComponents[i].GetType()}. Message: {outcome.Item2}"
                            );
                            break;
                    }
                }
                catch (Exception e)
                {
                    sb.AppendLine(
                        $"#Exception at validation of [{i}] {allComponents[i].GetType()}: {e}"
                    );
                }
            }

            for (int i = 0; i < validationContext.ToDirtyList.Count; i++)
            {
                var objectToDirty = validationContext.ToDirtyList[i].gameObject;
                var componentToDirty = validationContext.ToDirtyList[i];
                PrefabUtility.RecordPrefabInstancePropertyModifications(objectToDirty);
                EditorUtility.SetDirty(objectToDirty);
                PrefabUtility.RecordPrefabInstancePropertyModifications(componentToDirty);
                EditorUtility.SetDirty(componentToDirty);
            }
        }
        
        private class ValidationContext : IValidationContext
        {
            public List<Component> ToDirtyList { get; }

            public ValidationContext()
            {
                ToDirtyList = new List<Component>();
            }
            
            public void AddToDirtyList(Component comp)
            {
                Assert.IsNotNull(comp);
                ToDirtyList.Add(comp);
            }
        }
    }
}