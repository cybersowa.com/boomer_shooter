﻿using System;
using GameCode.Editor.Global;
using UnityEditor;
using UnityEngine;
using ASM = global::AnimatedStateMachine;

namespace GameCode.Editor.AnimatedStateMachine
{
    public class AnimatedStateMachineInfo : EditorWindow
    {
        [MenuItem(EditorConsts.ToolToolbarRoot + nameof(AnimatedStateMachineInfo))]
        private static void ShowWindow()
        {
            var window = GetWindow<AnimatedStateMachineInfo>();
            window.titleContent = new GUIContent(nameof(AnimatedStateMachineInfo));
            window.Show();
        }
        
        
        private ASM m_Target;

        private void OnGUI()
        {
            GUILayout.BeginVertical();
            
            m_Target = EditorGUILayout.ObjectField("Target prefab: ", m_Target, typeof(ASM), true) as ASM;

            if (m_Target == null)
            {
                return;
            }

            GUILayout.Label("Current State is:");
            if (m_Target.CurrentState == null)
            {
                GUILayout.Label("null");
                return;
            }
            GUILayout.Label($"{m_Target.CurrentState}, play state: {m_Target.CurrentState.StateAnimation.state}, current time: {m_Target.CurrentState.StateAnimation.time}, duration: {m_Target.CurrentState.StateAnimation.duration}");

            GUILayout.Space(20);
            
            GUILayout.Label("Current Transition is:");
            if (m_Target.CurrentTransition == null)
            {
                GUILayout.Label("null");
                return;
            }
            GUILayout.Label($"{m_Target.CurrentTransition}, play state: {m_Target.CurrentTransition.Transition.state}, current time: {m_Target.CurrentTransition.Transition.time}, duration: {m_Target.CurrentTransition.Transition.duration}");
            
            GUILayout.EndVertical();
        }

        private void Update()
        {
            if (m_Target != null)
            {
                Repaint();
            }
        }
    }
}