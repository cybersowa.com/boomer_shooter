﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameCode.Editor.FileUtils;
using GameCode.Editor.Global;
using GameCode.General;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;



/*
    Chrześcijaństwo jest fundamentalnie niekompatybilne z wolnym rynkiem.
    Nie da się osiągnąć wolności w kraju, w którym chrześcijaństwo: jej wyznawcy, jej kultura lub jej moralność ma wpływy polityczne lub gospodarcze.
    Aby przetrwać, chrześcijaństwo nie stojuje się prawie w ogóle do swojej świętej księgi - inaczej musieliby się przyznać,
    iż to ich nauki stanową prawdziwe podwaliny komunizmu.
    Obecnie chrześcijanie się chełpią nawiązywaniem do kultury antycznego Rzymu. Kultury, którą sami zniszczyli!
    W starożytnym Rzymie panowały duże swobody rynkowe, jak również pluralizm społeczny i religijny.
    Chrześcijanie nienawidzili tego, ponieważ jest to religia ludzi mentalnie leniwych, nienawidzących ludzi bardziej zaradnych i lepiej zorganizowanych od nich.
    Biblia uczy, że bogacz to jest zły człowiek. Uczy również, iż należy zarabiać, ale nie na własny rachunek,
    lecz cały dorobek trzeba oddać lokalnej władzy (którą w domu oczywiście musi stanowić ojciec).
    Oryginalnie aby stać się chrześcijaninem trzeba było oddać cały swój majątek gminie chrześcijańskiej, której człowiek stawał się częścią.
    W takiej gminie członkowie NIE MIELI PRAWA POSIADAĆ WŁASNOŚCI PRYWATNEJ! Cały majątek w gminie był zarządzany przez starszych w gminie.
    Prawdziwe chrześcijaństwo jest to SOCJALIZM W NAJCZYSTSZEJ POSTACI!
    W starożytnym Rzymie panowała zasada, że obce grupy religijne mogą sobie spokojnie żyć i praktykować wszędzie w imperium, ale nie w stolicy! 
    Chrześcijanie, ponieważ nienawidzili Rzymu postanowili złamać tą zasadę i próbowali narzucić swoją religię stolicy Rzymu.
    Dopiero wtedy Rzym postanowił coś zrobić z tym wewnętrznym AGRESOREM i WROGIEM destabilizującym społeczeństwo.
    Ten wewnętrzny konflikt będzie trwał jeszcze przez jakieś 300 lat, podczas których cywilizacja Rzymu będzie stopniowo wyniszczana od wewnątrz.
    Konflikt ten skończy się dopiero, gdy chrześcijanie złożą obietnicę przekazania taniej, ale za to ideologicznie umotywowanej armii,
    która wbrew oficjalnym chrześcijaśkim zasadom, będzie mordować obcych w imię Rzymu w zamian, iż Rzym podda swoją kulturę chrześcijaństwu.
    Był to początek końca jednej z największych i najwspanialszych cywilizacji ludzkich.
    Chrześcijaństwo, będąc defacto socjalizmem z silną kastowością, nie mogłoby przetrwać bez pasożytowania na innej kulturze.
    Dlatego po rozpadzie Rzymu instytucje chrześcijańskie coraz żadziej stosowały się do swoich zasad,
    a coraz częściej czerpały z zasad starożytnego Rzymu oraz Grecji.
    Chrześcijanie nauczają większość wiernych biblii, aby mieć szerokie rzesze sfrustrowanych, biednych nędzników, którzy głęboko wierzą,
    iż to jakieś tajemne siły z zewnątrz są odpowiedzialni za ich nędzę. Podczas gdy to ich własna wiara, przekonania i kultura nie pozwala im przejrzeć na oczy,
    i nauczyć się uczciwego i dochodowego fachu. Zaś mniejszości wśród chrześcijan wmawia się, iż to jest dobrze, iż nie stosują się do zasad chrześcijaństwa
    i całkowicie ignorują to co jest w biblii, nawet jeżeli dotyczy to obrzydliwych przestępstw, o ile tylko wrzucają dużo na tacę.
    Współcześnie notorycznie to widać w każdym kraju, w którym chrześcijanie mają znaczący wpływ.
    Niszczą cudzą własność.
    Fizycznie atakują innych ludzi (szczególnie upodobali sobie bicie nieletnich dziewcząt z farbowanymi włosami).
    Wykorzystują wpływy polityczne, aby cenzurować ludzi o innych poglądach, jak również aby unikać odpowiedzialności
    za swoje błędy oraz wyrządzone przez organizacje chrześcijańskie krzywdy.
    Gdyby nie szeroki pakiet przywilejów:
     * fundusz kościelny, o który żadna inna grupa nie może się ubiegać
     * przywilej realizacji spraw w imieniu urzędu państwowego, o co nikt inny nie może się ubiegać
     * wykorzystywanie zajęć szkolnych w celu szerzenia swojej propagandy nie tylko bez uiszczania opłaty za wynajmowanie sali,
            ale wręcz pobierając za to haracz i utrudniając przy tym unikanie bycia narażonym na tą propagandę
     * przemycając swoją propagandę na innych zajęciach w szkole (w tym na j.polskim, matematyce, biologii i innych zajęciach),
            co w przypadku jakichkolwiek innych treści byłoby uznane za niespełnienie obowiązku przez nauczyciela,
            ale ich propaganda jest chroniona poprzez wypaczoną interpretację wolności słowa.
    Dla przypomnienia, wolność słowa oznacza iż masz prawo wykorzystywać swoje własne zasoby do kreowania dowolnego komunikatu
    (stąd wolność: ponieważ masz wolną rękę, aby używać tego co już jest twoją własnością; oraz słowa: ponieważ chodzi o komunikację między ludźmi).
    Co, o co efektywnie chrześcijanie walczą jest to przymus wysłuchania. Przymus: ponieważ chcą wykorzystać czyjeś zasoby według własnego widzimisię;
    oraz wysłuchania, ponieważ ten przymus nie ogranicza się tylko do wygłoszenia ich propagandy, lecz również do zmuszenia innych ludzi do bycia w takiej
    sytuacji, aby mimowolnie tą propagandę słyszeli i zapamiętali.
*/
namespace GameCode.Editor.AnimatedStateMachine
{
    public class AnimatedStateMachineWizzard : EditorWindow
    {
        [MenuItem(EditorConsts.ToolToolbarRoot + nameof(AnimatedStateMachineWizzard))]
        private static void ShowWindow()
        {
            var window = GetWindow<AnimatedStateMachineWizzard>();
            window.titleContent = new GUIContent(nameof(AnimatedStateMachineWizzard));
            window.Show();
        }

        private const int PreferredWindowWidth = 440;
        private const int ScrollWidth = 20;
        private const int VirtualColumns = 8;
        private const string DefaultTransitionNameFormat = "From{0}To{1}Transition";
        private const string StateSuffix = "State";
        private const string TimelineSuffix = "Timeline";

        private int VirtualWindowWidth => Mathf.Max((int)position.width - ScrollWidth, PreferredWindowWidth);
        private int UnitWidth => VirtualWindowWidth / VirtualColumns;
        private Vector2 m_ScrollPosition;
        
        private GameObject m_Prefab;

        private List<AnimationStateEntry> m_StateEntries = new List<AnimationStateEntry>();

        private void OnGUI()
        {
            if (GUILayout.Button("Set Window Width to Preferred"))
            {
                var pos = position;
                pos.width = PreferredWindowWidth + ScrollWidth;
                position = pos;
            }
            
            m_Prefab = EditorGUILayout.ObjectField("Target prefab: ", m_Prefab, typeof(GameObject), false) as GameObject;

            if (m_Prefab != null)
            {
                GUILayout.TextArea(AssetDatabase.GetAssetPath(m_Prefab));
            }

            m_ScrollPosition = GUILayout.BeginScrollView(m_ScrollPosition, false, true);
            
            GUILayout.BeginVertical();
            for (int i = 0; i < m_StateEntries.Count; i++)
            {
                if (DrawStateEntry(i, m_StateEntries[i]))
                {
                    m_StateEntries.RemoveAt(i);
                    break;
                }
            }

            DrawAddEntryButton();
            
            GUILayout.Space(24);
            GUILayout.EndVertical();
            
            GUILayout.EndScrollView();
            
            DrawBuildButton();
            GUILayout.Space(24);
        }

        private void DrawBuildButton()
        {
            GUILayout.Space(24);
            GUILayout.BeginHorizontal();
            GUILayout.Space(UnitWidth);
            if (GUILayout.Button("Create States And Assets", GUILayout.Width(6*UnitWidth)))
            {
                BuildAnimatedStateMachine();
            }
            GUILayout.EndHorizontal();
        }

        private void DrawAddEntryButton()
        {
            GUILayout.Space(24);
            GUILayout.BeginHorizontal();
            GUILayout.Space(UnitWidth);
            if (GUILayout.Button("Add Animated State", GUILayout.Width(6*UnitWidth)))
            {
                m_StateEntries.Add(new AnimationStateEntry()
                {
                    StateName = "UnsetStateName",
                    TransistionEntries = new List<TransistionEntry>()
                });
            }
            GUILayout.EndHorizontal();
        }

        private bool DrawStateEntry(int stateIndex, AnimationStateEntry stateEntry)
        {
            bool scheduledForRemoval = false;
            
            GUILayout.Space(16);
            
            GUILayout.BeginHorizontal();
            GUILayout.Label($"[{stateIndex}] State object name:", GUILayout.Width(3 * UnitWidth));
            stateEntry.StateName = GUILayout.TextField(stateEntry.StateName, GUILayout.Width(3*UnitWidth));
            GUILayout.Label(StateSuffix, GUILayout.Width(2*UnitWidth));
            GUILayout.EndHorizontal();

            //removal button
            GUILayout.BeginHorizontal();
            GUILayout.Space(5*UnitWidth);
            if (GUILayout.Button($"remove {stateEntry.StateName}", GUILayout.Width(3*UnitWidth)))
            {
                scheduledForRemoval = true;
            }
            GUILayout.EndHorizontal();
            

            //draw transitions
            for (int i = 0; i < stateEntry.TransistionEntries.Count; i++)
            {
                GUILayout.Space(12);
                GUILayout.BeginHorizontal();
                GUILayout.Space(1*UnitWidth);
                var transition = stateEntry.TransistionEntries[i];
                transition.TargetOfTransition = EditorGUILayout.IntField("Transition target:",
                    transition.TargetOfTransition, GUILayout.Width(5*UnitWidth));
                GUILayout.EndHorizontal();
                
                GUILayout.BeginHorizontal();
                GUILayout.Space(2*UnitWidth);
                // GUILayout.Label($"[{stateIndex}] ");
                try
                {
                    GUILayout.Label(string.Format(DefaultTransitionNameFormat, stateEntry.StateName,
                        m_StateEntries[transition.TargetOfTransition].StateName));
                }
                catch (Exception e)
                {
                    
                }
                GUILayout.EndHorizontal();
            }
            
            //add transition button
            GUILayout.BeginHorizontal();
            GUILayout.Space(1*UnitWidth);
            if (GUILayout.Button("Add Transition", GUILayout.Width(2*UnitWidth)))
            {
                stateEntry.TransistionEntries.Add(new TransistionEntry()
                {
                    TargetOfTransition = 0
                });
            }
            GUILayout.EndHorizontal();
            
            return scheduledForRemoval;
        }

        public class AnimationStateEntry
        {
            public string StateName;
            public List<TransistionEntry> TransistionEntries = new List<TransistionEntry>();
        }
        
        public class TransistionEntry
        {
            public int TargetOfTransition;
        }

        private void BuildAnimatedStateMachine()
        {
            // Object parentObject = EditorUtility.GetPrefabParent(obj);
            string prefabPath = AssetDatabase.GetAssetPath(m_Prefab);
            string prefabFolder = prefabPath.Substring(0, prefabPath.Length - m_Prefab.name.Length - ".prefab".Length - 1);
            GameObject prefabContents = PrefabUtility.LoadPrefabContents(prefabPath);
            
            //CREATE HIERARCHY IN PREFAB
            const string AnimatedStateMachineGOName = "AnimatedStateMachine";
            GameObject animatedStateMachineGO = null;
            for (int i = 0; i < prefabContents.transform.childCount; i++)
            {
                var contentChildGO = prefabContents.transform.GetChild(i).gameObject;
                if (contentChildGO.name.ToLower().Equals(AnimatedStateMachineGOName.ToLower()))
                {
                    animatedStateMachineGO = contentChildGO;
                    break;
                }
;           }

            if (animatedStateMachineGO == null)
            {
                animatedStateMachineGO = new GameObject(AnimatedStateMachineGOName);
                animatedStateMachineGO.transform.parent = prefabContents.transform;
                animatedStateMachineGO.transform.localPosition = Vector3.zero;
                animatedStateMachineGO.transform.localRotation = Quaternion.identity;
                animatedStateMachineGO.transform.localScale = Vector3.one;
            }

            global::AnimatedStateMachine animatedStateMachineComp =
                animatedStateMachineGO.GetComponent<global::AnimatedStateMachine>();
            if (animatedStateMachineComp == null)
            {
                animatedStateMachineComp = animatedStateMachineGO.AddComponent<global::AnimatedStateMachine>();
            }

            const string TimelinesSubfolderName = "Timelines";
            string timelinesPath = $"{prefabFolder}/{TimelinesSubfolderName}";
            if (AssetDatabase.IsValidFolder(timelinesPath) == false)
            {
                AssetDatabase.CreateFolder(prefabFolder, TimelinesSubfolderName);
            }

            Dictionary<string, AnimatedState> animatedStates = new Dictionary<string, AnimatedState>();
            {
                var existingAnimatedStates = animatedStateMachineGO.GetComponentsInChildren<AnimatedState>();
                foreach (var state in existingAnimatedStates)
                {
                    animatedStates.Add(state.name, state);
                }
            }
            
            //Add or get existing states without adding transitions
            for (int i = 0; i < m_StateEntries.Count; i++)
            {
                var stateObjectName = AddStateSuffix(m_StateEntries[i].StateName);
                AnimatedState stateComponent = null;
                GameObject stateGO = null;
                PlayableDirector stateAnimator = null;
                
                if (animatedStates.TryGetValue(stateObjectName, out stateComponent))
                {
                    stateGO = stateComponent.gameObject;
                }
                else
                {
                    stateGO = new GameObject(stateObjectName);
                    stateComponent = stateGO.AddComponent<AnimatedState>();
                    stateGO.transform.parent = animatedStateMachineGO.transform;
                    stateGO.transform.ResetLocalPosRotScale();
                    animatedStates.Add(stateObjectName, stateComponent);
                }

                stateAnimator = stateComponent.StateAnimation;
                if (stateAnimator == null)
                {
                    if (stateGO.TryGetComponent<PlayableDirector>(out var playableDirector))
                    {
                        stateAnimator = stateComponent.StateAnimation = playableDirector;
                    }
                    else
                    {
                        stateAnimator = stateComponent.StateAnimation = stateGO.AddComponent<PlayableDirector>();
                    }
                }


                if (stateAnimator.playableAsset == null)
                {
                    stateAnimator.playableAsset = ProjectFileUtils.GetOrCreateAsset<TimelineAsset>(timelinesPath, AddTimelineSuffix(stateObjectName));
                }

                if (animatedStateMachineComp.States.FindIndex(x => x.State == stateComponent) < 0)
                {
                    animatedStateMachineComp.States.Add(new global::AnimatedStateMachine.StateDefinition()
                    {
                        State = stateComponent,
                        TransitionsTo = new List<global::AnimatedStateMachine.StateTransition>()
                    });
                }

                stateAnimator.playOnAwake = false;
                stateAnimator.extrapolationMode = DirectorWrapMode.Loop;
            }
            
            //fill up the transitions
            for (int i = 0; i < m_StateEntries.Count; i++)
            {
                var sourceStateEntry = m_StateEntries[i];
                var stateGOName = AddStateSuffix(sourceStateEntry.StateName);
                var animatedStateComponent = animatedStates[stateGOName];
                var allTransitionsForState = new List<PlayableDirector>(animatedStateComponent.GetComponentsInChildren<PlayableDirector>());
                allTransitionsForState.Remove(animatedStateComponent.StateAnimation);
                
                for (int j = 0; j < sourceStateEntry.TransistionEntries.Count; j++)
                {
                    var transitionEntry = sourceStateEntry.TransistionEntries[j];
                    if (m_StateEntries.IsIndexValid(transitionEntry.TargetOfTransition))
                    {
                        var targetStateEntry = m_StateEntries[transitionEntry.TargetOfTransition];
                        var transitionGOName = string.Format(DefaultTransitionNameFormat, sourceStateEntry.StateName,
                            targetStateEntry.StateName);

                        PlayableDirector transitionAnimator =
                            allTransitionsForState.Find(x => x.name.Equals(transitionGOName));

                        if (transitionAnimator == null)
                        {
                            var transitionGO = new GameObject(transitionGOName);
                            transitionGO.transform.SetParent(animatedStateComponent.transform);
                            transitionGO.transform.ResetLocalPosRotScale();
                            transitionAnimator = transitionGO.AddComponent<PlayableDirector>();
                        }

                        if (transitionAnimator.playableAsset == null)
                        {
                            transitionAnimator.playableAsset = ProjectFileUtils.GetOrCreateAsset<TimelineAsset>(timelinesPath, AddTimelineSuffix(transitionGOName));
                        }

                        transitionAnimator.playOnAwake = false;
                        transitionAnimator.extrapolationMode = DirectorWrapMode.None;

                        var stateDefinition =
                            animatedStateMachineComp.States.Find(x => x.State == animatedStateComponent);
                        
                        var targetAnimatedStateComponent = animatedStates[AddStateSuffix(targetStateEntry.StateName)];

                        var transition = stateDefinition.TransitionsTo.Find(x => x.TargetState == targetAnimatedStateComponent);
                        if (transition == null)
                        {
                            transition = new global::AnimatedStateMachine.StateTransition();
                            transition.TargetState = targetAnimatedStateComponent;
                            stateDefinition.TransitionsTo.Add(transition);
                        }
                        transition.Transition = transitionAnimator;
                    }
                    else
                    {
                        Debug.LogError($"Index [{transitionEntry.TargetOfTransition}] for transition from {sourceStateEntry.StateName} is invalid");
                    }
                }
            }

            //SAVE ASSET
            PrefabUtility.SaveAsPrefabAsset(prefabContents, prefabPath, out bool success);
            if (success)
            {
                Debug.Log($"Successfully saved prefab at {prefabPath}");
            }
            else
            {
                Debug.LogError($"Failed when saving prefab at {prefabPath}");
            }
            PrefabUtility.UnloadPrefabContents(prefabContents);
            Debug.LogError("DID EFFECTIVELY NOTHING - FUNCTION NOT IMPLEMENTED!");//todo: change the log messege to properly inform whether or not there was any change made and if so was it success?
        }

        private string AddStateSuffix(string stateName)
        {
            return stateName + StateSuffix;
        }

        private string AddTimelineSuffix(string fileName)
        {
            return fileName + TimelineSuffix;
        }
    }
}