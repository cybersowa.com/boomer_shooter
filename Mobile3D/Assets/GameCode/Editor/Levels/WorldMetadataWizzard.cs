using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GameCode.Editor.AnimatedStateMachine;
using GameCode.Editor.FileUtils;
using GameCode.Editor.Global;
using GameCode.General;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldMetadataWizzard : EditorWindow
{
    [MenuItem(EditorConsts.ToolToolbarRoot + nameof(WorldMetadataWizzard))]
    private static void ShowWindow()
    {
        var window = GetWindow<WorldMetadataWizzard>();
        window.titleContent = new GUIContent(nameof(WorldMetadataWizzard));
        window.Show();
    }

    [SerializeField] private SceneAsset m_WorldSceneAsset;
    [SerializeField] private List<SceneAsset> m_ChunksSceneAssets = new List<SceneAsset>();
    [SerializeField] private string m_WorldSceneAssetPath;
    [SerializeField] private string m_WorldSceneAssetDir;
    [SerializeField] private string m_ChunkSceneAssetsDir;
    
    private void OnGUI()
    {
        const string DirectoryDivider = "\\";
        const string ChunksFoldername = "Chunks";
        const string ResourcesBaseDir = "Assets\\Resources\\WorldsMetadata";
        
        var worldSceneAsset = EditorGUILayout.ObjectField("World definition scene: ", m_WorldSceneAsset, typeof(SceneAsset), false) as SceneAsset;
        bool updateChunkList = false;
        
        if (m_WorldSceneAsset != worldSceneAsset)
        {
            m_WorldSceneAsset = worldSceneAsset;
            updateChunkList = true;
        }
        
        if (m_WorldSceneAsset == null)
        {
            return;
        }

        if (updateChunkList)
        {
            m_WorldSceneAssetPath = AssetDatabase.GetAssetPath(m_WorldSceneAsset);
            m_WorldSceneAssetDir = Path.GetDirectoryName(m_WorldSceneAssetPath);
            m_ChunkSceneAssetsDir = m_WorldSceneAssetDir + DirectoryDivider + ChunksFoldername;
            
            //todo: move this to after button is pressed
            if (AssetDatabase.IsValidFolder(m_ChunkSceneAssetsDir) == false)
            {
                AssetDatabase.CreateFolder(m_WorldSceneAssetDir, ChunksFoldername);
            }

            {
                m_ChunksSceneAssets.Clear();
                var chunkAssetId = AssetDatabase.FindAssets("t:Scene", new[] {m_ChunkSceneAssetsDir});
                
                Debug.Log($"Chunks found: {chunkAssetId.Length}");
                for (int i = 0; i < chunkAssetId.Length; i++)
                {
                    string chunkAssetPath = AssetDatabase.GUIDToAssetPath(chunkAssetId[i]);
                    Debug.Log($"Chunk {i} of GUID {chunkAssetId[i]} path: {chunkAssetPath}");
                    m_ChunksSceneAssets.Add(
                        AssetDatabase.LoadAssetAtPath<SceneAsset>(chunkAssetPath)
                        );
                }
            }
        }

        GUILayout.TextArea(m_WorldSceneAssetPath);
        GUILayout.TextArea(m_WorldSceneAssetDir);

        GUILayout.TextField($"Chunks (overal count {m_ChunksSceneAssets.Count}):");
        for (int i = 0; i < m_ChunksSceneAssets.Count; i++)
        {
            EditorGUILayout.ObjectField($"[{i}] ", m_ChunksSceneAssets[i], typeof(SceneAsset), false);
        }

        if (GUILayout.Button("Generate and update scene metadatas"))
        {
            string worldName = m_WorldSceneAsset.name;
            string worldMetadataDir = ResourcesBaseDir + "\\" + worldName;
            ProjectFileUtils.EnsureFolderInAssetsExist(worldMetadataDir);
            string chunkMetadataDir = worldMetadataDir + "\\" + "Chunks";
            ProjectFileUtils.EnsureFolderInAssetsExist(chunkMetadataDir);

            var worldSceneMetadata = ProjectFileUtils.GetOrCreateAsset<WorldSceneMetadata>(worldMetadataDir, worldName);
            worldSceneMetadata.SceneFileName = worldName;

            List<ChunkSceneMetadata> chunkMetadatas = new List<ChunkSceneMetadata>();
            for (int i = 0; i < m_ChunksSceneAssets.Count; i++)
            {
                var chunkAsset = m_ChunksSceneAssets[i];
                var chunkSceneMetadata = ProjectFileUtils.GetOrCreateAsset<ChunkSceneMetadata>(chunkMetadataDir, chunkAsset.name);
                chunkSceneMetadata.SceneFileName = chunkAsset.name;
                chunkMetadatas.Add(chunkSceneMetadata);
            }

            worldSceneMetadata.AllChunks = chunkMetadatas;
            
            EditorUtility.SetDirty(worldSceneMetadata);
            for (int i = 0; i < chunkMetadatas.Count; i++)
            {
                EditorUtility.SetDirty(chunkMetadatas[i]);
            }

            EditorSceneManager.EnsureUntitledSceneHasBeenSaved(
                "In order to process we need to unload all currently loaded scenes and load all scenes of selected World. " +
                "You have untitled/unknown scene. Do you want to save it?");

            List<Scene> currentlyOpenScenes = new List<Scene>();
            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                currentlyOpenScenes.Add(EditorSceneManager.GetSceneAt(i));
            }

            EditorSceneManager.SaveModifiedScenesIfUserWantsTo(currentlyOpenScenes.ToArray());

            EditorSceneManager.OpenScene(m_WorldSceneAssetPath, OpenSceneMode.Single);
            for (int i = 0; i < m_ChunksSceneAssets.Count; i++)
            {
                EditorSceneManager.OpenScene($"{m_ChunkSceneAssetsDir}\\{m_ChunksSceneAssets[i].name}.unity",
                    OpenSceneMode.Additive);
            }
            
            {//World Scene
                var worldScene = EditorSceneManager.GetSceneByName(m_WorldSceneAsset.name);
                List<GameObject> rootObjects = new List<GameObject>();
                worldScene.GetRootGameObjects(rootObjects);

                WorldManager worldManager = null;
                foreach (var obj in rootObjects)
                {
                    worldManager = obj.GetComponentInChildren<WorldManager>(true);
                    if (worldManager)
                    {
                        if (worldManager.gameObject != obj)
                        {
                            Debug.LogError($"WorldManager isn't a root object in the scene! Don't make messy setups! {worldManager.GetPathInHierarchy()}");
                        }

                        if (worldManager.gameObject.activeInHierarchy == false)
                        {
                            Debug.LogError($"WorldManager has to be enabled! Don't make messy setups! {worldManager.GetPathInHierarchy()}");
                        }
                        
                        break;
                    }
                }

                if (worldManager == null)
                {
                    GameObject worldManagerObj = new GameObject("_WorldManager");
                    EditorSceneManager.MoveGameObjectToScene(worldManagerObj, worldScene);
                    worldManagerObj.transform.SetAsFirstSibling();
                    worldManager = worldManagerObj.AddComponent<WorldManager>();
                }

                worldManager.Metadata = worldSceneMetadata;

                EditorSceneManager.SetActiveScene(worldScene);
            }

            for (int chI = 0; chI < worldSceneMetadata.AllChunks.Count; chI++)
            {//Chunk Scenes - todo: logic is almost the same as for World Scene, candidate to refactor
                var chunkScene = EditorSceneManager.GetSceneByName(worldSceneMetadata.AllChunks[chI].SceneFileName);
                List<GameObject> rootObjects = new List<GameObject>();
                chunkScene.GetRootGameObjects(rootObjects);

                ChunkManager chunkManager = null;
                foreach (var obj in rootObjects)
                {
                    chunkManager = obj.GetComponentInChildren<ChunkManager>(true);
                    if (chunkManager)
                    {
                        if (chunkManager.gameObject != obj)
                        {
                            Debug.LogError($"ChunkManager isn't a root object in the scene! Don't make messy setups! {chunkManager.GetPathInHierarchy()}");
                        }

                        if (chunkManager.gameObject.activeInHierarchy == false)
                        {
                            Debug.LogError($"ChunkManager has to be enabled! Don't make messy setups! {chunkManager.GetPathInHierarchy()}");
                        }
                        
                        break;
                    }
                }

                if (chunkManager == null)
                {
                    GameObject chunkManagerObj = new GameObject("_ChunkManager");
                    EditorSceneManager.MoveGameObjectToScene(chunkManagerObj, chunkScene);
                    chunkManagerObj.transform.SetAsFirstSibling();
                    chunkManager = chunkManagerObj.AddComponent<ChunkManager>();
                }

                chunkManager.Metadata = worldSceneMetadata.AllChunks[chI];
            }
        }
    }
}
