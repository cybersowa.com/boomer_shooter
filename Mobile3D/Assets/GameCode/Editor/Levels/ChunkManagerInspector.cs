﻿using System;
using System.Collections.Generic;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameCode.Editor.Levels
{
    [CustomEditor(typeof(ChunkManager))]
    public class ChunkManagerInspector : UnityEditor.Editor
    {
        private const string ChunkMetadataName = nameof(ChunkSceneMetadata);
        private const string LoadChunkZonesName = nameof(ChunkSceneMetadata.LoadChunkZones);
        private const string IgnoreLoadChunkZonesName = nameof(ChunkSceneMetadata.IgnoreLoadChunkZones);
        private const string UnloadPreventionZonesName = nameof(ChunkSceneMetadata.UnloadPreventionZones);
        
        private const float DefaultLoadPadding = 6.0f;
        private const float DefaultUnloadPreventionPadding = 14.0f;
        
        private List<BoxBoundsHandle> m_LoadZonesBoundsHandles = new List<BoxBoundsHandle>();
        private List<BoxBoundsHandle> m_IgnoreLoadZonesBoundsHandles = new List<BoxBoundsHandle>();
        private List<BoxBoundsHandle> m_UnloadPreventionZonesBoundsHandles = new List<BoxBoundsHandle>();
        private SerializedObject m_MetadataSerializedObject;
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            ChunkManager manager = (ChunkManager) target;
            ChunkSceneMetadata chunkSceneMetadata = manager.Metadata;
            if (chunkSceneMetadata == null)
            {
                return;
            }
            
            if (m_MetadataSerializedObject == null)
            {
                m_MetadataSerializedObject = new SerializedObject(chunkSceneMetadata);
            }
            
            m_MetadataSerializedObject.Update();
            
            var loadZonesMetadataProperty = m_MetadataSerializedObject.FindProperty(LoadChunkZonesName);
            EditorGUILayout.PropertyField(loadZonesMetadataProperty, new GUIContent($"{ChunkMetadataName}.{LoadChunkZonesName}"));
            
            var ignoreLoadZonesMetadataProperty = m_MetadataSerializedObject.FindProperty(IgnoreLoadChunkZonesName);
            EditorGUILayout.PropertyField(ignoreLoadZonesMetadataProperty, new GUIContent($"{ChunkMetadataName}.{IgnoreLoadChunkZonesName}"));
            
            var unloadPreventionZonesMetadataProperty = m_MetadataSerializedObject.FindProperty(UnloadPreventionZonesName);
            EditorGUILayout.PropertyField(unloadPreventionZonesMetadataProperty, new GUIContent($"{ChunkMetadataName}.{UnloadPreventionZonesName}"));

            if(m_MetadataSerializedObject.hasModifiedProperties)
            {
                m_MetadataSerializedObject.ApplyModifiedProperties();
            }
        }

        private void OnSceneGUI()
        {
            ChunkManager manager = (ChunkManager) target;
            ChunkSceneMetadata chunkSceneMetadata = manager.Metadata;
            
            bool hasBoundsChanged = false;
            
            hasBoundsChanged |= EnforceAtLeastOneSetOfBounds(chunkSceneMetadata, manager);

            EnsureCorrectNumberOfHandles(chunkSceneMetadata);
            
            {
                Handles.color = Color.yellow;
                for (int i = 0; i < chunkSceneMetadata.LoadChunkZones.Count; i++)
                {
                    hasBoundsChanged |= UpdateBound(chunkSceneMetadata, ref chunkSceneMetadata.LoadChunkZones, ref m_LoadZonesBoundsHandles, i);
                }
                
                Handles.color = Color.magenta;
                for (int i = 0; i < chunkSceneMetadata.IgnoreLoadChunkZones.Count; i++)
                {
                    hasBoundsChanged |= UpdateBound(chunkSceneMetadata, ref chunkSceneMetadata.IgnoreLoadChunkZones, ref m_IgnoreLoadZonesBoundsHandles, i);
                }
                
                Handles.color = Color.cyan;
                for (int i = 0; i < chunkSceneMetadata.UnloadPreventionZones.Count; i++)
                {
                    hasBoundsChanged |= UpdateBound(chunkSceneMetadata, ref chunkSceneMetadata.UnloadPreventionZones, ref m_UnloadPreventionZonesBoundsHandles, i);
                }
            }

            if (hasBoundsChanged)
            { 
                Repaint();
                m_MetadataSerializedObject.ApplyModifiedProperties();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunkSceneMetadata"></param>
        /// <param name="manager"></param>
        /// <returns>true if metadata has changed</returns>
        private bool EnforceAtLeastOneSetOfBounds(ChunkSceneMetadata chunkSceneMetadata, ChunkManager manager)
        {
            bool hasAddedBounds = false;
            
            //ensure at least one load chunk zone exists
            if (chunkSceneMetadata.LoadChunkZones.Count == 0)
            {
                Scene chunkScene = manager.gameObject.scene;
                Bounds bounds = GenerateEncapsulatingEverythingBounds(chunkScene);
                bounds.size += Vector3.one * DefaultLoadPadding;
                manager.Metadata.LoadChunkZones.Add(bounds);
                hasAddedBounds = true;
            }
            
            //ensure at least one chunk unload prevention zone exists
            if (chunkSceneMetadata.UnloadPreventionZones.Count == 0)
            {
                Scene chunkScene = manager.gameObject.scene;
                Bounds bounds = GenerateEncapsulatingEverythingBounds(chunkScene);
                bounds.size += Vector3.one * DefaultUnloadPreventionPadding;
                manager.Metadata.UnloadPreventionZones.Add(bounds);
                hasAddedBounds = true;
            }

            return hasAddedBounds;
        }

        private void EnsureCorrectNumberOfHandles(ChunkSceneMetadata chunkSceneMetadata)
        {
            {// load zones
                //ensure we have all bounds handles we need
                while (m_LoadZonesBoundsHandles.Count > chunkSceneMetadata.LoadChunkZones.Count)
                {
                    m_LoadZonesBoundsHandles.RemoveAt(m_LoadZonesBoundsHandles.Count - 1);
                }

                while (m_LoadZonesBoundsHandles.Count < chunkSceneMetadata.LoadChunkZones.Count)
                {
                    m_LoadZonesBoundsHandles.Add(new BoxBoundsHandle());
                }
            }
            
            {// ignore load zones
                //ensure we have all bounds handles we need
                while (m_IgnoreLoadZonesBoundsHandles.Count > chunkSceneMetadata.IgnoreLoadChunkZones.Count)
                {
                    m_IgnoreLoadZonesBoundsHandles.RemoveAt(m_IgnoreLoadZonesBoundsHandles.Count - 1);
                }

                while (m_IgnoreLoadZonesBoundsHandles.Count < chunkSceneMetadata.IgnoreLoadChunkZones.Count)
                {
                    m_IgnoreLoadZonesBoundsHandles.Add(new BoxBoundsHandle());
                }
            }
            
            {// unload prevention zones
                //ensure we have all bounds handles we need
                while (m_UnloadPreventionZonesBoundsHandles.Count > chunkSceneMetadata.UnloadPreventionZones.Count)
                {
                    m_UnloadPreventionZonesBoundsHandles.RemoveAt(m_UnloadPreventionZonesBoundsHandles.Count - 1);
                }

                while (m_UnloadPreventionZonesBoundsHandles.Count < chunkSceneMetadata.UnloadPreventionZones.Count)
                {
                    m_UnloadPreventionZonesBoundsHandles.Add(new BoxBoundsHandle());
                }
            }
        }

        private bool UpdateBound(ChunkSceneMetadata chunkSceneMetadata, ref List<Bounds> boundsList, ref List<BoxBoundsHandle> handlesList, int index)
        {
            Bounds handleBounds = boundsList[index];
            handlesList[index].center = handleBounds.center;
            handlesList[index].size = handleBounds.size;

            EditorGUI.BeginChangeCheck();
            handlesList[index].DrawHandle();
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(chunkSceneMetadata, "Change Bounds");
                handleBounds.center = handlesList[index].center;
                handleBounds.size = handlesList[index].size;
                boundsList[index] = handleBounds;
                return true;
            }
            
            return false;
        }

        private Bounds GenerateEncapsulatingEverythingBounds(Scene scene)
        {
            var rootObjects = scene.GetRootGameObjects();
            List<Collider> allColliders = new List<Collider>();
            for (int i = 0; i < rootObjects.Length; i++)
            {
                allColliders.AddRange(
                    rootObjects[i].GetComponentsInChildren<Collider>(false)
                    );
            }

            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (int i = 0; i < allColliders.Count; i++)
            {
                var collider = allColliders[i];
                min = Vector3.Min(min, collider.bounds.min);
                max = Vector3.Max(max, collider.bounds.max);
            }

            Vector3 center = (min + max) * 0.5f;
            Vector3 size = max - min;

            Bounds bounds = new Bounds(center, size);

            return bounds;
        }
    }
}