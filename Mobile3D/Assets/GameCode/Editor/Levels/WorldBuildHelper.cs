﻿using System;
using GameCode.Editor.Global;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.Levels
{
    public class WorldBuildHelper : EditorWindow
    {
        [MenuItem(EditorConsts.ValidationToolbarRoot+nameof(WorldBuildHelper))]
        private static void ShowWindow()
        {
            var window = GetWindow<WorldBuildHelper>();
            window.titleContent = new GUIContent("WorldBuildHelper");
            window.Show();
        }

        private string m_WorldName = "";
        private string m_LastSummary = "";
        
        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            
            m_WorldName = EditorGUILayout.TextField("World name: ", m_WorldName);
            if (string.IsNullOrWhiteSpace(m_WorldName) == false)
            {
                if (GUILayout.Button("Create folders"))
                {
                    try
                    {
                        AssetDatabase.StartAssetEditing();
                        m_LastSummary = "";
                        m_LastSummary += new FolderAppendChain().Add("(Scenes)")
                            .Add(m_WorldName)
                            .Add("Chunks")
                            .GetPath();
                        
                        m_LastSummary += "\n";
                        m_LastSummary += new FolderAppendChain().Add("Resources")
                            .Add("Localization")
                            .Add(m_WorldName)
                            .GetPath();
                        
                        m_LastSummary += "\n";
                        m_LastSummary += new FolderAppendChain().Add("Resources")
                            .Add("Pickups")
                            .Add(m_WorldName)
                            .GetPath();
                        
                        m_LastSummary += "\n";
                        m_LastSummary += new FolderAppendChain().Add("Resources")
                            .Add("WorldsMetadata")
                            .Add(m_WorldName)
                            .Add("Chunks")
                            .GetPath();
                    }
                    finally
                    {
                        AssetDatabase.StopAssetEditing();
                    }
                }
            }

            EditorGUILayout.LabelField("Last Action Summary:");
            EditorGUILayout.TextArea(m_LastSummary);
            
            EditorGUILayout.EndVertical();
        }
        
        private class FolderAppendChain
        {
            private string m_PathBase = "Assets";

            public FolderAppendChain()
            {
            }

            private FolderAppendChain(string pathBase)
            {
                m_PathBase = pathBase;
            }

            public FolderAppendChain Add(string subfolder)
            {
                string targetPath = m_PathBase + "/" + subfolder;
                if (false == AssetDatabase.IsValidFolder(targetPath))
                {
                    AssetDatabase.CreateFolder(m_PathBase, subfolder);
                }

                return new FolderAppendChain(targetPath);
            }

            public string GetPath() => m_PathBase;
        }
    }
}