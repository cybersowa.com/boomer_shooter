﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameCode.Editor.Global;
using GameCode.GameState;
using GameCode.Levels;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace GameCode.Editor.Levels
{
    [CustomEditor(typeof(PlayerCheckpoint))]
    public class PlayerCheckpointInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Start From Here - doesn't work right now. TODO: make it work again"))
            {
                // var playerCheckpoint = target as PlayerCheckpoint;
                //
                // SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(EditorSceneManager.GetActiveScene().path);
                // EditorSceneManager.playModeStartScene = sceneAsset;
                //
                // GameSnapshot serializedGameState = new GameSnapshot()
                // {
                //     ForceFirstCheckpoint = false,
                //     CheckpointID = playerCheckpoint.ID,
                //     WorldName = sceneAsset.name,
                //     PlayerPosition = playerCheckpoint.transform.position,
                //     PlayerRotation = playerCheckpoint.transform.rotation,
                //     GameComponents = new List<IGameComponent>(),
                //     AllManagersStates = new List<ALevelStaticManger.ASerializedState>()
                // };
                //
                // EditorStaticConfig.EnterPlaymodeWithState(serializedGameState);
            }
        }
    }
}