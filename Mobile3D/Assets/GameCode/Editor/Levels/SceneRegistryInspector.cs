﻿using System.Collections.Generic;
using System.Linq;
using GameCode.Editor.UnityAssetDatabase;
using GameCode.Levels.Metadata;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.Levels
{
    [CustomEditor(typeof(SceneRegistry))]
    public class SceneRegistryInspector : UnityEditor.Editor
    {
        private SerializedObject m_SceneRegistrySerializedObject;
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            bool markDirty = false;
            
            if (m_SceneRegistrySerializedObject == null)
            {
                m_SceneRegistrySerializedObject = new SerializedObject(target);
            }

            if (GUILayout.Button("Find and Register All Scenes (Worlds and Chunks)"))
            {
                // var allScenes = AssetDatabaseUtils.FindAllAssetsOfTypeAndDerrived<ASceneMetadata>();
                var allScenes = Resources.LoadAll<ASceneMetadata>("").ToList();
                SceneRegistry registry = (SceneRegistry) target;
                
                registry.Editor_AllScenes.Clear();
                registry.Editor_AllScenes.AddRange(allScenes);
                
                //ensure all have valid guids
                Dictionary<int, ASceneMetadata> dict = new Dictionary<int, ASceneMetadata>();
                for (int i = 0; i < allScenes.Count; i++)
                {
                    var scene = allScenes[i];

                    while (scene.SceneGUID == 0 || dict.ContainsKey(scene.SceneGUID))
                    {
                        scene.SceneGUID = ID.GenerateNewIntUID();
                        EditorUtility.SetDirty(scene);
                    }
                }
                
                registry.Editor_InitRuntimeCache();

                markDirty = true;
            }
            
            if(markDirty)
            {
                m_SceneRegistrySerializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
                Repaint();
            }
        }
    }
}