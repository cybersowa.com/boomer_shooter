﻿using System.Collections.Generic;
using GameCode.InspectorUtils;
using GameCode.Utils;
using UnityEditor;
using UnityEngine;

namespace GameCode.Editor.InspectorUtils
{
    /// <summary>
    /// based on: https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor/
    /// </summary>
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyAttributeDrawer : PropertyDrawer
    {
        /// <summary>
        /// Unity method for drawing GUI in Editor
        /// </summary>
        /// <param name="position">Position.</param>
        /// <param name="property">Property.</param>
        /// <param name="label">Label.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Saving previous GUI enabled value
            var previousGUIState = GUI.enabled;
            // Disabling edit for property
            GUI.enabled = false;
            // Drawing Property
            EditorGUI.PropertyField(position, property, label);
            // Setting old GUI enabled value
            GUI.enabled = previousGUIState;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label);
        }
    }

    public static class InspectorUtils
    {
        const float LabelWidth = 100.0f;
        const float MinObjectFieldWidth = 100.0f;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">will be ignored when null</param>
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>true if list was modified. Otherwise false</returns>
        public static bool DisplayList<T>(string title, List<T> list, bool autoRemoveNulls = false) where T : UnityEngine.Object
        {
            var windowWidth = EditorGUIUtility.currentViewWidth;
            
            const float ButtonWidth = 25.0f;
            const float Padding = 5 * 5.0f;
            var objectFieldWidth = windowWidth - LabelWidth - 3 * ButtonWidth - Padding;
            
            if(false == string.IsNullOrWhiteSpace(title))
            {
                GUILayout.Label(title);
            }

            if (objectFieldWidth < MinObjectFieldWidth)
            {
                GUILayout.Label($"Window width of {windowWidth} is too narrow to display the list");
                return false;
            }
            
            bool wasModified = false;
            
            EditorGUILayout.BeginVertical();

            for (int i = 0; i < list.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label($"[{i}]",GUILayout.Width(LabelWidth));
                var entry = EditorGUILayout.ObjectField(list[i], typeof(T), GUILayout.Width(objectFieldWidth)) as T;
                bool wantsToRemove = GUILayout.Button("-",GUILayout.Width(ButtonWidth));
                bool wantsToMoveUp = GUILayout.Button("^",GUILayout.Width(ButtonWidth));
                bool wantsToMoveDown = GUILayout.Button("v",GUILayout.Width(ButtonWidth));
                EditorGUILayout.EndHorizontal();

                if (autoRemoveNulls && entry == null)
                {
                    wantsToRemove = true;
                }
                
                if (wantsToRemove)
                {
                    list.RemoveAt(i);
                    wasModified = true;
                    break;
                }

                if (wantsToMoveUp && i > 0)
                {
                    var e = list[i];
                    list[i] = list[i -1];
                    list[i - 1] = e;
                    wasModified = true;
                    break;
                }

                if (wantsToMoveDown && i < list.LastIndex())
                {
                    var e = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = e;
                    wasModified = true;
                    break;
                }

                if (entry != list[i])
                {
                    list[i] = entry;
                    wasModified = true;
                }
            }
            
            GUILayout.Label($"Count: {list.Count}");
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Add new:",GUILayout.Width(LabelWidth));
            var newEntry = EditorGUILayout.ObjectField(default(T), typeof(T), GUILayout.Width(objectFieldWidth)) as T;
            EditorGUILayout.EndHorizontal();
            
            if (newEntry != null)
            {
                list.Add(newEntry);
            }
            
            EditorGUILayout.EndVertical();

            return wasModified;
        }

        public static T ObjectField<T>(string label, T obj, float labelWidth = LabelWidth) where T : UnityEngine.Object
        {
            if (string.IsNullOrWhiteSpace(label))
            {
                EditorGUILayout.BeginHorizontal();
                obj = EditorGUILayout.ObjectField(obj, typeof(T)) as T;
                EditorGUILayout.EndHorizontal();
            }
            else
            {
                var windowWidth = EditorGUIUtility.currentViewWidth;
            
                const float Padding = 2 * 5.0f;
                var objectFieldWidth = windowWidth - LabelWidth - Padding;
            
                if (objectFieldWidth < MinObjectFieldWidth)
                {
                    GUILayout.Label($"Window width of {windowWidth} is too narrow to display the object field");
                    return default(T);
                }
            
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label(label,GUILayout.Width(LabelWidth));
                obj = EditorGUILayout.ObjectField(obj, typeof(T), GUILayout.Width(objectFieldWidth)) as T;
                EditorGUILayout.EndHorizontal();
            }

            return obj;
        }
    }
}