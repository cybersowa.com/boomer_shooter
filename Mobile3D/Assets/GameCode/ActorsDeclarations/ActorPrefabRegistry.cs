﻿using System.Collections.Generic;
using GameCode.General;
using Unity.Assertions;
using UnityEngine;

namespace GameCode.ActorsDeclarations
{
    [CreateAssetMenu(fileName = nameof(ActorPrefabRegistry), menuName = Const.MenuNamePoolingGroup + nameof(ActorPrefabRegistry))]
    public partial class ActorPrefabRegistry : ScriptableObject
    {
        private static ActorPrefabRegistry s_Instance;

        public static ActorPrefabRegistry Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = Resources.Load<ActorPrefabRegistry>("Pooling/ActorPrefabRegistry");
                    s_Instance.InitRuntimeCache();
                }

                return s_Instance;
            }
        }
         

        [SerializeField] private List<ASessionActor> m_ActorPrefabList = new List<ASessionActor>();
       

        private Dictionary<PrefabID, ASessionActor> m_ActorPrefabs = new Dictionary<PrefabID, ASessionActor>();

        private void InitRuntimeCache()
        {
            m_ActorPrefabs.Clear();

            for (int i = 0; i < m_ActorPrefabList.Count; i++)
            {
                Assert.IsNotNull(m_ActorPrefabList[i]);
                m_ActorPrefabs.Add(m_ActorPrefabList[i].PrefabID, m_ActorPrefabList[i]);
            }
        }

        public bool TryGet(PrefabID id, out ASessionActor actor)
        {
            return m_ActorPrefabs.TryGetValue(id, out actor);
        }
    }
}