﻿using System;
using GameCode.Enemies;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using GameCode.Pooling;
using Unity.Assertions;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = System.Diagnostics.Debug;

namespace GameCode.ActorsDeclarations
{
    /// <summary>
    /// might be better to store InstanceID in ASerializableActorState for respawning purposes
    ///
    /// need to figure out how we want to handle unloading chunks overlapping Actors.
    /// In general we want to take snapshot of Actors memento so we can reload them when reloading chunk.
    /// Probably it has to be a separate layer that checks which Actors are within the chunk we want to unload,
    /// and also we want to store Actor mementos in buckets, where each bucket is assigned to different chunk,
    /// so when we load chunk we can easily determine which Actors to respawn 
    /// </summary>
    public partial class Actor
    {
        /// <summary>
        /// Generates new InstanceID
        /// </summary>
        /// <param name="actorPrefab"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <param name="world">World where the actor should reside. Pass null is actor should be spawned for global scope</param>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        public static ASessionActor Spawn(ASessionActor actorPrefab, Vector3 position, Quaternion rotation, WorldManager world,
            out InstanceID instanceID)
        {
            bool isGlobal = world == null;
            int instanceIDWorld = 0;
            Scene spawnScene = new Scene();

            if (world != null)
            {
                instanceIDWorld = world.Metadata.SceneGUID;
                spawnScene = SceneManager.GetSceneByName(world.Metadata.name);
            }
            
            ulong instanceIDRaw = isGlobal ? Game.GetNextInstanceID() : world.GetNextInstanceID();
            instanceID = new InstanceID(instanceIDRaw, instanceIDWorld);

            var actorInstance = ObjectPool.Instantiate<ASessionActor>(actorPrefab, position, rotation, spawnScene);
            actorInstance.InstanceID = instanceID;
            actorInstance.Load(actorPrefab.CreateDefaultSnapshot());

            if (world == null)
            {
                Game.GlobalActors.Add(actorInstance);
            }
            else
            {
                world.RuntimeObjectsDatabase.Add<ASessionActor>(actorInstance);
            }

            return actorInstance;
        }
        
        /// <summary>
        /// Provide InstanceID which should be assigned to Actor
        /// </summary>
        /// <param name="actorPrefab"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <param name="instanceID">remember to respawn actors only when target world is already loaded!</param>
        /// <returns></returns>
        public static ASessionActor Respawn(ASessionActor actorPrefab, Vector3 position, Quaternion rotation,
            AActorSnapshot snapshot)
        {
            var worldSceneID = snapshot.InstanceID.WorldID;
            bool isGlobal = worldSceneID == 0;
            ASceneMetadata sceneMetadata = null;
            Scene spawnScene = new Scene();
            
            if (isGlobal == false)
            {
                sceneMetadata = SceneRegistry.Instance.WorldScenesMap[worldSceneID];
                spawnScene = SceneManager.GetSceneByName(sceneMetadata.name);
            }

            var actorInstance = ObjectPool.Instantiate<ASessionActor>(actorPrefab, position, rotation, spawnScene);
            actorInstance.InstanceID = snapshot.InstanceID;
            actorInstance.Load(snapshot);

            if (isGlobal)
            {
                Game.GlobalActors.Add(actorInstance);
            }
            else
            {
                Debug.Assert(Game.CurrentWorld != null, "Game.CurrentWorld != null");
                Assert.IsTrue(sceneMetadata.name.Equals(Game.CurrentWorld.gameObject.scene.name));
                Game.CurrentWorld.RuntimeObjectsDatabase.Add<ASessionActor>(actorInstance);
            }

            return actorInstance;
        }

        static public void Despawn(ASessionActor actor, bool preserveSnapshot = true)
        {
            var worldSceneID = actor.InstanceID.WorldID;
            bool isGlobal = worldSceneID == 0;
            ASceneMetadata sceneMetadata = null;
            
            if (isGlobal == false)
            {
                sceneMetadata = SceneRegistry.Instance.WorldScenesMap[worldSceneID];
            }

            if (preserveSnapshot)
            {
                var allChunks = Game.GetChunkMetadatasForWorld(Game.CurrentWorld);
                var chunk = Game.FindChunkForActor(actor, allChunks);
                int chunkID = (chunk != null) ? chunk.SceneGUID : 0;
                Game.RuntimeState.AddOrReplace(actor.CreateSnapshotForCurrentState(), chunkID);
            }
            else
            {
                Game.RuntimeState.Remove(actor.InstanceID);
            }
            
            if (isGlobal)
            {
                Game.GlobalActors.Remove(actor);
            }
            else
            {
                Debug.Assert(Game.CurrentWorld != null, "Game.CurrentWorld != null");
                Assert.IsTrue(sceneMetadata.name.Equals(Game.CurrentWorld.gameObject.scene.name));
                Game.CurrentWorld.RuntimeObjectsDatabase.Remove<ASessionActor>(actor);
            }
            
            actor.InstanceID = InstanceID.Invalid;
            ObjectPool.Destroy(actor);
        }
    }
}