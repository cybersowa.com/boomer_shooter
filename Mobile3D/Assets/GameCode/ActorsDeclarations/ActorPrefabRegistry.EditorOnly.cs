﻿using System.Collections.Generic;

namespace GameCode.ActorsDeclarations
{
#if UNITY_EDITOR
    
    public partial class ActorPrefabRegistry
    {
        public List<ASessionActor> Editor_ActorPrefabList => m_ActorPrefabList;
        
        public void Editor_InitRuntimeCache()
        {
            InitRuntimeCache();
        }
    }
    
#endif
}