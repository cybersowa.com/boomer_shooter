﻿using UnityEngine;

#if UNITY_EDITOR
    public abstract partial class ASessionActor
    {
        public void Editor_SetPrefabID(PrefabID prefabID)
        {
            m_PrefabID = prefabID;
        }
    }
    
#endif