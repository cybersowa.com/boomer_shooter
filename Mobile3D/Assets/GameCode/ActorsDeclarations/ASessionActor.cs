using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GameCode.InspectorUtils;
using GameCode.Pooling;
using UnityEngine;

/// <summary>
/// Aka Dynamic/Temporary Actor (as opposite to StaticManagers)
/// 
/// The idea is this type of objects are never placed in Scene directly.
/// They are always managed through Pooling System
/// and through they respective managers
///
/// That way managers just have to serialize simplified data
/// and loading game state can be done by simply wiping out all ASessionActors
/// and reinstancing them 
/// </summary>
public abstract partial class ASessionActor : MonoBehaviour, IPoolable
{
    [SerializeField]
    private PrefabID m_PrefabID;

    public PrefabID PrefabID { get => m_PrefabID; }

    public InstanceID InstanceID;

    private bool m_IsInPool = false;
    public bool IsInPool { get => m_IsInPool; }

    public void SetPooledFlag(bool isPooled)
    {
        m_IsInPool = isPooled;
    }

    public virtual void OnBeingReleasedFromPoolBucket()
    {
    }

    public virtual void OnBeingPushedToPoolBucket()
    {
    }

    public abstract AActorSnapshot CreateSnapshotForCurrentState();

    public abstract AActorSnapshot CreateDefaultSnapshot();

    protected void AssignBaseFields(AActorSnapshot snapshot)
    {
        snapshot.PrefabID = PrefabID;
        snapshot.InstanceID = InstanceID;
        
        var t = transform;
        snapshot.Position = t.position;
        snapshot.Rotation = t.rotation;
    }

    public abstract void Load(AActorSnapshot fromSnapshot);
}

[SerializeField]
public abstract partial class AActorSnapshot : ICloneable
{
    public PrefabID PrefabID;
    public InstanceID InstanceID;
    public Vector3 Position;
    public Quaternion Rotation;
    public abstract object Clone();
}

[Serializable]
public struct PrefabID
{
    [GameCode.InspectorUtils.ReadOnly]
    [SerializeField]
    private int m_RawValue;

    public int RawValue
    {
        get => m_RawValue;
        
#if UNITY_EDITOR
        set => m_RawValue = value;
#else
        //temp hack as what we want here is init, but that requires C# 9.0 and project itself probably won't be updated to it
        set => m_RawValue = value;
#endif
    }

    public static PrefabID Empty
    {
        get => 0;
    }

    public static implicit operator int(PrefabID id) => id.RawValue;
    public static implicit operator PrefabID(int rawValue) => new PrefabID() {RawValue = rawValue};

    public override int GetHashCode()
    {
        return RawValue;
    }

    public override string ToString()
    {
        return $"PrefabID[{m_RawValue}]";
    }
}

[Serializable]
public struct ActorPrefabReference
{
    public PrefabID ID;

    public static implicit operator PrefabID(ActorPrefabReference prefabRef) => prefabRef.ID;
}


public struct InstanceID
{
    public readonly ulong ID;
    public readonly int WorldID;

    public InstanceID(InstanceID toCopyFrom)
    {
        ID = toCopyFrom.ID;
        WorldID = toCopyFrom.WorldID;
    }
    
    public InstanceID(ulong id, int worldID)
    {
        ID = id;
        WorldID = worldID;
    }

    public override int GetHashCode()
    {
        //https://stackoverflow.com/questions/892618/create-a-hashcode-of-two-numbers

        int hash = 23;
        hash = hash * 31 + ID.GetHashCode();
        hash = hash * 31 + WorldID.GetHashCode();

        return hash;
    }

    public static InstanceID Invalid => new InstanceID(0, 0);

    public static bool operator ==(InstanceID id1, InstanceID id2)
    {
        return id1.WorldID == id2.WorldID && id1.ID == id2.ID;
    }
    
    public static bool operator !=(InstanceID id1, InstanceID id2)
    {
        return id1.WorldID != id2.WorldID || id1.ID != id2.ID;
    }
    
    public bool IsGlobalInstance()
    {
        return WorldID == 0;
    }
}