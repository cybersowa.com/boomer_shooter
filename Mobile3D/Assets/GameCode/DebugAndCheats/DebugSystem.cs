using System.Text;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.LowLevel;

public class DebugSystem : SystemBase
{
    protected override void OnUpdate()
    {
        bool logPlayerLoopStructure = Input.GetKeyDown(KeyCode.I);

        if (logPlayerLoopStructure)
        {
            var currentPlayerLoop = PlayerLoop.GetCurrentPlayerLoop();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Printing all current player loop systems for frame {UnityEngine.Time.frameCount}");

            PlayerLoopUtils.RecursivePlayerLoopPrint(currentPlayerLoop, sb, 0);
            
            Debug.Log(sb);
        }
    }
}
