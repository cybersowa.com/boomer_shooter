using System.Collections;
using System.Collections.Generic;
using System.Text;
using GameCode.General;
using GameCode.LocalizationEngine;
using TMPro;
using UnityEngine;

namespace GameCode.Localization
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedStaticLabel : MonoBehaviour, IValidRestrictions
    {
        public LocalizedText Text;
        public TextMeshProUGUI Label;

        public (IValidRestrictions.Outcome, string) OnTryValidate(IValidationContext ctx)
        {
            StringBuilder sb = new StringBuilder();
            bool hasBeenUpdated = false;
            if (Label == null)
            {
                Label = GetComponent<TextMeshProUGUI>();
                sb.Append("Assigned Label ref. ");
                hasBeenUpdated = true;
            }

            if (Text == null)
            {
                sb.Append("Localized Text isn't assigned! ");
                return (IValidRestrictions.Outcome.Failed, sb.ToString());
            }

            string localizedText = Text.Get(Lang.Fallback);
            if (localizedText.Equals(Label.text) == false)
            {
                Label.text = localizedText;
                sb.Append("Updated displayed text. ");
                hasBeenUpdated = true;
            }

            if (hasBeenUpdated)
            {
                return (IValidRestrictions.Outcome.Updated, sb.ToString());
            }

            return (IValidRestrictions.Outcome.NoChange, sb.ToString());
        }
    }
}
