using System;
using GameCode.LocalizationEngine;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TMPTextSwitcherBehaviour : PlayableBehaviour
{
    public Color color = Color.white;
    public float fontSize = 14;
    public LocalizedText text;
    public bool ShowWholeTextAtOnce = true;
    public int LettersPerSecond => 15;
}
