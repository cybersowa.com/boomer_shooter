﻿using System;
using UnityEngine;
using GameCode.General;


[CreateAssetMenu(fileName = nameof(SerializedPickupData), menuName = Const.MenuNameGame + nameof(SerializedPickupData))]
public class SerializedPickupData : ScriptableObject
{
    public static readonly int EmptyID = Guid.Empty.GetHashCode();

    public static int GenerateID()
    {
        //GetInstanceID() isn't stable across reopening editor
        //so to avoid possible clash caused by generating the same id from different runs
        //we use other method
        return Guid.NewGuid().GetHashCode();
    }
    
    [SerializeField]
    private int m_ID = EmptyID;

    public int ID => m_ID;

    [ContextMenu(nameof(ResetID))]
    private void ResetID()
    {
        m_ID = GenerateID();
    }
    
    private void OnEnable()
    {
        if (m_ID.Equals(EmptyID))
        {
            ResetID();
        }
    }
}