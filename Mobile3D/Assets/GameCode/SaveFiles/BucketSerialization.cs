﻿using System;

namespace GameCode.BucketSerialization
{
    public struct SerializedBucketHeader
    {
        public int SerializationFunctionID;
        public int Version;
        /// <summary>
        /// Should match DataBucket.Length
        /// </summary>
        public int BucketSize;
    }
    
    /// <summary>
    /// TODO: create separate DeserializationBucket which differs in DataBucket which instead of being a separate array instance, it's pointing to one shared data array and has startIndex and length fields
    /// </summary>
    public struct SerializedBucket
    {
        public SerializedBucketHeader Header;
        public byte[] DataBucket;
    }

    // public interface IBucketSerializable <- To Be Removed as we have completely separate registry for function dispatcher
    // {
    //     public SerializedBucket SerializeToBucket();
    //     public void DeserializeFromBucket(SerializedBucket bucket);
    // }

    // https://docs.microsoft.com/en-gb/dotnet/csharp/programming-guide/types/how-to-convert-a-byte-array-to-an-int
    public static class SerializedBucketUtils
    {
        public static bool GetBucket(byte[] span, int startIndex, out SerializedBucket bucket, out int newStartIndex)
        {
            newStartIndex = startIndex;
            int index = startIndex;
            bucket.Header.SerializationFunctionID = BitConverter.ToInt32(span, index);
            index += sizeof(int);
            bucket.Header.Version = BitConverter.ToInt32(span, index);
            index += sizeof(int);
            bucket.Header.BucketSize = BitConverter.ToInt32(span, index);
            index += sizeof(int);

            bucket.DataBucket = new byte[bucket.Header.BucketSize];
            Array.Copy(span, index, bucket.DataBucket, 0, bucket.Header.BucketSize);
            // for (int i = 0; i < bucket.DataBucket.Length; i++)
            // {
            //     bucket.DataBucket[i] = span[index];
            //     index += sizeof(byte);
            // }

            newStartIndex = index + bucket.Header.BucketSize;
            // newStartIndex = index;
            return true;
        }
    }
}