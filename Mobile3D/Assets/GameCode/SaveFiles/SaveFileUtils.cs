﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using GameCode.BucketSerialization;
using GameCode.Config;
using GameCode.GameState;
using GameCode.General;
using GameCode.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameCode.SaveFiles
{
    public class SaveFileHeader
    {
        public bool HasCheckpointState;
        public long LastModifiedDateInTicks;
        public long PlayTimeInMiliSeconds;//todo: to be removed - this param is moved to game component

        public static (bool, SerializedBucket) Serializer(object instance, int functionId)
        {
            Assert.IsTrue(instance is SaveFileHeader);
            var gameplayConfiguration = instance as SaveFileHeader;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionId,
                    Version = 1
                }
            };

            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(gameplayConfiguration.LastModifiedDateInTicks));
            bucketData.AddRange(BitConverter.GetBytes(gameplayConfiguration.PlayTimeInMiliSeconds));
            bucketData.AddRange(BitConverter.GetBytes(gameplayConfiguration.HasCheckpointState));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, object) Deserializer(SerializedBucket bucket)
        {
            var gameplayConfiguration = new SaveFileHeader();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            gameplayConfiguration.LastModifiedDateInTicks = BitConverter.ToInt64(dataArray, readIndex);
            readIndex += sizeof(long);
            gameplayConfiguration.PlayTimeInMiliSeconds = BitConverter.ToInt64(dataArray, readIndex);
            readIndex += sizeof(long);
            gameplayConfiguration.HasCheckpointState = BitConverter.ToBoolean(dataArray, readIndex);
            readIndex += sizeof(bool);

            return (true, gameplayConfiguration);
        }
    }
    
    public class SaveFileContents
    {
        public SaveFileHeader Header;
        public GameSnapshot RuntimeState;
        public GameSnapshot CheckpointState;


        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is SaveFileContents);
            var sfc = instance as SaveFileContents;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };

            sfc.Header.HasCheckpointState = sfc.CheckpointState != null;
            
            List<byte> bucketData = new List<byte>();
            SerializationSystem.Serialize(sfc.Header, out SerializedBucket headerBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(headerBucket));
            
            SerializationSystem.Serialize(sfc.RuntimeState, out SerializedBucket runtimStateBucket);
            bucketData.AddRange(SerializationUtils.ToBytes(runtimStateBucket));

            if (sfc.Header.HasCheckpointState)
            {
                SerializationSystem.Serialize(sfc.CheckpointState, out SerializedBucket checkpointStateBucket);
                bucketData.AddRange(SerializationUtils.ToBytes(checkpointStateBucket));
            }
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var sfc = new SaveFileContents();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            //read semi-fixed data
            readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex, out SerializedBucket headerBucket);
            SerializationSystem.Deserialize(headerBucket, out System.Object saveFileHeader);
            sfc.Header = saveFileHeader as SaveFileHeader;
            
            //read we-hell-don't-know-whats-there-data
            readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex, out SerializedBucket runtimeStateBucket);
            SerializationSystem.Deserialize(runtimeStateBucket, out System.Object serializedGameState);
            sfc.RuntimeState = serializedGameState as GameSnapshot;
            
            if (sfc.Header.HasCheckpointState)
            {
                readIndex += SerializationUtils.ToSerializedBucket(dataArray, readIndex, out SerializedBucket checkpointBucket);
                SerializationSystem.Deserialize(checkpointBucket, out System.Object serializedGameCheckpointState);
                sfc.CheckpointState = serializedGameCheckpointState as GameSnapshot;
            }
            else
            {
                sfc.CheckpointState = null;
            }

            return (true, sfc);
        }
    }
    
    public static class SaveFileUtils
    {
        public const string SaveFileExtention = ".fms";
        public const string SaveFileFormat = "fms_{0}"+SaveFileExtention;
        
        public static List<string> GetAllSaveFilePaths()
        {
            List<string> allPaths = new List<string>();

            Assert.IsTrue(Directory.Exists(Application.persistentDataPath));
            var allFiles = Directory.GetFiles(Application.persistentDataPath);
            for (int i = 0; i < allFiles.Length; i++)
            {
                if (allFiles[i].EndsWith(SaveFileExtention))
                {
                    allPaths.Add(allFiles[i]);
                }
            }
            
            return allPaths;
        }

        public static List<string> ConvertPathsToFileNames(List<string> paths)
        {
            List<string> fileNames = new List<string>();

            for (int i = 0; i < paths.Count; i++)
            {
                fileNames.Add(Path.GetFileName(paths[i]));
            }

            return fileNames;
        }

        public static int GetHighestSaveNumber(List<string> fileNames)
        {
            int highestNumber = 0;

            var allNumbers = GetSlotNumbers(fileNames);

            for (int i = 0; i < allNumbers.Count; i++)
            {
                highestNumber = Mathf.Max(highestNumber, allNumbers[i]);
            }

            return highestNumber;
        }

        public static List<int> GetSlotNumbers(List<string> fileNames)
        {
            List<int> slotNumbers = new List<int>();
            
            for (int i = 0; i < fileNames.Count; i++)
            {
                if (fileNames[i].StartsWith("fms_") == false)
                {
                    continue;
                }
                
                var parameters = fileNames[i].ParseExact(SaveFileFormat);
                
                Assert.IsTrue(parameters.Length == 1);

                int number = int.Parse(parameters[0]);

                slotNumbers.Add(number);
            }

            return slotNumbers;
        }

        public static bool SaveContentsToFileAtSlot(SaveFileContents contents, int slot)
        {
            SaveFilePathAndNameForSlot(slot, out var filePath, out _);
            return SaveFileToPath(filePath, contents);
        }


        public static void DeleteSaveFileForSlot(int slot)
        {
            SaveFilePathAndNameForSlot(slot, out var filePath, out _);
            DeleteFileAtPath(filePath);
        }

        private static bool SaveFileToPath(string filePath, SaveFileContents saveFileContents)
        {
            bool success = false;
            
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            
            var file = File.Create(filePath);
            SerializationSystem.Serialize(saveFileContents, out SerializedBucket bucket);
            var byteData = SerializationUtils.ToBytes(bucket);
            file.Write(byteData, 0, byteData.Length);
            file.Close();

            return success;
        }

        private static void DeleteFileAtPath(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public static bool LoadSaveFromSlot(int slot, out SaveFileContents saveFileContents)
        {
            SaveFilePathAndNameForSlot(slot, out var saveFilePath, out _);
            return LoadFromPath(saveFilePath, out saveFileContents);
        }

        public static bool LoadFromPath(string filePath, out SaveFileContents saveFileContents)
        {
            bool success = false;
            saveFileContents = default;

            if (File.Exists(filePath) == false)
            {
                return success;
            }

            var byteData = File.ReadAllBytes(filePath);
            SerializationUtils.ToSerializedBucket(byteData, 0, out SerializedBucket saveFileBucket);
            SerializationSystem.Deserialize(saveFileBucket, out object saveFileInstance);
            saveFileContents = saveFileInstance as SaveFileContents;

            success = true;
            
            return success;
        }

        public static void SaveFilePathAndNameForSlot(int slot, out string saveFilePath, out string saveFileName)
        {
            saveFileName = string.Format(SaveFileFormat, slot);
            saveFilePath = Application.persistentDataPath + "/" + saveFileName;
        }

        public static void SaveFilePathForName(string fileName, out string filePath)
        {
            filePath = Application.persistentDataPath + "/" + fileName;
        }

        public static string GetGenericHeaderFormattedText(SaveFileHeader header)
        {
            var lastModifiedDateTime = new DateTime(header.LastModifiedDateInTicks);
            var playTime = new DateTime(header.LastModifiedDateInTicks);
            string text =
                $"Play Time: {playTime.Hour}:{playTime.Minute}:{playTime.Second}. " +
                $"Last saved: 1{lastModifiedDateTime.Year} {lastModifiedDateTime.Month} {lastModifiedDateTime.Day}, " +
                $"{lastModifiedDateTime.Hour}:{lastModifiedDateTime.Minute}:{lastModifiedDateTime.Second}";

            return text;
        }
    }
}