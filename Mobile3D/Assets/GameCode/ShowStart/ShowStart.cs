﻿using System;
using System.Collections.Generic;
using GameCode.BucketSerialization;
using GameCode.GameState;
using GameCode.Levels;
using GameCode.Levels.Metadata;
using GameCode.LocalizationEngine;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace GameCode
{
    public class ShowStart : ALevelStaticManger
    {
        public float LockProgressButtonTime = 2.0f;
        public WorldSceneMetadata GameplayWorldMetadata;
        
        [Space]
        public AnimatedStateMachine StateMachine;
        public AnimatedState[] Phases;

        [Header("Dynamic Text Test")] 
        public LocalizationVariable CharacterName;
        public LocalizationVariable CharacterAge;
        public LocalizationVariable CharacterCrime;
        public LocalizationVariable RoundCount;
        
        private int m_CurrentPhaseIndex;
        private float m_LockProgressButtonTimeLeft;

        private void OnEnable()
        {
            ResetToStartingPhase();
            Game.GetOrCreate<GameStatistics>().RoundCounter++;

            {//just a test
                int index = Random.Range(0, 4);
                
                LocalizationVariables.Set(CharacterName, () =>
                {
                    var options = new[]
                        {"Pedro Hannibal", "Halina Chanożkiewicz", "Martyniak Czupakabra", "Huanes Mohikanin"};
                    return options[index];
                });
                
                
                LocalizationVariables.Set(CharacterAge, () =>
                {
                    var options = new[]
                        {43, 34, 23, 32};
                    return options[index].ToString();
                });
                
                
                LocalizationVariables.Set(CharacterCrime, () =>
                {
                    var options = new[]
                        {"Podrapał się po tyłku", "Przywaliła w kartony", "Widział potwora z Lochness", "Ciupaga"};
                    return options[index];
                });
                
                
                LocalizationVariables.Set(RoundCount, () => Game.GetOrCreate<GameStatistics>().RoundCounter.ToString());
            }
        }

        private void Update()
        {
            if (m_LockProgressButtonTimeLeft > 0.0f)
            {
                m_LockProgressButtonTimeLeft -= Time.deltaTime;
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    m_CurrentPhaseIndex++;
                    if (m_CurrentPhaseIndex < Phases.Length)
                    {
                        StateMachine.RequestState(Phases[m_CurrentPhaseIndex]);
                        m_LockProgressButtonTimeLeft = LockProgressButtonTime;
                    }
                    else
                    {
                        Game.RuntimeState = Game.CreateSnapshot();
                        Game.RuntimeState.ForceFirstCheckpoint = true;
                        Game.LoadWorld(GameplayWorldMetadata.SceneFileName);
                    }
                }
            }
        }

        public override void LoadState(SerializedStateMap serializedStateMap)
        {
            Debug.Log("ShowStart.LoadState called");
            ResetToStartingPhase();
        }

        public override void ClearState()
        {
            Debug.Log("ShowStart.ClearState called");
            ResetToStartingPhase();
        }

        private void ResetToStartingPhase()
        {
            m_CurrentPhaseIndex = 0;
            StateMachine.RequestState(Phases[0], false);
            m_LockProgressButtonTimeLeft = LockProgressButtonTime;
        }

        public override ASerializedState GenerateSerializedState()
        {
            return new SerializedState();
        }
        
        public class SerializedState : ASerializedState
        {
            public override object Clone()
            {
                var clone = new SerializedState();
                clone.ID = ID;
                return clone;
            }
        }
        

        public static (bool, SerializedBucket) Serializer(System.Object instance, int functionHandle)
        {
            Assert.IsTrue(instance is ShowStart.SerializedState);
            var state = instance as ShowStart.SerializedState;

            SerializedBucket bucket = new SerializedBucket()
            {
                Header = new SerializedBucketHeader()
                {
                    SerializationFunctionID = functionHandle,
                    Version = 1
                }
            };
            
            List<byte> bucketData = new List<byte>();
            bucketData.AddRange(BitConverter.GetBytes(state.ID));
            
            bucket.Header.BucketSize = bucketData.Count;
            bucket.DataBucket = bucketData.ToArray();

            return (true, bucket);
        }

        public static (bool, System.Object) Deserializer(SerializedBucket bucket)
        {
            var state = new ShowStart.SerializedState();
            int readIndex = 0;
            var dataArray = bucket.DataBucket;
            
            state.ID = BitConverter.ToInt32(dataArray, readIndex);
            readIndex += sizeof(int);

            return (true, state);
        }
    }
}