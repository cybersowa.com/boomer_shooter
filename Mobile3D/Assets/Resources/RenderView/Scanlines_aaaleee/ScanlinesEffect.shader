﻿Shader "Custom/Scanlines" {

     Properties {
         _MainTex("_Color", 2D) = "white" {}
         _LineWidth("Line Width", Float) = 4
         _Hardness("Hardness", Float) = 0.9
     }
 
     SubShader {

         Tags {"IgnoreProjector" = "True" "Queue" = "Overlay"} 

         Pass {
         	ZTest Always 
         	Cull Off 
         	ZWrite Off 

         	Fog{ Mode off }
 
         CGPROGRAM
 
 		 #pragma vertex vert
 		 #pragma fragment frag
 		 #pragma fragmentoption ARB_precision_hint_fastest
 		 #include "UnityCG.cginc"
 		 #pragma target 3.0
 
	     struct v2f {
	         float4 pos      : POSITION;
	         float2 uv       : TEXCOORD0;
	         float4 scr_pos : TEXCOORD1;
	     };
	 
	     uniform sampler2D _MainTex;
	     uniform float _LineWidth;
	     uniform float _Hardness;
	 
	     v2f vert(appdata_img v) {
	         v2f o;
	         o.pos = UnityObjectToClipPos(v.vertex);
	         o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord);
	         o.scr_pos = ComputeScreenPos(o.pos);
	         
	         return o;
	     }
	 
	     half4 frag(v2f i) : COLOR {
	         half4 color = tex2D(_MainTex, i.uv);
	         fixed lineSize = _ScreenParams.y*0.002778;
	         float ps = i.scr_pos.y / i.scr_pos.w;//(i.scr_pos.y * _ScreenParams.y / i.scr_pos.w);

			float cycle = i.scr_pos.y * 360 + _Time.y;// * 0.5;//*100;
	     	cycle = fmod(cycle, 1);

	     	float colorBrightness = color.r * 0.3 + color.g*0.59 + color.b *0.11;
	     	colorBrightness = pow(colorBrightness, 2);
	     	
			float baseGradient = cycle - 0.5;
	     	baseGradient = baseGradient * 2;
	     	baseGradient = 1.0 - abs(baseGradient);
	     	baseGradient = pow(baseGradient, 2);

	     	float scanlinePower = 0.5 * pow(colorBrightness, 1/ baseGradient) + 0.5 * baseGradient;
	     	float scanlinePowerV2 = pow(colorBrightness, 1/ baseGradient);
	     	float scanlinePowerV3 = (colorBrightness + baseGradient) * 0.5 + 0.25;
	     	float scanlinePowerV4 = pow(colorBrightness + baseGradient * 0.5, 0.25);
	     	//half4 scanlineColor = pow(color, 1/ baseGradient);
	     	
			// return 0.5*color + 0.75*scanlineColor;

	     	return scanlinePowerV4 * color;// * 1.25;// * 0.75 + color * 0.25;w
			return 0.75*color * scanlinePower + color*0.5;//(1-scanlinePower)*color*0.5;

			// half4 finalColor = color * scanlinePower;
			// finalColor = pow(finalColor, 0.75);
	  //    	
			// return finalColor;
	     	
			//return i.scr_pos.y * _ScreenParams.y * 0.0001;
	     	
	         //return ((int)(ps / floor(_LineWidth*lineSize)) % 2 == 0) ? color : color * float4(_Hardness,_Hardness,_Hardness,1);
	     }
	 
	     ENDCG
	     }
     }
     FallBack "Diffuse"
 }